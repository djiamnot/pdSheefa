// -----------------------------------------------------------------------------
// |    ___  ___  _  _ _     ___                                        _      |
// |   / __>| . \| || \ |   | __>_ _  ___ ._ _ _  ___  _ _ _  ___  _ _ | |__   |
// |   \__ \|  _/| ||   |   | _>| '_><_> || ' ' |/ ._>| | | |/ . \| '_>| / /   |
// |   <___/|_|  |_||_\_|   |_| |_|  <___||_|_|_|\___.|__/_/ \___/|_|  |_\_\   |
// |                                                                           |
// |---------------------------------------------------------------------------|
//
// http://spinframework.sourceforge.net
// Copyright (C) 2009 Mike Wozniewski, Zack Settel
//
// Developed/Maintained by:
//    Mike Wozniewski (http://www.mikewoz.com)
//    Zack Settel (http://www.sheefa.net/zack)
//
// Principle Partners:
//    Shared Reality Lab, McGill University (http://www.cim.mcgill.ca/sre)
//    La Societe des Arts Technologiques (http://www.sat.qc.ca)
//
// Funding by:
//    NSERC/Canada Council for the Arts - New Media Initiative
//    Heritage Canada
//    Ministere du Developpement economique, de l'Innovation et de l'Exportation
//
// -----------------------------------------------------------------------------
//  This file is part of the SPIN Framework.
//
//  SPIN Framework is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SPIN Framework is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with SPIN Framework. If not, see <http://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------


#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include "m_pd.h"


using namespace std;


class spinNode
{
public:
	spinNode(t_symbol *arg_id, t_symbol *arg_type) {
		id = arg_id;
		type = arg_type;
	}
	t_symbol *id;
	t_symbol *type;
};



// declaration of this Pd class:
t_class *spinNodeList_class;



// declaration of the pd struct:
typedef struct _spinNodeList
{
	t_object x_obj;  // the t_object has to be the 1st entry in the structure


	std::vector<spinNode*> nodeList;
 	std::vector<t_symbol*> typeFilter;

 	t_symbol *currentSelection;

 	bool hasWorldNode;

 	t_outlet *status_outlet;
	t_outlet *event_outlet;

} t_spinNodeList;



// ***********************************************************
// This is a function that can be used by std::sort to make a
// list of spinNodes alphabetical:
static bool nodeSortFunction (spinNode *n1, spinNode *n2)
{
	return ( string(n1->id->s_name) < string(n2->id->s_name) );
}

// ***********************************************************
// return a pointer to a spinNode in the nodeList, given an id:
static spinNode* getNode(t_spinNodeList *x, t_symbol *id)
{
	vector<spinNode*>::iterator iter;
	for (iter = x->nodeList.begin(); iter != x->nodeList.end(); iter++)
	{
		if ((*iter)->id == id)
		{
			return (*iter);
			break;
		}
	}
	return NULL;
}

static bool isAllowedType(t_spinNodeList *x, t_symbol *type)
{
	// check to see if this type is allowed...

	// default case is to let anything through:
	bool allowed = true;

	// if a typeFilter exists, we allow if the provided type is in the list:
	if (x->typeFilter.size())
	{
		allowed = false;
		std::vector<t_symbol*>::iterator iter;
		for (iter = x->typeFilter.begin(); iter != x->typeFilter.end(); iter++)
		{
			if ((*iter) == type) {
				allowed=true;
				break;
			}
		}
	}

	return allowed;
}

// ***********************************************************
static void spinNodeList_debug (t_spinNodeList *x)
{

	post("\n=====================================================");

	startpost("[spinNodeList]:: %d types:", x->typeFilter.size());
	std::vector<t_symbol*>::iterator typeIter;
	for (typeIter = x->typeFilter.begin(); typeIter != x->typeFilter.end(); typeIter++)
	{
		poststring((*typeIter)->s_name);
	}
	endpost();

	post("[spinNodeList]:: %d nodes:", x->nodeList.size());
	std::vector<spinNode*>::iterator iter;
	for (iter = x->nodeList.begin(); iter != x->nodeList.end() ; iter++)
	{
		post("  (%s)\t%s", (*iter)->type->s_name, (*iter)->id->s_name);
	}

}

// ***********************************************************
static void spinNodeList_bang (t_spinNodeList *x)
{
	int listOffset;

	// if we include the world node, then we will prepend one extra item to
	// the front of the list
	if (x->hasWorldNode) listOffset = 1;
	else listOffset = 0;


	t_atom *atomList = (t_atom*) getbytes( (x->nodeList.size()+listOffset) * sizeof(t_atom) );

	if (x->hasWorldNode) SETSYMBOL(atomList, gensym("world"));

	vector<spinNode*>::iterator iter;
	for (int i=0; i<x->nodeList.size(); i++)
	{
		SETSYMBOL (atomList+i+listOffset, x->nodeList[i]->id);
	}

	outlet_anything(x->status_outlet, &s_list, x->nodeList.size()+listOffset, atomList);
}


// ***********************************************************
static void spinNodeList_info (t_spinNodeList *x, t_symbol *id)
{

	spinNode *n = getNode(x, id);
	if (n)
	{
		t_atom *atomList = (t_atom*) getbytes(3*sizeof(t_atom));
		SETSYMBOL (atomList+0, n->id);
		SETSYMBOL (atomList+1, n->type);
		outlet_anything(x->event_outlet, gensym("info"), 2, atomList);
	} else {
		post("Error: Could not find node %s", id->s_name);
	}
}


// ***********************************************************
static spinNode *spinNodeList_doCreate(t_spinNodeList *x, t_symbol *id, t_symbol *type)
{
	spinNode *n;

	// do nothing if it already exists:
	n = getNode(x,id);
	if (n) {
		//post("ERROR: node %s already exists with type %s. Could not create.", n->id->s_name, n->type->s_name);
		return NULL;
	}

	// otherwise, we need to create a new spinNode:
	n = new spinNode(id, type);
	x->nodeList.push_back(n);

	return n;
}

static void spinNodeList_createNode (t_spinNodeList *x, t_symbol *id, t_symbol *type)
{
	// ignore id of "world" ad "NULL"
	if ( (id==gensym("world")) || (id==gensym("NULL")) ) return;

	// check to see if this type is allowed:
	if (!isAllowedType(x,type)) return;

	// create the node:
	spinNode *n = spinNodeList_doCreate(x,id,type);

	if (n)
	{
		// re-sort list alphabetically:
		std::sort(x->nodeList.begin(),x->nodeList.end(),&nodeSortFunction);

		// send a createNode event from the event_outlet:
		t_atom *atomList = (t_atom*) getbytes(3*sizeof(t_atom));
		SETSYMBOL (atomList+0, n->id);
		SETSYMBOL (atomList+1, n->type);
		outlet_anything(x->event_outlet, gensym("createNode"), 2, atomList);

		// send the nodeList out the status_outlet:
		spinNodeList_bang(x);
	}
}

// ***********************************************************
static void spinNodeList_doDelete (t_spinNodeList *x, spinNode *n)
{
	if (!n) return;

	// first, send a deleteNode event from the event_outlet:
	/*
	t_atom *atomList = (t_atom*) getbytes(3*sizeof(t_atom));
	SETSYMBOL (atomList+0, n->id);
	SETSYMBOL (atomList+1, n->type);
	outlet_anything(x->event_outlet, gensym("deleteNode"), 2, atomList);
	*/
	t_atom a;
	SETSYMBOL (&a, n->id);
	outlet_anything(x->event_outlet, gensym("deleteNode"), 1, &a);

	std::vector<spinNode*>::iterator iter;
	for (iter = x->nodeList.begin(); iter != x->nodeList.end(); iter++)
	{
		if ((*iter) == n) {
			x->nodeList.erase(iter);
			break;
		}
	}
}

static void spinNodeList_deleteNode (t_spinNodeList *x, t_symbol *id)
{
	// find the node:
	spinNode *n = getNode(x,id);

	if (!n) {
		//post("Error: Could not delete node %s. Not found.", id->s_name);
		return;
	}

	// now remove it from the nodeList:
	spinNodeList_doDelete(x, n);

	// send the nodeList:
	spinNodeList_bang(x);
}

static void spinNodeList_getType (t_spinNodeList *x, t_symbol *id)
{
	spinNode *n = getNode(x, id);
	t_atom outList[2];

	if (n)
	{
		SETSYMBOL (outList, n->id);
		SETSYMBOL (outList, n->type);
		outlet_anything(x->event_outlet, gensym("nodeType"), 2, outList);
	}

}

// ***********************************************************
static void spinNodeList_typeList(t_spinNodeList *x, t_symbol *s, int argc, t_atom *argv)
{
	if ((!argc) || (argv[0].a_type!=A_SYMBOL)) return;

	// first item is the type
	t_symbol *type = argv[0].a_w.w_symbol;

	if (!isAllowedType(x,type)) return;

	// first remove all nodes of that type:
	std::vector<spinNode*>::iterator iter;
	for (iter = x->nodeList.begin(); iter != x->nodeList.end(); )
	{
		if ((*iter)->type == type) {
			x->nodeList.erase(iter);
			//break;
		} else iter++;
	}

	// now add the new list:
	t_symbol *nullSymbol = gensym("NULL");
	for (int i=1; i<argc; i++)
	{
		if ((argv[i].a_type == A_SYMBOL) && (argv[i].a_w.w_symbol!=nullSymbol))
		{
			spinNode *n = spinNodeList_doCreate(x, argv[i].a_w.w_symbol, type);
		}
	}

	// if the currentSelection has been deleted, then send a deleteNode message:
	spinNode *n = getNode(x,x->currentSelection);
	if (!n)
	{
		t_atom a;
		SETSYMBOL (&a, x->currentSelection);
		outlet_anything(x->event_outlet, gensym("deleteNode"), 1, &a);
	}

	// re-sort list alphabetically:
	std::sort(x->nodeList.begin(),x->nodeList.end(),&nodeSortFunction);

	// send the nodeList:
	spinNodeList_bang(x);
}

// ***********************************************************
static void spinNodeList_clear(t_spinNodeList *x)
{
	/*
	std::vector<spinNode*>::iterator iter;

	iter = x->nodeList.begin();
	while (iter != x->nodeList.end())
	{
		spinNodeList_doDelete(x, (*iter));
	}

	// send the nodeList:
	spinNodeList_bang(x);
	*/
}

// ***********************************************************
// ********************** CONSTRUCTOR: ***********************
// ***********************************************************
static void *spinNodeList_new (t_symbol *s, int argc, t_atom *argv)
{
	t_spinNodeList *x = (t_spinNodeList *) pd_new (spinNodeList_class);

	x->nodeList.clear();
	x->typeFilter.clear();
	x->hasWorldNode = false;
	x->currentSelection = gensym("NULL");

	for (int i=0; i<argc; i++)
	{
		//std::cout << "got arg of type: " << argv[i].a_type << ", value=" << atom_getsymbol(argv+i)->s_name << std::endl;
		if (argv[i].a_type == A_SYMBOL)
		{
			if (atom_getsymbol(argv+i) == gensym("world")) x->hasWorldNode = true;
			else x->typeFilter.push_back(atom_getsymbol(argv+i));
		} else {
			//error("Error: spinNodeList takes only symbol arguments, corresponding to the allowed node types.");
			//return NULL;
		}
	}

	x->status_outlet = outlet_new(&x->x_obj, &s_list);
	x->event_outlet = outlet_new(&x->x_obj, &s_list);

	// add a symbol inlet to update the currentSelection:
	symbolinlet_new(&x->x_obj, &x->currentSelection);

	return (void *) x;
}


// ***********************************************************
// ********************** DESTRUCTOR: ************************
// ***********************************************************
static void spinNodeList_free (t_spinNodeList * x)
{
	spinNodeList_clear(x);
	x->typeFilter.clear();
}

// ***********************************************************
// ******************* PD SETUP FUNCTION: ********************
// ***********************************************************
extern "C" void spinNodeList_setup (void)
{


	// Now we create a new Pd class:
	spinNodeList_class = class_new(gensym("spinNodeList"), (t_newmethod)spinNodeList_new, (t_method)spinNodeList_free, sizeof(t_spinNodeList), CLASS_DEFAULT, A_GIMME, 0);
	// Note: for some reason, setting up new Pd methods and classes with zero
	// arguments won't work. For example, class_new() and class_addmethod()
	// needs to have at least one A_DEFLOAT or something in order to instantiate
	// properly. why?? because of c++? ????

	// Declare all Pd method handlers:
	class_addmethod(spinNodeList_class, (t_method)spinNodeList_debug, gensym("debug"), A_DEFFLOAT, 0);
	class_addmethod(spinNodeList_class, (t_method)spinNodeList_clear, gensym("clear"), A_DEFFLOAT, 0);
	class_addmethod(spinNodeList_class, (t_method)spinNodeList_createNode, gensym("createNode"), A_DEFSYMBOL, A_DEFSYMBOL, 0);
	class_addmethod(spinNodeList_class, (t_method)spinNodeList_deleteNode, gensym("deleteNode"), A_DEFSYMBOL, 0);
	class_addmethod(spinNodeList_class, (t_method)spinNodeList_getType, gensym("getType"), A_DEFSYMBOL, 0);
	class_addmethod(spinNodeList_class, (t_method)spinNodeList_info, gensym("info"), A_DEFSYMBOL, 0);
	class_addmethod(spinNodeList_class, (t_method)spinNodeList_typeList, gensym("typeList"), A_GIMME, 0);
	class_addsymbol(spinNodeList_class, (t_method)spinNodeList_info);
	class_addbang(spinNodeList_class, (t_method)spinNodeList_bang);
}
