// ***************************************************
// Written by Mike Wozniewski (http://www.mikewoz.com)
// ***************************************************

#include "m_pd.h"
#include <vector>

#define UNUSED -1


using namespace std;

static t_class *anyreg_class;

/*
typedef struct _anyregParams
{
	t_symbol *id;
	t_symbol *selector;
	t_atom *atomList;
	int atomCount;
	int refcount;
} t_anyregParams;

*/

class anyregParams
{
public:
	anyregParams(t_symbol *s)
	{
		this->id=s;
		this->refcount = 1;
		this->atomCount = 0;
		this->selector = gensym("list");	
	}
	~anyregParams();
  
	t_symbol *id;
	t_symbol *selector;
	t_atom *atomList;
	int atomCount;
	int refcount;

};

typedef struct _anyreg
{
	t_object x_obj;
	anyregParams *paramsPtr;
} t_anyreg;

vector<anyregParams*> anyregVector;


anyregParams* anyreg_getPointer(t_symbol *argid)
{
  
  vector<anyregParams*>::iterator iter;
  for (iter = anyregVector.begin(); iter != anyregVector.end(); iter++)
  {
    if ((*iter)->id == argid)
    {
      return (*iter);
    }
  }

  // not found:
  return NULL;  
  
}



void anyreg_debug(t_anyreg *x, t_symbol* s)
{
	vector<anyregParams*>::iterator iter;
  for (iter = anyregVector.begin(); iter != anyregVector.end(); iter++)
  {
		startpost("REGISTER %s:", (*iter)->id->s_name);
		if ((*iter)->atomCount)
		{
			poststring((*iter)->selector->s_name);
			postatom((*iter)->atomCount, (*iter)->atomList);
		} else poststring("<empty>");
		endpost(); 
  }
}

void anyreg_bang(t_anyreg *x)
{
	if (x->paramsPtr->atomCount) outlet_list(x->x_obj.ob_outlet, x->paramsPtr->selector, x->paramsPtr->atomCount, x->paramsPtr->atomList);
}

void anyreg_anything(t_anyreg *x, t_symbol* s, t_int argc, t_atom* argv)
{
	
	/*
	startpost("got s=%s with args:", s->s_name);
	postatom(argc, argv);
	endpost();
	*/
	
	// first clear the atomList:
	if (x->paramsPtr->atomCount) freebytes(x->paramsPtr->atomList, x->paramsPtr->atomCount*sizeof(t_atom));
	
	
	if (  (s==(gensym("symbol"))) || (s==(gensym("list"))) || (s==(gensym("float"))) || (s==(gensym("pointer")))  )
	{
		// the selector is standard Pd, so set the internal selector, and do a straight
		// copy of the atomList into memory:
		x->paramsPtr->selector = s;
		x->paramsPtr->atomList = (t_atom*) copybytes(argv, argc*sizeof(t_atom));
		x->paramsPtr->atomCount = argc;
	} else {
		// the selector is non-standard, so we make set the selector to 'list' and
		// attach the symbol *s as the first atom in the atomList, and copy the rest
		// of the atoms from argv as the rest of the atomList:
		x->paramsPtr->selector = gensym("list");
		x->paramsPtr->atomList = (t_atom*) getbytes((argc+1)*sizeof(t_atom));
		x->paramsPtr->atomCount = argc+1;
		SETSYMBOL(x->paramsPtr->atomList, s);
		for (int i=0; i<argc; i++)
		{
			(x->paramsPtr->atomList+i+1)->a_type = (argv+i)->a_type;
			(x->paramsPtr->atomList+i+1)->a_w = (argv+i)->a_w;
		}

	}
	
	/*
	startpost("AFTER params: id=%s, selector=%s, count=%d, atoms= ", x->paramsPtr->id->s_name, x->paramsPtr->selector->s_name, x->paramsPtr->atomCount);
	postatom(x->paramsPtr->atomCount, x->paramsPtr->atomList);
	endpost();
	*/
	
}

static void *anyreg_new(t_symbol *s, int argc, t_atom *argv)
{
	
	t_symbol *newsym;
	t_atom *newargv;
	int newargc;
	t_anyreg *x;
	
	
	// Take the first symbol from the list of args and set our id to that.
	// All other atoms get saved to the atomList
	
	if (!argc)
	{
		error("anyreg: requires ID_sym");
		return NULL;
	}

	if ((argv)->a_type != A_SYMBOL)
	{
		error("anyreg: requires ID_sym");
		return NULL;
	}
	else newsym=(argv)->a_w.w_symbol;


		
	x = (t_anyreg *)pd_new(anyreg_class);
	outlet_new(&x->x_obj, &s_list);
	
	anyregParams *testPtr = anyreg_getPointer(newsym);
	
	if (testPtr!=NULL)
	{
		x->paramsPtr = testPtr;
		x->paramsPtr->refcount++;
	} else {
		x->paramsPtr = new anyregParams(newsym);
		anyregVector.push_back(x->paramsPtr);
	}
	
	// create a right inlet to dynamically change the id:
	inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("symbol"), gensym("setID"));
	

	if (argc==1) return(x);
	
	// else there are other atoms to set to anyreg's initial state

	newargv=argv+1;
	newsym=gensym("anything");
	newargc=argc-1;
	
	if (argc==2) 
	{	switch ((newargv)->a_type)
		{	 
			case A_SYMBOL:
				newsym = gensym("symbol");
				break;
			case A_FLOAT: 
				newsym = gensym("float");
				break;
			default: 
				error("anyreg_new: bug found, ignoring additional arg");
				return(x);
		}
	}
	else // argv at least 3
	{
		switch ((newargv)->a_type)
		{	 
			case A_SYMBOL: 
				newsym = newsym=(newargv)->a_w.w_symbol;
				newargv++;
				newargc--;
				break;
			case A_FLOAT:  
				newsym = gensym("list");
				break;
			default: 
				error("anyreg_new: bug found, ignoring additional args");
				return(x);
		}
	}
		
	anyreg_anything(x, newsym, newargc, newargv);
	
	//post("ANYREG:newsym=%s",newsym->s_name);

	return (x);
}

void anyreg_free(t_anyreg *x)
{
	
  vector<anyregParams*>::iterator iter;
	
	// If there are still other objects using the paramsPtr, then we just decrease
	// the refcount. Otherwise, we free the atomList and remove this paramsPtr from
	// the vector before the object is destroyed:
	if (x->paramsPtr->refcount > 1)
	{
		x->paramsPtr->refcount--;
		
	} else {
		for (iter = anyregVector.begin(); iter != anyregVector.end(); iter++)
		{
			if ((*iter)->id == x->paramsPtr->id)
			{
				if (x->paramsPtr->atomCount) freebytes(x->paramsPtr->atomList, x->paramsPtr->atomCount*sizeof(t_atom));
				anyregVector.erase(iter);
				break;
			}
		}
	}
	
}

void anyreg_setID(t_anyreg *x, t_symbol* s)
{
	if (s != x->paramsPtr->id)
	{
		// dereference the current paramsPtr:
		anyreg_free(x);
		
		// test to see if a paramsPtr exists for the provided symbol:
		anyregParams *testPtr = anyreg_getPointer(s);
		
		// if it exists, just set the paramsPtr to that symbol:
		if (testPtr!=NULL)
		{
			x->paramsPtr = testPtr;
			x->paramsPtr->refcount++;
			
			// otherwise creat a new one:
		} else {
			x->paramsPtr = new anyregParams(s);
			anyregVector.push_back(x->paramsPtr);
		}
	}
	
}

extern "C" void anyreg_setup(void)
{
	anyreg_class = class_new(gensym("anyreg"), (t_newmethod)anyreg_new, (t_method)anyreg_free, sizeof(t_anyreg), CLASS_DEFAULT, A_GIMME, 0);

	class_addbang(anyreg_class, anyreg_bang);
	class_addanything(anyreg_class, anyreg_anything);
	class_addmethod(anyreg_class, (t_method)anyreg_setID, gensym("setID"), A_DEFSYMBOL, 0);
	class_addmethod(anyreg_class, (t_method)anyreg_debug, gensym("anyreg_debug"), A_DEFSYMBOL, 0);
	//class_sethelpsymbol(anyreg_class, gensym("help-anyreg"));
	
	anyregVector.clear();
}
