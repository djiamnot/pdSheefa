TO BUILD pdsheefa (and xjimmies) FROM SOURCE:


----------------------------
FOR OSX

You will need to have:

MacPorts (http://www.macports.org)
XCode developer tools (http://developer.apple.com/tools/xcode)


Then install dependencies:

For OSX 10.5:
> sudo port install  liblo

For OSX 10.6 libraries:
> sudo port install liblo +univeral  
(the +universal option after a library ensures that both i386 and 64 bit are built)


-------------------------------
FOR LINUX

You will need to install the following dependency:

FOR Ubuntu 10.4 (Lucid) and more recent:

> sudo apt-get install liblo-dev


FOR Ubuntu 9.10 (Karmic) and earlier:

liblo version 0.26+ is NOT available as a package, so you MUST build liblo from source.

(go to http://liblo.sourceforge.net)

--------------------------------


To compile and install:

> cd PATH_TO_PDSHEEFA
> make
> make install

All externs,  abstractions and helpfiles will be copied to the directories below:


for OSX: 
	~/Library/Pd/pdsheefa
	~/Library/Pd/xjimmies
	
for Linux:
	/usr/local/lib/pd-externals/pdsheefa
	~/pd-externals/xjimmies

note: you will need to add the above two directories to your pd search path, according to your OS

-Z. Settel March 2011