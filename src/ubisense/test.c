///
/// Ubisense on-the-wire protocol example
/// Author: Pete Steggles
/// 
/// Example code showing how to decode the Ubisense
/// on-the-wire protocol.
///
/// Elements currently supported:
///
/// - Cell entry event: when a tag enters a cell
/// - Cell exit event: when a tag leaves a cell
/// - Location event: when a tag is located
/// - Uplink only event: when a tag uplinks with a radio message
///                      but is not located by the cell
/// - Button event: when a tag button is pressed
///
/// Elements currently missing:
///
/// - Data delivery request: a message to the cell requesting
///                          delivery of a message to a tag in
///                          that cell
/// - Data ack event: when a data delivery request is ack'ed
///
/// Contents of this file:
///
/// - Magic numbers used to identify Ubisense packets 
/// - Structures containing Ubisense messages 
/// - Methods for parsing OTW messages into message structs 
/// - Scaffolding to create a socket, parse and print data  
///

/// Platform notes: this code is written for Unix-style networking
/// so these includes, and the socket code, will have to be changed
/// for e.g. Windows platforms (unless using cygwin or similar).

#include <stdlib.h>

#include <string.h>
#include <sys/socket.h>

//#include <sys/types.h>
//#include <arpa/inet.h>

#include <netinet/in.h>
 
/// NB sizeof(int) must be 4
///    sizeof(short) must be 2
///    sizeof(float) must be 4

/// -- Magic numbers used to identify Ubisense packets --------
 
#define MAGIC_0 0xE298
#define CELL_ENTRY_MESSAGE_MAGIC_1 0x1B1
#define CELL_EXIT_MESSAGE_MAGIC_1 0x12E
#define LOCATION_MESSAGE_MAGIC_1 0x26A
#define UPLINK_ONLY_MESSAGE_MAGIC_1 0x13D
#define BUTTON_MESSAGE_MAGIC_1 0xBD

/// -- Structures containing Ubisense messages ----------------

/// The cell entry message structure 
struct CellEntryMessage {
   unsigned short magic_0 ;           
   unsigned short magic_1 ;
   unsigned int tag_id_bottom ;         // Bottom 32 bits of tag id
   unsigned int slot ;                  // Timeslot number of message
   unsigned int cell ;                  // Id of the cell that generated the message
   unsigned int dump ;                  // Filter state dump request (internal use)
} ;
 
/// The cell exit message structure 
struct CellExitMessage {
   unsigned short magic_0 ;
   unsigned short magic_1 ;
   unsigned int tag_id_bottom ;         // Bottom 32 bits of tag id
   unsigned int slot ;                  // Timeslot number of message
   unsigned int cell ;                  // Id of the cell that generated the message
} ;

/// The location message structure 
struct LocationMessage {
   unsigned short magic_0 ;
   unsigned short magic_1 ;
   unsigned int tag_id_top ;            // Top 32 bits of tag id (currently always zero)
   unsigned int tag_id_bottom ;         // Bottom 32 bits of tag id
   unsigned int flags ;                 // (flags & 1) == 'this is a valid location'
   float x ;                            // (x,y,z) position
   float y ;                            //        .
   float z ;                            //        .
   float gdop ;                         // Geometric dilution of precision 
   float error ;                        // Standard error of location calculation
   unsigned int slot ;                  // Timeslot number of message
   unsigned int slot_interval ;         // Microseconds between two timeslots (a constant
                                        // for one instance of the location system)
   unsigned int slot_delay ;            // Delay between the timeslot in which the UWB
                                        // was transmitted and the timeslot of the message
                                        // (a constant for one instance of the location system)
   unsigned int cell ;                  // Id of the cell that generated the message
} ;

/// The uplink-only message structure
struct UplinkOnlyMessage {
   unsigned short magic_0 ;
   unsigned short magic_1 ;
   unsigned int tag_id_top ;            // Top 32 bits of tag id (currently always zero)
   unsigned int tag_id_bottom ;         // Bottom 32 bits of tag id
   unsigned int slot ;                  // Timeslot number of message
   unsigned int cell ;                  // Id of the cell that generated the message
   unsigned char tremble_switch_on ;    // (tremble_switch_on & 1) == 'the tremble switch was recently activated'
   unsigned short rssi ;                // Maximum RSSI value for the tag in this cell
} ;

/// The button press message
struct ButtonMessage {
   unsigned short magic_0 ;
   unsigned short magic_1 ;
   unsigned int tag_id_bottom ;         // Bottom 32 bits of tag id
   unsigned int slot ;                  // Timeslot number of message
   unsigned char orange ;               // (orange & 1) == 'the orange button was pressed'
   unsigned char blue ;                 // (blue & 1) == 'the blue button was pressed'
} ;

/// -- Methods for parsing OTW messages into message structs --

/// Swap bytes in a 2-byte block
void swap2(char* b1, char* b2)
{
   char t = *b1;
   *b1 = *b2;
   *b2 = t;
}

/// Swap bytes in a 4-byte block
void swap4(char* b)
{
   swap2(b,b+3);
   swap2(b+1,b+2);
}

struct LocationMessage *parse_location_message (int length,char *buffer)
{
   if (length == sizeof(struct LocationMessage)) 
   {
      struct LocationMessage* message = ((struct LocationMessage *) buffer) ;
      if ((ntohs(message->magic_0) == MAGIC_0) && (ntohs(message->magic_1) == LOCATION_MESSAGE_MAGIC_1)) {
         message->tag_id_top = ntohl(message->tag_id_top) ;
         message->tag_id_bottom = ntohl(message->tag_id_bottom) ;
         message->flags = ntohl(message->flags) ;
         if (1 != ntohl(1)) {
            swap4((char*) &(message->x)) ;
            swap4((char*) &(message->y)) ;
            swap4((char*) &(message->z)) ;
            swap4((char*) &(message->gdop)) ;
            swap4((char*) &(message->error)) ;
         }
         message->slot = ntohl(message->slot) ;
         message->slot_interval = ntohl(message->slot_interval) ;
         message->slot_delay = ntohl(message->slot_delay) ;
         message->cell = ntohl(message->cell) ;
         return message ;
      }
   }
   return 0 ;
}

struct CellEntryMessage *parse_cell_entry_message (int length,char *buffer)
{
   if (length == sizeof(struct CellEntryMessage))
   {
      struct CellEntryMessage* message = ((struct CellEntryMessage *) buffer) ;
      if ((ntohs(message->magic_0) == MAGIC_0) && (ntohs(message->magic_1) == CELL_ENTRY_MESSAGE_MAGIC_1)) {
         message->tag_id_bottom = ntohl(message->tag_id_bottom) ;
         message->slot = ntohl(message->slot) ;
         message->cell = ntohl(message->cell) ;
         message->dump = ntohl(message->dump) ;
         return message ;
      }
   }
   return 0 ;
}

struct CellExitMessage *parse_cell_exit_message (int length,char *buffer)
{
   if (length == sizeof(struct CellExitMessage))
   {
      struct CellExitMessage* message = ((struct CellExitMessage *) buffer) ;
      if ((ntohs(message->magic_0) == MAGIC_0) && (ntohs(message->magic_1) == CELL_EXIT_MESSAGE_MAGIC_1)) {
         message->tag_id_bottom = ntohl(message->tag_id_bottom) ;
         message->slot = ntohl(message->slot) ;
         message->cell = ntohl(message->cell) ;
         return message ;
      }
   }
   return 0 ;
}

struct UplinkOnlyMessage *parse_uplink_only_message (int length,char *buffer)
{
   if (length == sizeof(struct UplinkOnlyMessage))
   {
      struct UplinkOnlyMessage* message = ((struct UplinkOnlyMessage *) buffer) ;
      if ((ntohs(message->magic_0) == MAGIC_0) && (ntohs(message->magic_1) == UPLINK_ONLY_MESSAGE_MAGIC_1)) {
         message->tag_id_bottom = ntohl(message->tag_id_bottom) ;
         message->slot = ntohl(message->slot) ;
         message->cell = ntohl(message->cell) ;
         message->rssi = ntohs(message->rssi) ;
         return message ;
      }
   }
   return 0 ;
}

struct ButtonMessage *parse_button_message (int length,char *buffer)
{
   if (length == sizeof(struct ButtonMessage))
   {        
      struct ButtonMessage* message = ((struct ButtonMessage *) buffer) ;
      if ((ntohs(message->magic_0) == MAGIC_0) && (ntohs(message->magic_1) == BUTTON_MESSAGE_MAGIC_1)) {
         message->tag_id_bottom = ntohl(message->tag_id_bottom) ;
         message->slot = ntohl(message->slot) ;
         return message ;
      }  
   }  
   return 0 ;
}

/// -- Scaffolding to create a socket, parse and print data -- 

/// Call this once to create a UDP socket on a given port
/// If it returns -1 it has failed.
int create_udp_socket_returning_minus_one_if_failed(int port) 
{
   int    s;            
   struct sockaddr_in host_address;   
   
   s=socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if (s < 0) 
      return -1 ;
   
   memset((void*)&host_address, 0, sizeof(host_address));
   host_address.sin_family=PF_INET;
   host_address.sin_addr.s_addr=INADDR_ANY;
   host_address.sin_port=htons(port);
   
   if (bind(s, (struct sockaddr*)&host_address, sizeof(host_address)) < 0) 
      return -1 ;

   return s;
}

#include <stdio.h>
#define BUFFER_SIZE 256

void receive_from_socket_and_print(int s)
{
   int make_sure_the_buffer_is_word_aligned ;
   char buffer[BUFFER_SIZE] ;
   struct sockaddr_in host_address;
   unsigned int host_address_size = sizeof(host_address);

   while (1) {
      int length = recvfrom(s, buffer, BUFSIZ, 0, (struct sockaddr*) &host_address, &host_address_size);
      { 
         struct LocationMessage *message = 0 ;
         if ((message = parse_location_message(length,buffer))) {
            fprintf(stdout,
                    "location: %ul in cell %ul at (%f,%f,%f) (%s) in slot %ul\n",
                    message->tag_id_bottom,
                    message->cell,
                    message->x,
                    message->y,
                    message->z,
                    (message->flags & 1 ? "valid" : "invalid"),
                    message->slot) ;
         }
      }
      { 
         struct CellEntryMessage *message = 0 ;
         if ((message = parse_cell_entry_message(length,buffer))) {
            fprintf(stdout,
                    "cell entry: %ul enters cell %ul in slot %ul\n",
                    message->tag_id_bottom,
                    message->cell,
                    message->slot) ;
         }
      }
      { 
         struct CellExitMessage *message = 0 ;
         if ((message = parse_cell_exit_message(length,buffer))) {
            fprintf(stdout,
                    "cell exit: %ul leaves cell %ul in slot %ul\n",
                    message->tag_id_bottom,
                    message->cell,
                    message->slot) ;
         }
      }
      {
         struct UplinkOnlyMessage *message= 0 ;
         if ((message = parse_uplink_only_message(length,buffer))) {
            fprintf(stdout,
                    "uplink: %ul uplinks in cell %ul but is not located in slot %ul with tremble switch %s\n",
                    message->tag_id_bottom,
                    message->cell,
                    message->slot,
                    (message->tremble_switch_on ? "on" : "off")) ;
         }
      }
      {
         struct ButtonMessage *message= 0 ;
         if ((message = parse_button_message(length,buffer))) {
            unsigned char orange = message->orange ;
            unsigned char blue = message->blue ;
            fprintf(stdout,
                    "button: %ul sends %s%s%s button event in slot %ul\n",
                    message->tag_id_bottom,
                    (orange ? "orange" : ""),
                    (((orange) && (blue)) ? " and " : ((orange) || (blue) ? "" : "null")),
                    (blue ? "blue" : ""),
                    message->slot) ;
         }
      }
   }
}

int main (char argc,char *argv[])
{
   if (argc != 2) {
      fprintf(stderr,"usage: %s <port>\n",argv[0]) ;
      return(-1) ;
   }

   int socket = create_udp_socket_returning_minus_one_if_failed(atoi(argv[1])) ;

   if (socket == -1) {
      fprintf(stderr,"%s: failed to open socket %s\n",argv[0],argv[1]) ;
      return(-1) ;
   }

   receive_from_socket_and_print(socket) ;
}


