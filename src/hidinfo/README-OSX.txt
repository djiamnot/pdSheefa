ON OSX:

Before compiling hidinfo, you will need to build the HID Utiliities Library. 

To do so, you will need to install the XCode developer tools (http://developer.apple.com/tools/xcode), and then open project, located in:

pdsheefa/src/hidinfo/HID_UtilitiesSource.


Open that directory and click on the project called:  "HID Utilities Slib.xcodeproj" and BUILD  (choose "build" in the "build" menubar item).


this will generate the needed libarary, and you can procede with compiling hidinfo. 

   
