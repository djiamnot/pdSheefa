//#include <m_pd.h>
#include "m_pd.h"

#include <sys/stat.h>

#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>

#ifdef __LINUX__
#include <linux/input.h>
#define LINUXEVENT_DEVICE   "/dev/input/event"
#endif

#ifdef __DARWIN__
#include "HID_Utilities_External.h"
#endif


static t_class *hidinfo_class;

typedef struct _hidinfo
{
	t_object x_obj;
} t_hidinfo;


#ifdef __DARWIN__
// ********************* DARWIN STUFF: *************************
pRecDevice hid_get_device_by_number(t_int device_number)
{
	pRecDevice pCurrentHIDDevice;
	t_int i, numdevs;

	// If the specified device is greater than the total number of devices,
	// return an error:
	numdevs = (t_int) HIDCountDevices();
	if (device_number >= numdevs) {
		error("[hid]: no such device, \"%d\", only %d devices found\n",device_number,numdevs);
		return (NULL);
	}

	// The most recently discovered HID is the first element of the list here.
	// We want the oldest to be number 0 rather than the newest, so we use
	// (numdevs - device_number - 1):
	pCurrentHIDDevice = HIDGetFirstDevice();
	for (i=0; i < numdevs - device_number - 1; ++i)
	{
		pCurrentHIDDevice = HIDGetNextDevice(pCurrentHIDDevice);
	}
	
	return pCurrentHIDDevice;
}


static void hidinfo_bang(t_hidinfo *x)
{
	pRecDevice pCurrentHIDDevice;
	
	t_atom outList[2];
	t_int numdevs;
	int i;
	
	if (HIDBuildDeviceList (NULL, NULL)) error("[hid]: no HID devices found\n");
	
	numdevs = (t_int) HIDCountDevices();
	for (i=0; i < numdevs; i++)
	{
		pCurrentHIDDevice = hid_get_device_by_number(i);
		//post("[HIDINFO]: Device %d: '%s' '%s' version %d",i,pCurrentHIDDevice->manufacturer, pCurrentHIDDevice->product,pCurrentHIDDevice->version);

		SETFLOAT(outList, i);
		SETSYMBOL(outList+1, gensym(pCurrentHIDDevice->product));
		outlet_anything(x->x_obj.ob_outlet, &s_list, 2, outList);
	}
}

// ********************* END OF DARWIN STUFF *************************
#endif


#ifdef __LINUX__
void hidinfo_bang(t_hidinfo *x)
{
	int i,eventType, eventCode, buttons, rel_axes, abs_axes, ff, fd;
	char devicename[256] = "Unknown";
	char devname[18] = "/dev/input/event0";
	struct input_event  x_input_event;
	t_atom outList[2];

	for (i=0;i<10;++i)
	{
		sprintf(&devname,"%s%d",LINUXEVENT_DEVICE,i);
		if (devname) {
			// open the device read-only, non-exclusive:
			fd = open (&devname, O_RDONLY | O_NONBLOCK);
			
			// test if device NOT open:
			if (fd < 0 ) {

				SETFLOAT(outList, i);
				SETSYMBOL(outList+1, gensym("NULL"));
				outlet_anything(x->x_obj.ob_outlet, &s_list, 2, outList);
				
				fd = -1;

			// otherwise:
			} else {
				// read input_events from the LINUXEVENT_DEVICE stream
				// (It seems that is just there to flush the event input buffer?)
				while (read (fd, &(x_input_event), sizeof(struct input_event)) > -1);
			
				// get name of device:
				ioctl(fd, EVIOCGNAME(sizeof(devicename)), devicename);
				
				// output to Pd:
				SETFLOAT(outList, i);
				SETSYMBOL(outList+1, gensym(devicename));
				outlet_anything(x->x_obj.ob_outlet, &s_list, 2, outList);
				
				close (fd);
			}

		} else {
			// devname does not exist
		}
	}
}
#endif

static void *hidinfo_new(t_symbol* s, int argc, t_atom *argv)
{
	t_hidinfo *x = (t_hidinfo *)pd_new(hidinfo_class);
	outlet_new(&x->x_obj, &s_list);

	return (x);
}

void hidinfo_setup(void)
{
	hidinfo_class = class_new(gensym("hidinfo"), (t_newmethod)hidinfo_new, 0, sizeof(t_hidinfo), CLASS_DEFAULT, 0);
	class_addbang(hidinfo_class, hidinfo_bang);

}


