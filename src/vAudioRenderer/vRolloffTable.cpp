

#include <sstream>
#include <vector>
#include "vRolloffTable.h"
#include "math.h"

 #include <cstdio>  // added by ZS. to work with Ubuntu 9.1
using namespace std;


// ***********************************************************
// constructor
vRolloffTable::vRolloffTable (t_symbol *path, t_symbol *rolloffName) 
{
	this->path = path;
	this->tableName = rolloffName;
	this->tableSize = 1;
	this->tableData = new t_float(1.0f);
	if (!this->update()) throw "CreationError";
}


// ***********************************************************
// destructor
vRolloffTable::~vRolloffTable()
{
  freebytes(tableData,tableSize*sizeof(t_float));
}

// ***********************************************************
const char *vRolloffTable::getFilename()
{
	//char filename[255];
	//sprintf(filename, "%s/%s", canvas_getdir(x->x_canvas)->s_name, r->s_name);
	
    string filename = string(this->path->s_name) + "/rolloffs/" + string(this->tableName->s_name) + ".txt";
    
    return filename.c_str();
}

// ***********************************************************
bool vRolloffTable::update()
{
	t_float tmpValue;
	vector<t_float> values;

	// first free the existing tableData if there is any:
	freebytes(this->tableData,this->tableSize*sizeof(t_float));
	this->tableSize = 0;
	
	// special case (default table):
	if (this->tableName == gensym("default"))
	{
		// create some default table data:
		
		t_float defaultData[] = {1, 0.99981, 0.999239, 0.998288, 0.996958, 0.99525, 0.993166, 0.990708, 0.987879, 0.984681, 0.981117, 0.977191, 0.972907, 0.968269, 0.963281, 0.957949, 0.952277, 0.946272, 0.939938, 0.933282, 0.926311, 0.919031, 0.911449, 0.903572, 0.895409, 0.886966, 0.878252, 0.869274, 0.860042, 0.850565, 0.840849, 0.830906, 0.820743, 0.810371, 0.799799, 0.789036, 0.778092, 0.766976, 0.7557, 0.744272, 0.732703, 0.721003, 0.709182, 0.69725, 0.685217, 0.673094, 0.66089, 0.648616, 0.636282, 0.623898, 0.611473, 0.599018, 0.586543, 0.574057, 0.561569, 0.54909, 0.536628, 0.524193, 0.511794, 0.499439, 0.487138, 0.474898, 0.462729, 0.450638, 0.438633, 0.426722, 0.414912, 0.40321, 0.391624, 0.380161, 0.368826, 0.357626, 0.346567, 0.335654, 0.324894, 0.314291, 0.30385, 0.293576, 0.283473, 0.273545, 0.263796, 0.25423, 0.244849, 0.235656, 0.226655, 0.217848, 0.209236, 0.200821, 0.192606, 0.18459, 0.176776, 0.169164, 0.161754, 0.154547, 0.147542, 0.140739, 0.134138, 0.127738, 0.121538, 0.115537, 0.109733, 0.104124, 0.0987091, 0.0934859, 0.0884521, 0.0836053, 0.0789429, 0.0744622, 0.0701603, 0.0660341, 0.0620804, 0.0582959, 0.0546772, 0.0512207, 0.0479228, 0.0447797, 0.0417875, 0.0389425, 0.0362406, 0.0336777, 0.0312499, 0.0289529, 0.0267827, 0.024735, 0.0228058, 0.0209907, 0.0192855, 0.0176862, 0.0161885, 0.0147884, 0.0134815, 0.012264, 0.0111318, 0.0100808, 0.00910727, 0.00820723, 0.00737692, 0.00661264, 0.00591077, 0.00526774, 0.00468011, 0.00414452, 0.0036577, 0.00321647, 0.0028178, 0.00245871, 0.00213636, 0.00184802, 0.00159107, 0.00136299, 0.00116139, 0.000984002, 0.000828655, 0.000693304, 0.000576021, 0.000474987, 0.000388501, 0.000314973, 0.000252925, 0.000200985, 0.000157888, 0.000122474, 9.36829e-05, 7.05518e-05, 5.22126e-05, 3.78865e-05, 2.68825e-05, 1.85905e-05, 1.24788e-05, 8.08839e-06, 5.02895e-06, 2.97313e-06, 1.65165e-06, 8.47948e-07, 3.92642e-07, 1.57903e-07, 5.17707e-08, 1.22918e-08, 1.61902e-09, 0, 0};

		this->tableSize = 181;
		this->tableData = (t_float *) copybytes(defaultData, this->tableSize*sizeof(t_float));
		return true;
	}
		

    // Load the rolloff table file and verify:
	const char *filename = this->getFilename();

    FILE *fd;
    if ( !(fd = fopen(filename, "r")) )
    {
        post("ERROR: failed to load %s. File does not exist or is not a valid rolloff file.", filename);
        return false;
    }
    while ( fscanf(fd, "%f", &tmpValue ) != EOF )
    {
        values.push_back(tmpValue); // push onto vector
    }
    fclose(fd);



	if (values.size() < 1)
	{
		post("ERROR: failed to load %s. Bad data.", filename);
    return false;
		
	} else {
		// the table data is good!
		this->tableData = new t_float[values.size()];
		copy( values.begin(), values.end(), this->tableData );
		this->tableSize = values.size();
	
		return true;
	}
  
}

// ***********************************************************
t_float vRolloffTable::getValue(t_float tableIndex)
{
	// function accepts a tableIndex in the range of [0,1), and returns
	// an interpolated value from the table.
  
	
	// first check that the index is in the correct range:
	if ((tableIndex < 0) || (tableIndex > 1))
		return (t_float) 0.0;

	double indexResidual; // the fraction component of the index that is left over
	double index;

	indexResidual = modf( (tableIndex * (this->tableSize - 1)), (double *) &index);

	// get the table value:
	t_float tableValue = this->tableData[(int)index];
	
	// interpolate between next value in table:
	if (index+1 < this->tableSize)
	{
		tableValue = ((1-indexResidual)*tableValue) + (indexResidual * this->tableData[(int)index+1]);
	}

    //std::cout << "vRolloffTable->get(): tableSize=" << this->tableSize << ", tableIndex=" << tableIndex << ", index=" << index << ", indexResidual=" << indexResidual << ", tableValue=" << tableValue << std::endl;


	return tableValue;

}
