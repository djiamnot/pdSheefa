#ifndef __vAudioRenderer_H
#define __vAudioRenderer_H

#include <regex.h>
#include <set>
#include "pdUtil.h"


class vListener;
class vSoundNode;
class vSoundConn;
class vRolloffTable;

#define DYNAMIC_CREATION_X 50
#define DYNAMIC_CREATION_Y 50
#define DYNAMIC_CREATION_Y_OFFSET 40


// declaration of the pd struct:
typedef struct _vAudioRenderer
{
	t_object x_obj;  // the t_object has to be the 1st entry in the structure
  
	t_canvas *x_canvas; // store the canvas to find relative data (eg, rolloffs)
	
	/* // now global
	std::vector<vListener*>  vListenerList;
	std::vector<vSoundNode*> vSoundNodeList;
 	std::vector<vSoundConn*> vSoundConnList;
 	std::vector<vRolloffTable*> rolloffList;
 	*/
 	
 	// for dynamic patching, we count how many of each object we've created:
 	int dynamicNodeCount;
	int dynamicConnCount;
	
	
	t_symbol *connectFilter;
 	float gainThreshold;
	regex_t connectRegex;
	
	// the refreshFlag notifies the renderer that a connection has been made so
	// traversals for all listener nodes can be done.
 	bool refreshFlag;
 	
 	// keep atom list in memory so not to re-allocate with each computation
 	t_atom coeffAtoms[5];
 	
 	t_outlet *coeff_outlet;
	t_outlet *event_outlet;

} t_vAudioRenderer;


// iterators:
typedef std::vector<vListener*>::iterator listenerIterator;
typedef std::vector<vSoundNode*>::iterator nodeIterator;
typedef std::vector<vSoundConn*>::iterator connIterator;
typedef std::vector<vRolloffTable*>::iterator rolloffIterator;

// predefine helper functions:
static bool nodeSortFunction (vSoundNode *n1, vSoundNode *n2);
static vSoundNode* getNode(t_vAudioRenderer *x, t_symbol *id);
static std::vector<vSoundConn*> getConnectionsForNode(t_vAudioRenderer *x, t_symbol *id);
static vSoundConn* getConnection(t_vAudioRenderer *x, t_symbol *src, t_symbol *snk);

void vAudioRenderer_compute(t_vAudioRenderer *x, vSoundConn* conn);
vSoundNode* vAudioRenderer_createSoundNode (t_vAudioRenderer *x, t_symbol *id);

/*
static void vAudioRenderer_refreshGraph(t_vAudioRenderer *x);
static void vAudioRenderer_refreshGraph(t_vAudioRenderer *x, vListener *listener, vSoundNode *sinkNode);
static void vAudioRenderer_disableGraph(t_vAudioRenderer *x, vSoundConn *conn);
static void vAudioRenderer_disableGraph(t_vAudioRenderer *x, vListener *listener, vSoundConn *conn);
*/

// dynamic patching functions:
t_symbol *vAudioRenderer_getDynamicPatch(t_vAudioRenderer *x, std::string which);
t_symbol *vAudioRenderer_getNodeSubpatch(t_vAudioRenderer *x);
t_symbol *vAudioRenderer_getConnSubpatch(t_vAudioRenderer *x);
void vAudioRenderer_clearDynamicPatch(t_vAudioRenderer *x, std::string which);

#endif
