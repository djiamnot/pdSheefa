// ===================================================================
// Audioscape library for PureData
// Copyright (c) 2007
//
// Collaborators:
//    Shared Reality Lab (SRE), McGill University Centre for Intelligent Machines (CIM)
//       www.cim.mcgill.ca/sre
//    La Société des Arts Technologiques (SAT)
//       www.sat.qc.ca
//
// Project Directors:
//    Science - Jeremy R. Cooperstock (SRE/CIM)
//    Arts - Zack Settel
//
// Conception:
//    Zack Settel
//
// Development Team:
//    Mike Wozniewski (SRE/CIM): Researcher, Head Developer
//    Zack Settel: Artist, Researcher, Audio/DSP programming
//    Jean-Michel Dumas (SAT): Assistant Researcher
//    Mitchel Benovoy (SRE/CIM): Video Texture Programming
//    Stéphane Pelletier (SRE/CIM): Video Texture Programming
//    Pierre-Olivier Charlebois (SRE/CIM): Former Developer
//
// Funding by / Souventionné par:
//    Natural Sciences and Engineering Research Council of Canada (NSERC)
//    Canada Council for the Arts
//    NSERC/Canada Council for the Arts - New Media Initiative
//
// ===================================================================
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// ===================================================================


#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <regex.h>


#include "vAudioRenderer.h"
#include "vListener.h"
#include "vSoundNode.h"
#include "vSoundConn.h"
#include "vRolloffTable.h"

#include "g_canvas.h"



using namespace std;

// declaration of this Pd class:
t_class *vAudioRenderer_class;


static t_symbol *symbol_SoundNode = gensym("SoundNode");
static t_symbol *symbol_SoundConnection = gensym("SoundConnection");
static t_symbol *symbol_NULL = gensym("NULL");


// globally-accessible pointer (to enforce that only one renderer exists):
t_vAudioRenderer *globalAudioRenderer;


// *****************************************************************************
// This is a function that can be used by std::sort to make a
// list of nodes alphabetical:
static bool nodeSortFunction (vSoundNode *n1, vSoundNode *n2)
{
	return ( string(n1->id->s_name) < string(n2->id->s_name) );
}

// *****************************************************************************
// return a pointer to a vListener in the x->vListenerList, given an id:
static vListener* getListener(t_vAudioRenderer *x, t_symbol *id)
{
	listenerIterator L;
	for (L = x->vListenerList.begin(); L != x->vListenerList.end(); L++)
	{
		if ((*L)->node->id == id)
		{
			return (*L);
		}
	}
	
	return NULL;
}

// *****************************************************************************
// return a pointer to a vSoundNode in the x->vSoundNodeList, given an id:
static vSoundNode* getNode(t_vAudioRenderer *x, t_symbol *id)
{
	nodeIterator n;
	for (n = x->vSoundNodeList.begin(); n != x->vSoundNodeList.end(); n++)
	{
		if ((*n)->id == id)
		{
			return (*n);
		}
	}
	
	return NULL;
}

// *****************************************************************************
// return a list of all connections that "directly involve" a node (ie, as the
// source or the sink):
static std::vector<vSoundConn*> getConnectionsForNode(t_vAudioRenderer *x, t_symbol *id)
{
	vector<vSoundConn*> foundConnections;
	
	connIterator c;
	for (c = x->vSoundConnList.begin(); c != x->vSoundConnList.end(); c++)
	{
		if (((*c)->src->id == id) || ((*c)->snk->id == id))
		{
			foundConnections.push_back(*c);
		}
	}
	return foundConnections;
}

// *****************************************************************************
// return a pointer to a vSoundConn in the x->vSoundConnList:
static vSoundConn* getConnection(t_vAudioRenderer *x, t_symbol *src, t_symbol *snk)
{

	connIterator c;
	for (c = x->vSoundConnList.begin(); c != x->vSoundConnList.end(); c++)
	{
		if (((*c)->src->id == src) && ((*c)->snk->id == snk))
		{
			return (*c);
		}
	}
	
	
	/*
	vSoundNode *srcNode = getNode(x,src);
	if ( srcNode )
	{
		connIterator c;
		for (c = srcNode->connectTO.begin(); c != srcNode->connectTO.end(); c++)
		{
			if ((*c)->snk->id == snk) return (*c);
		}
	}
	*/

	return NULL;
}

static vSoundConn* getConnection(t_vAudioRenderer *x, t_symbol *id)
{
	connIterator c;
	for (c = x->vSoundConnList.begin(); c != x->vSoundConnList.end(); c++)
	{
		if ((*c)->id == id)
		{
			return (*c);
		}
	}
	
	return NULL;
}



// *****************************************************************************
void vAudioRenderer_compute(t_vAudioRenderer *x, vSoundConn* conn)
{

	// If this connection is set to "thru", then there is no need to compute any
	// coefficients. The audio signal in Pd will bypass all gains, filters, and
	// delays
	if (conn->thru)
	{
		return;
	}
	
	// If one of the connected nodes has been deactivated, then there is no need
	// to compute anything. Enable the mute (and send the status change if this
	// has just happened)
	if ((!conn->src->active) || (!conn->snk->active))
	{
		if (!conn->mute)
		{
			conn->mute = true;
			SETSYMBOL (x->coeffAtoms+0, gensym("mute"));
			SETFLOAT (x->coeffAtoms+1, 1);
			outlet_anything(x->coeff_outlet, conn->id, 2, x->coeffAtoms);
		}
		return;
	}
	
	
	Vector3 conn_vector = conn->snk->pos - conn->src->pos;
	
	// DISTANCE:
	
	double distance = (double)conn_vector.Mag();
	double distanceScalar = 1 / (1.0 + pow(distance,(double)conn->distanceEffect*.01));
	
	// SIMPLE MUTE MECHANISM (TODO: also mute any connected regions)
	
	if (distanceScalar < x->gainThreshold)
	{
		if (!conn->mute)
		{
			conn->mute = true;
			SETSYMBOL (x->coeffAtoms+0, gensym("mute"));
			SETFLOAT (x->coeffAtoms+1, 1);
			outlet_anything(x->coeff_outlet, conn->id, 2, x->coeffAtoms);
		}
		return;
	}
	
	
	// ROLLOFF: 

	// incidence (radians) between source and the connection_vector:
	Vector3 srcDir = EulerToQuat(conn->src->rot) * Vector3(0,1,0);
	double srcIncidence = AngleBetweenVectors(srcDir, conn_vector);
	double srcIncidenceGain = conn->src->rolloff->getValue( (srcIncidence * conn->src->spread) / M_PI );
	
	
	// incidence (radians) between sink and the connection_vector:
	Vector3 snkDir = EulerToQuat(conn->snk->rot) * Vector3(0,1,0);
	double snkIncidence = AngleBetweenVectors(Vector3(0,0,0)-snkDir, conn_vector);

	if (conn->type == gensym("spin.ListenerConnection"))
	{
		post("    rot: (%.3f,%.3f,%.3f)",conn->snk->rot.x,conn->snk->rot.y,conn->snk->rot.z);
	}
	
	// if connected sink is a listener, IGNORE it (i.e. set the sink's IncidenceGain to unity) since the incidence(s) for the sink's vmic(s) is/are calculated in the connection patch

	double snkIncidenceGain = (conn->type == gensym("spin.ListenerConnection")) ? 1. : conn->snk->rolloff->getValue( (snkIncidence * conn->snk->spread) / M_PI );

	

	double rolloffScaler = (double) (1.0 - (.01*conn->rolloffEffect  * (1.0 - srcIncidenceGain*snkIncidenceGain)));


	
	
	// combined amplitude scalar:
	double G1 = distanceScalar * rolloffScaler; // * 0.00001;
	
	// incidence filter:
	// - range is 500Hz -> 21750 + 500 Hz)
	// - feeds Pd object [lop~ 500]
	double Lpf1 = 500 + ( 21550 * (0.5 - ( .5*cos(pow(rolloffScaler,4)*M_PI) ) ) );
	
	// absorption filter:
	// - function of distance
	// - range is 50Hz -> 22000Hz
	// - feeds Pd object [lop~ 22000]
	double Lpf2 = 22000 - ( 3500*log(distance+1) );
	if (Lpf2 < 50) Lpf2=50;
	if (Lpf2 > 22000) Lpf2=22000;
	
	// variable delay for Doppler:
	// - feeds Pd object [vd~ $0-dop]
	double vdel1 = distance * 2.89855; // speed of sound
	
	
	// If we've gotten this far, then make sure that the mute is not on:
	if (conn->mute)
	{
		conn->mute = false;
		SETSYMBOL (x->coeffAtoms+0, gensym("mute"));
		SETFLOAT (x->coeffAtoms+1, 0);
		outlet_anything(x->coeff_outlet, conn->id, 2, x->coeffAtoms);
	}
	
	// Now output coeffs:
							   
	SETFLOAT (x->coeffAtoms+0, (t_float) G1);
	SETFLOAT (x->coeffAtoms+1, (t_float) Lpf1);
	SETFLOAT (x->coeffAtoms+2, (t_float) Lpf2);
	SETFLOAT (x->coeffAtoms+3, (t_float) vdel1);
							   
	if (conn->type == gensym("spin.ListenerConnection"))
	{
		double snkAzim = AngleBetweenVectors(snkDir, conn_vector, 3);
		double snkRoll = AngleBetweenVectors(snkDir, conn_vector, 2);
		double snkElev = AngleBetweenVectors(snkDir, conn_vector, 1);

		SETFLOAT (x->coeffAtoms+4, (t_float) snkAzim);
		SETFLOAT (x->coeffAtoms+5, (t_float) snkElev);
		SETFLOAT (x->coeffAtoms+6, (t_float) snkRoll);
		SETFLOAT (x->coeffAtoms+6, (t_float) distance);
		outlet_anything(x->coeff_outlet, conn->id, 7, x->coeffAtoms);
	}
	else 		
		outlet_anything(x->coeff_outlet, conn->id, 4, x->coeffAtoms);
}


// *****************************************************************************
// dynamic patching functions:
// *****************************************************************************

t_symbol *vAudioRenderer_getDynamicPatch(t_vAudioRenderer *x, std::string which)
{
	t_symbol *dollarZero = canvas_realizedollar(x->x_canvas, gensym("$0"));
	t_symbol *dynamicPatch = gensym((char*)("pd-" + string(dollarZero->s_name) + "-" + which).c_str());
	

	// Check to see if the subpatcher exists:
	if (dynamicPatch->s_thing)
	{
		// Found the patch! just return the symbol
		
	} else {
		// If not, then we try to create it directly underneath the current
		// object by reading the x,y position from the t_object...

		t_object *thisObj = &(x->x_obj);
		
		int offset = 20;
		if (which=="SoundConnections") offset += 20;
		
		string creationString = stringify((int)thisObj->te_xpix) + " " + stringify((int)thisObj->te_ypix+offset) + " DynamicPatchHolder \\$0-" + which;
		
		int atomCount;
		t_atom *atomList = atomsFromString(creationString, atomCount);

		// send message to "host" canvas to instantiate subpatch:
		typedmess((_class **) x->x_canvas, gensym("obj"), atomCount, atomList); 

		pdMesg(dynamicPatch, gensym("vis"), "0");
		
		vAudioRenderer_clearDynamicPatch(x, which);
	}

	return dynamicPatch;
}

t_symbol *vAudioRenderer_getNodeSubpatch(t_vAudioRenderer *x)
{
	return vAudioRenderer_getDynamicPatch(x, "SoundNodes");
}

t_symbol *vAudioRenderer_getConnSubpatch(t_vAudioRenderer *x)
{
	return vAudioRenderer_getDynamicPatch(x, "SoundConnections");
}

void vAudioRenderer_clearDynamicPatch(t_vAudioRenderer *x, std::string which)
{
	t_symbol *subpatch = vAudioRenderer_getDynamicPatch(x, which);
	
	pdMesg(subpatch, gensym("clear"), "0");
	
	// create a [cnv], warning users to not edit the patch:
	pdMesg(subpatch, gensym("obj"), string("0 0 cnv 10 450 40 empty empty WARNING:__THIS__IS__A__DYNAMIC__PATCH!___ 10 10 1 16 -261234 -66577 0"));
	pdMesg(subpatch, gensym("obj"), string("10 23 cnv 10 1 1 empty empty ...DO-NOT.directly.edit.this_and_DO-NOT.move.objects! 0 5 0 14 -233017 -66577 0"));

	if (which=="SoundNodes") x->dynamicNodeCount = 0;
	if (which=="SoundConnections") x->dynamicConnCount = 0;
	
	pdMesg(subpatch, gensym("vis"), "0");
}



// *****************************************************************************
// *** PD OBJECT METHODS:
// *****************************************************************************


// *****************************************************************************
static void vAudioRenderer_outputNodeList (t_vAudioRenderer *x)
{
	t_atom *atomList;

	if (x->vSoundNodeList.size())
	{
		atomList = (t_atom*) getbytes(x->vSoundNodeList.size() * sizeof(t_atom));
		
		for (int i=0; i<x->vSoundNodeList.size(); i++)
		{
			SETSYMBOL (atomList+i, x->vSoundNodeList[i]->id);
		}
		outlet_anything(x->event_outlet, gensym("nodeList"), x->vSoundNodeList.size(), atomList);
	}
	
	else 
	{
		atomList = (t_atom*) getbytes(sizeof(t_atom));
		SETSYMBOL (atomList, gensym("NULL"));
		outlet_anything(x->event_outlet, gensym("nodeList"), 1, atomList);
	}
}

static void vAudioRenderer_outputConnList (t_vAudioRenderer *x)
{
	t_atom *atomList;

	if (x->vSoundConnList.size())
	{
		atomList = (t_atom*) getbytes(x->vSoundConnList.size() * sizeof(t_atom));
		
		for (int i=0; i<x->vSoundConnList.size(); i++)
		{
			SETSYMBOL (atomList+i, x->vSoundConnList[i]->id);
		}
		outlet_anything(x->event_outlet, gensym("connections"), x->vSoundConnList.size(), atomList);
	}
	
	else 
	{
		atomList = (t_atom*) getbytes(sizeof(t_atom));
		SETSYMBOL (atomList, gensym("NULL"));
		outlet_anything(x->event_outlet, gensym("connections"), 1, atomList);
	}
}


static void vAudioRenderer_bang (t_vAudioRenderer *x)
{
	
	// compute a connection if updateFlag is set on any node:
	for (connIterator c = x->vSoundConnList.begin(); c != x->vSoundConnList.end(); c++)
	{
		if (((*c)->src->updateFlag) || ((*c)->snk->updateFlag))
		{
			vAudioRenderer_compute(x,(*c));
		}
	}
	
	// reset all update flags:
	for (nodeIterator n = x->vSoundNodeList.begin(); n != x->vSoundNodeList.end(); n++)
	{
		(*n)->updateFlag = false;
	}
	
}

static void vAudioRenderer_connect (t_vAudioRenderer *x, t_symbol *src, t_symbol *snk)
{

	// check if exists first:
	vSoundConn* conn = getConnection(x,src,snk);
	if (!conn) {
		
		// Check src and snk against the connectFilter. If either match, then 
		// proceed with the connection:
		int srcRegexStatus = regexec(&x->connectRegex, src->s_name, (size_t)0, NULL, 0);
		int snkRegexStatus = regexec(&x->connectRegex, snk->s_name, (size_t)0, NULL, 0);
			
		if ((srcRegexStatus==0) || (snkRegexStatus==0))
		{
			
			// check if both nodes exist
			vSoundNode *srcNode = getNode(x,src);
			vListener *listener = getListener(x,snk);
			vSoundNode *snkNode;
			
			if (listener) snkNode = listener->node;
			else snkNode = getNode(x,snk);
			

			if (srcNode && snkNode)
			{
				// create connection:
				conn = new vSoundConn(srcNode, snkNode);

				// register the connection with both the AudioRenderer and the 
				// sink node (for backwards connectivity computation):
				x->vSoundConnList.push_back(conn);
				snkNode->connectFROM.push_back(conn);

				// if this is a listener connection, set the proper type:
				if (listener)
				{
					conn->type = gensym("spin.ListenerConnection");
				}

				// dynamically create the Pd object:
				conn->createPdObj(x);
				
				t_atom *atomList = (t_atom*) getbytes(2*sizeof(t_atom));
				
				// send the listenerType if needed:
				if (listener)
				{
					SETSYMBOL (atomList+0, gensym("setType"));
					SETSYMBOL (atomList+1, listener->type);
					outlet_anything(x->coeff_outlet, conn->id, 2, atomList);
				}
				
				// send an event from the event_outlet:
				SETSYMBOL (atomList+0, src);
				SETSYMBOL (atomList+1, snk);
				outlet_anything(x->event_outlet, gensym("connect"), 2, atomList);
				
				vAudioRenderer_outputConnList(x);
			}
		}
	}
}

static void vAudioRenderer_disconnect (t_vAudioRenderer *x, t_symbol *src, t_symbol *snk)
{
	connIterator c;
	for (c = x->vSoundConnList.begin(); c != x->vSoundConnList.end(); c++)
	{
		if ( ((*c)->src->id==src) && ((*c)->snk->id==snk) )
		{
			// grab a pointer to the actual connection:
			vSoundConn *conn = (*c);
			
			// remove it from the list:
			x->vSoundConnList.erase(c);	
			
			// delete the Pd object:
			conn->deletePdObj(x);
			
			// send an event from the event_outlet:
			t_atom *atomList = (t_atom*) getbytes(2*sizeof(t_atom));
			SETSYMBOL (atomList+0, conn->src->id);
			SETSYMBOL (atomList+1, conn->snk->id);
			outlet_anything(x->event_outlet, gensym("disconnect"), 2, atomList);
			
			// delete the vSoundConn instance:
			delete conn;	
			
			vAudioRenderer_outputConnList(x);
			
			break;
		}
	}
	
}

vListener* vAudioRenderer_createListener (t_vAudioRenderer *x, t_symbol *id)
{
	// ignore "NULL"
	if (id==gensym("NULL")) return NULL;
	
	// check if it already exists:
	vListener *L = getListener(x,id);

	if (!L)
	{
		// if not, create a new vListener:
		L = new vListener(x, id);
		
		// add it to the x->vListenerList:
		x->vListenerList.push_back(L);
	}
	
	return L;
}

vSoundNode* vAudioRenderer_createSoundNode (t_vAudioRenderer *x, t_symbol *id)
{
	// ignore "NULL"
	if (id==gensym("NULL")) return NULL;
	
	// check if it already exists:
	vSoundNode *n = getNode(x,id);

	if (!n) {
	
		// if not, create a new vSoundNode:
		n = new vSoundNode(id);
		
		// give it one of the rolloffs living in the shared list:
		n->setRolloff(x, gensym("default"));
		
		// add it to the x->vSoundNodeList:
		x->vSoundNodeList.push_back(n);
		
		// create the Pd patch:
		n->createPdObj(x);
		
		// send a createNode event from the event_outlet:
		t_atom *atomList = (t_atom*) getbytes(3*sizeof(t_atom));
		SETSYMBOL (atomList+0, n->id);
		SETSYMBOL (atomList+1, gensym("SoundNode"));
		outlet_anything(x->event_outlet, gensym("createNode"), 2, atomList);
		
		vAudioRenderer_outputNodeList(x);
		
	}
	
	return n;
}

static void vAudioRenderer_createNode (t_vAudioRenderer *x, t_symbol *id, t_symbol *type)
{
	if (type==gensym("SoundNode")) vAudioRenderer_createSoundNode(x,id);
	if (type==gensym("Listener")) vAudioRenderer_createListener(x,id);
}


static void vAudioRenderer_deleteNode (t_vAudioRenderer *x, t_symbol *id)
{
	// first check if the node is a listener and delete that first:
	vListener *listener = getListener(x,id);
	vSoundNode *node;
	
	if (listener)
	{
		node = listener->node;
		for (listenerIterator L = x->vListenerList.begin(); L != x->vListenerList.end(); L++)
		{
			if ((*L)==listener)
			{
				x->vListenerList.erase(L);
				break;
			}
		}
		delete listener;	
	}
	
	else
	{
		node = getNode(x,id);
	}
	
	
	if (node)
	{

		// remove all connections that this node is part of:
		std::vector<vSoundConn*> connections = getConnectionsForNode(x, id);
		for (connIterator c = connections.begin(); c != connections.end(); c++)
		{
			vAudioRenderer_disconnect(x,(*c)->src->id, (*c)->snk->id);
		}
		
		//  delete the actual pd object:
		node->deletePdObj(x);
		
		// send an event (before it's actually deleted):
		t_atom a;
		SETSYMBOL (&a, node->id);
		outlet_anything(x->event_outlet, gensym("deleteNode"), 1, &a);
		
		
		// remove node from the x->vSoundNodeList:
		nodeIterator n;
		for (n = x->vSoundNodeList.begin(); n != x->vSoundNodeList.end(); n++)
		{
			if ((*n) == node)
			{
				x->vSoundNodeList.erase(n);
				delete node;
				break;
			}
		}
		
		vAudioRenderer_outputNodeList(x);

	}
}
	
static void vAudioRenderer_clear(t_vAudioRenderer *x)
{
	// clear connections before nodes:
	while (x->vSoundConnList.size())
	{
		vAudioRenderer_disconnect(x, x->vSoundConnList[0]->src->id, x->vSoundConnList[0]->snk->id);
	}
	
	// delete nodes (NOTE: this includes listeners) 
	while (x->vListenerList.size())
	{
		vAudioRenderer_deleteNode(x, x->vListenerList[0]->node->id);
	}
	
	vAudioRenderer_clearDynamicPatch(x, "SoundNodes");
	vAudioRenderer_clearDynamicPatch(x, "SoundConnections");
	
	vAudioRenderer_outputNodeList(x);
	vAudioRenderer_outputConnList(x);
}


// *****************************************************************************
static void vAudioRenderer_debug (t_vAudioRenderer *x)
{
	listenerIterator L;
	nodeIterator n;
	connIterator c;
	
	vector<vSoundNode*> listenerNodes;

	post("\n=====================================================");
	
	post("[vAudioRenderer]:: connectFilter = %s", x->connectFilter->s_name);
	post("[vAudioRenderer]:: gainThreshold = %.2f", x->gainThreshold);
	post("[vAudioRenderer]:: %d listeners:", x->vListenerList.size());
	for (L = x->vListenerList.begin(); L != x->vListenerList.end(); L++)
	{
		post("  %s:", (*L)->node->id->s_name);
		post("    pos: (%.3f,%.3f,%.3f)",(*L)->node->pos.x,(*L)->node->pos.y,(*L)->node->pos.z);
		post("    rot: (%.3f,%.3f,%.3f)",(*L)->node->rot.x,(*L)->node->rot.y,(*L)->node->rot.z);
		post("    active:\t%d", (*L)->node->active);
		post("    plugin:\t%s", (*L)->node->plugin->s_name);
		startpost("    listening to:");
		for (c = (*L)->node->connectFROM.begin(); c != (*L)->node->connectFROM.end(); c++)
		{
			poststring((*c)->src->id->s_name);
		}
		endpost();
		
		listenerNodes.push_back((*L)->node);
	}
	
	post("[vAudioRenderer]:: %d nodes:", x->vSoundNodeList.size() - x->vListenerList.size());
	for (n = x->vSoundNodeList.begin(); n != x->vSoundNodeList.end() ; n++)
	{
		// only nodes that are not listenerNodes (above):
		if (find(listenerNodes.begin(),listenerNodes.end(),*n) == listenerNodes.end())
		{
			post("  %s:", (*n)->id->s_name);
			post("    pos: (%.3f,%.3f,%.3f)",(*n)->pos.x,(*n)->pos.y,(*n)->pos.z);
			post("    rot: (%.3f,%.3f,%.3f)",(*n)->rot.x,(*n)->rot.y,(*n)->rot.z);
			post("    active:\t%d", (*n)->active);
			post("    plugin:\t%s", (*n)->plugin->s_name);
			post("    rolloff:\t%s", (*n)->rolloff->tableName->s_name);
			post("    spread:\t%.3f", (*n)->spread);
			post("    length:\t%.3f", (*n)->length);
		}
	}

	post("[vAudioRenderer]:: %d connections:", x->vSoundConnList.size());
	for (c = x->vSoundConnList.begin(); c != x->vSoundConnList.end(); c++)
	{
		post("  %s:", (*c)->id->s_name);
		post("    type:\t%s", (*c)->type->s_name);
		post("    thru:\t%d", (*c)->thru);
		post("    mute:\t%d", (int)(*c)->mute);
		post("    distanceEffect:\t%.1f", (*c)->distanceEffect);
		post("    rolloffEffect:\t%.1f", (*c)->rolloffEffect);
		post("    dopplerEffect:\t%.1f", (*c)->dopplerEffect);
		post("    diffractionEffect:\t%.1f", (*c)->diffractionEffect);
	}
	
}

static void vAudioRenderer_setGainThreshold (t_vAudioRenderer *x, t_floatarg f)
{
	x->gainThreshold = f;
	
	// Now set update flags for every connected source node (which will result
	// in an update for every connection):
	connIterator c;
	for (c = x->vSoundConnList.begin(); c != x->vSoundConnList.end(); c++)
	{
		(*c)->src->updateFlag = true;
	}
}

static void vAudioRenderer_setConnectFilter (t_vAudioRenderer *x, t_symbol *s)
{
	// we like specifying just one asterisk ( * ), so we need to convert to a
	// regular expression:
	if (s==gensym("*")) s = gensym(".*");

	if (regcomp(&x->connectRegex, s->s_name, REG_EXTENDED|REG_NOSUB) != 0)
	{
		post("vAudioRenderer error: bad regex pattern passed to setConnectFilter(): %s ", s->s_name);
	    return;
	}
	
	x->connectFilter = s;
}


// *****************************************************************************
static void vAudioRenderer_nodeList (t_vAudioRenderer *x, t_symbol *s, int argc, t_atom *argv)
{
	// there need to be at least 2 args in order to be a legal message:
	// argv[0] is the nodeType
	// argv[1+] is the list of nodes or argv[1] will be "NULL" if list is empty
	
	if (argc<2) return;
	
	if (argv[0].a_type == A_SYMBOL)
	{
		if (argv[0].a_w.w_symbol==symbol_SoundNode)
		{
			for (int i=1; i<argc; i++)
			{
				if (argv[i].a_w.w_symbol != symbol_NULL)
					vAudioRenderer_createNode(x, argv[i].a_w.w_symbol, symbol_SoundNode);
			}
		}
		/*
		else if (argv[0].a_w.w_symbol==symbol_SoundConnection)
		{
			if (argv[i].a_w.w_symbol != symbol_NULL)
			{
				// a connection symbol is in the form "src-snk.conn", so we will
				// need to break it up to do the connection
				connectionString = string(argv[i].a_w.w_symbol->s_name);
				string srcName = ??
				string snkName = ??
				vAudioRenderer_connect (x, gensym(srcName.c_str()), gensym(snkName.c_str()));
			}
		}
		*/
	}
}


// *****************************************************************************
static void vAudioRenderer_call (t_vAudioRenderer *x, t_symbol *s, int argc, t_atom *argv)
{
	/*
	startpost("Got call message:");
	postatom(argc, argv);
	endpost();
	*/
	
	// argv[0]: node id
	// argv[1]: method
	// argv[2+]: method args
	
	// there need to be at least 2 args in order to be a legal message:
	if (argc<2) return;
	
	// the first 2 args also need to be symbolic:
	if ((argv[0].a_type != A_SYMBOL) || (argv[1].a_type != A_SYMBOL)) return;
	
	string method = argv[1].a_w.w_symbol->s_name;
	
	// see if we are interested in this node or listener:
	
	vListener *listener = getListener(x,argv[0].a_w.w_symbol);
	vSoundNode *node;
	
	if (listener)
	{
		if (method=="setType")
		{
			listener->type = atom_getsymbol(argv+2);
			listener->refreshConnections(x);
		}
		
		node = listener->node;
	}
	else
	{
		node = getNode(x,argv[0].a_w.w_symbol);
	}
	
	
	if (node)
	{
		// now apply the method:
		
		if (method=="global6DOF")
		{
			node->pos = Vector3(atom_getfloat(argv+2),atom_getfloat(argv+3),atom_getfloat(argv+4));
			node->rot = Vector3(atom_getfloat(argv+5),atom_getfloat(argv+6),atom_getfloat(argv+7));
			node->updateFlag=true;
		}
		else if (method=="setActive")
		{
			node->active = atom_getfloat(argv+2);
			node->updateFlag=true;
		}
		else if (method=="setPlugin") node->plugin = atom_getsymbol(argv+2);
		else if (method=="setRolloff")
		{
			node->setRolloff(x, atom_getsymbol(argv+2));
			node->updateFlag=true;
		}
		else if (method=="setSpread")
		{
			node->spread = atom_getfloat(argv+2);
			node->updateFlag=true;
		}
		else if (method=="setLength")
		{
			node->length = atom_getfloat(argv+2);
			node->updateFlag=true;
		}
		else if (method=="connect") vAudioRenderer_connect(x,node->id,atom_getsymbol(argv+2));
		else if (method=="disconnect") vAudioRenderer_disconnect(x,node->id,atom_getsymbol(argv+2));
		
	} else {
		
		vSoundConn *conn = getConnection(x,argv[0].a_w.w_symbol);
		if (conn)
		{
			if (method=="thru") conn->thru = (int) atom_getfloat(argv+2);
			else if (method=="setDistanceEffect") conn->distanceEffect = atom_getfloat(argv+2);
			else if (method=="setRolloffEffect") conn->rolloffEffect = atom_getfloat(argv+2);
			else if (method=="setDopplerEffect") conn->dopplerEffect = atom_getfloat(argv+2);
			else if (method=="setDiffractionEffect") conn->diffractionEffect = atom_getfloat(argv+2);

			// force update
			conn->snk->updateFlag = true;
		}
	}

}


// *****************************************************************************
// *** CONSTRUCTOR:
// *****************************************************************************

static void *vAudioRenderer_new (t_symbol *idString)
{
	if (globalAudioRenderer) return (void *) t_vAudioRenderer;
	
	t_vAudioRenderer *x = (t_vAudioRenderer *) pd_new (vAudioRenderer_class);

	x->vListenerList.clear();
	x->vSoundNodeList.clear();
	x->vSoundConnList.clear();
	
	// Get the canvas on which this Pd object resides. NOTE: this must be done
	// in the constructor, otherwise canvas_getcurrent() returns garbage.
	x->x_canvas = canvas_getcurrent();
	
	x->dynamicNodeCount = 0;
	x->dynamicConnCount = 0;
	
	vAudioRenderer_setConnectFilter(x, gensym(".*")); // match everything
	
	x->gainThreshold = 0.004;
	
	x->coeff_outlet = outlet_new(&x->x_obj, &s_list);
	x->event_outlet = outlet_new(&x->x_obj, &s_list);
	
	globalAudioRenderer = x;
	return (void *) x;
}


// ***********************************************************
// ********************** DESTRUCTOR: ************************
// ***********************************************************
static void vAudioRenderer_free (t_vAudioRenderer * x)
{
	vAudioRenderer_clear(x);
	regfree(&(x->connectRegex));
}

// ***********************************************************
// ******************* PD SETUP FUNCTION: ********************
// ***********************************************************
extern "C" void vAudioRenderer_setup (void)
{
 

	// Now we create a new Pd class:
	vAudioRenderer_class = class_new(gensym("vAudioRenderer"), (t_newmethod)vAudioRenderer_new, (t_method)vAudioRenderer_free, sizeof(t_vAudioRenderer), CLASS_DEFAULT, A_DEFSYMBOL, 0);
	// Note: for some reason, setting up new Pd methods and classes with zero
	// arguments won't work. For example, class_new() and class_addmethod()
	// needs to have at least one A_DEFLOAT or something in order to instantiate
	// properly. why?? because of c++? ????

	// Declare all Pd method handlers:
	class_addbang(vAudioRenderer_class, (t_method)vAudioRenderer_bang);

	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_createSoundNode, gensym("createSoundNode"), A_DEFSYMBOL, 0);
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_createListener, gensym("createListener"), A_DEFSYMBOL, 0);
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_createNode, gensym("createNode"), A_DEFSYMBOL, A_DEFSYMBOL, 0);
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_deleteNode, gensym("deleteNode"), A_DEFSYMBOL, 0);
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_clear, gensym("clear"), A_DEFFLOAT, 0);
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_debug, gensym("debug"), A_DEFFLOAT, 0);

	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_setGainThreshold, gensym("setGainThreshold"), A_DEFFLOAT, 0);

	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_setConnectFilter, gensym("setConnectFilter"), A_DEFSYMBOL, 0);

	
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_nodeList, gensym("nodeList"), A_GIMME, 0);

	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_call, gensym("call"), A_GIMME, 0);
	//class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_connMsg, gensym("connMsg"), A_GIMME, 0);

}
