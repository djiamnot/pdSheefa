#include "vAudioRenderer.h"
#include "vSoundNode.h"
#include "vSoundConn.h"

#include "g_canvas.h"

using namespace std;

// *****************************************************************************

vSoundConn::vSoundConn(vSoundNode *src, vSoundNode *snk) {
	this->id = gensym( (string(src->id->s_name) + "-" + string(snk->id->s_name) + ".conn" ).c_str() );
	this->src=src;
	this->snk=snk;
	thru = 0;
	mute = true;
	distanceEffect = 100.0;
	rolloffEffect = 100.0;
	dopplerEffect = 100.0;
	diffractionEffect = 100.0;
	
	type = gensym("spin.SoundConnection");
	
	obj_x = -1;
	obj_y = -1;
	
	// set updateFlag on at least one of the nodes for initial computation:
	src->updateFlag = true;

}


vSoundConn::~vSoundConn()
{
	// destructor
}

// *****************************************************************************

void vSoundConn::createPdObj(t_vAudioRenderer *x)
{
	// First check if the object already exists. This can be done by looking in
	// the <id>.patch symbol to see if the s_thing attribute is set.
	// If so, it means that the required [namecanvas] instance is there.

	
	t_symbol *objRecv = gensym( (char *) (string(this->id->s_name)+".patch").c_str() );
	
	if (!(objRecv->s_thing))
	{			
		// the Pd object dosn't exist, so create it:
		
		t_symbol *dynamicPatch = vAudioRenderer_getDynamicPatch(x, "SoundConnections");

		this->obj_x = DYNAMIC_CREATION_X;
		this->obj_y = DYNAMIC_CREATION_Y + (x->dynamicConnCount * DYNAMIC_CREATION_Y_OFFSET);
		
		t_symbol *dollarZero = canvas_realizedollar(x->x_canvas, gensym("$0"));
		
		pdMesg(dynamicPatch, gensym("obj"), stringify((int)obj_x) + " " + stringify((int)obj_y) + " " + type->s_name + " " + dollarZero->s_name + " " + src->id->s_name + " " + snk->id->s_name + " " + id->s_name);
		x->dynamicConnCount++;
		
		// The created object MUST have a [namecanvas] object, which creates a
		// receiver by the name: <id>.patch
		
		// We now send a loadbang message to the abstraction (since loadbangs do
		// NOT get generated for dynamic objects)
		pdMesg(objRecv, gensym("loadbang"), "");
		
		// send initial 'mute' state so patch can do things:
		SETSYMBOL (x->coeffAtoms+0, gensym("mute"));
		SETFLOAT (x->coeffAtoms+1, (int)this->mute);
		outlet_anything(x->coeff_outlet, this->id, 2, x->coeffAtoms);
					
		// compute once:
		vAudioRenderer_compute(x, this);			
	}
	
}
 
void vSoundConn::deletePdObj(t_vAudioRenderer *x)
{
	t_symbol *objRecv = gensym( (char *) (string(this->id->s_name)+".patch").c_str() );
	
	// try to ensure that object exists before we try a delete:
	if ( (obj_x>=0) && (obj_y>=0) && (objRecv->s_thing) )
	{
		t_symbol *dynamicPatch = vAudioRenderer_getDynamicPatch(x, "SoundConnections");
		DELETE_PD_OBJ_AT_COORDS(dynamicPatch, obj_x, obj_y);

	}
}