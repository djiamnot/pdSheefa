#ifndef __vAudioManager_H
#define __vAudioManager_H

#include <vector>
#include "m_pd.h"

// declare the classes we need:
class vListener;
class vSoundNode;
class vSoundConn;
class vRolloffTable;


// iterators:
typedef std::vector<vListener*>::iterator listenerIterator;
typedef std::vector<vSoundNode*>::iterator nodeIterator;
typedef std::vector<vSoundConn*>::iterator connIterator;
typedef std::vector<vRolloffTable*>::iterator rolloffIterator;

static bool nodeSortFunction (vSoundNode *n1, vSoundNode *n2);

class vAudioManager
{
	
	public:
		
		vAudioManager();
		~vAudioManager();

		vListener* getListener(t_symbol *id);
		vSoundNode* getNode(t_symbol *id);
		std::vector<vSoundConn*> getConnectionsForNode(t_symbol *id);
		vSoundConn* getConnection(t_symbol *src, t_symbol *snk);
		vSoundConn* getConnection(t_symbol *id);
		vRolloffTable* getRolloff (t_symbol *id);
		
	
		
		
		std::vector<vListener*>  vListenerList;
		std::vector<vSoundNode*> vSoundNodeList;
	 	std::vector<vSoundConn*> vSoundConnList;
	 	std::vector<vRolloffTable*> vRolloffList;

};
		
#endif
