#ifndef __vListener_H
#define __vListener_H

#include "vAudioRenderer.h"

class vListener
{
	
public:
	
	vListener(t_vAudioRenderer *x, t_symbol *id);
	~vListener();
	
	void refreshConnections(t_vAudioRenderer *x);
	
	vSoundNode *node;
	
	t_symbol *type;
	
};


#endif