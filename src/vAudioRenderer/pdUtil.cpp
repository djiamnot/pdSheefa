// ===================================================================
// Dynamic Patching library for Pd
// Maintained by Mike Wozniewski (mike@mikewoz.com)
//
// ===================================================================
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// ===================================================================


#include "pdUtil.h"

#include <vector>
#include <string>
#include <sstream>
#include <iostream>

using namespace std;

std::string stringify(int i)
{
	std::ostringstream o;
	if (!(o << i)) return "";
	return o.str();
}

std::string stringify(float x)
{
	std::ostringstream o;
	if (!(o << x)) return "";
	return o.str();
}

// ***********************************************************
t_atom *atomsFromString (string theString, int &count)
{
	// This function takes an std::string and uses spaces to
	// tokenize the string into a array of Pd atoms. Some basic
	// type checking is done - if we can determine that the word
	// is a float, then we create a float atom, otherwise the
	// atom is assumed to be a symbol.

	t_atom *atomList;
	
	// using binbuf from Pd:

	t_binbuf *b;
	if (theString.size())
	{
		b = binbuf_new();
		binbuf_text(b, (char*) theString.c_str(), theString.size());
		count = binbuf_getnatom(b);
		atomList = (t_atom *) copybytes(binbuf_getvec(b), sizeof(*binbuf_getvec(b)) * count);
		binbuf_free(b);
	}
	
	if (count) return atomList;
	else return NULL;
	
}


// ***********************************************************
vector<string> tokenize(const string& str, const string& delimiters)
{
	vector<string> tokens;
	
	// skip delimiters at beginning:
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// find first "non-delimiter":
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	if (lastPos == string::npos)
	{
		// this is an empty string, so return empty vector:
		return tokens;
	}
	
	else if (pos == string::npos)
	{
		// no delimiter could be found (ie, there is just one token)
		tokens.push_back(str);
		return tokens;
	}

	else {
		while (string::npos != pos || string::npos != lastPos)
		{
			// found a token, add it to the vector:
			tokens.push_back(str.substr(lastPos, pos - lastPos));
			// skip delimiters (Note the "not_of"):
			lastPos = str.find_first_not_of(delimiters, pos);
			// find next "non-delimiter":
			pos = str.find_first_of(delimiters, lastPos);
		}	
		return tokens;
	}
}

// ***********************************************************
vector<t_float> floatsFromString (string theString)
{
	// This function takes an std::string and uses spaces to
	// tokenize the string into a vector of t_floats. If the
	// tokens are symbolic instead of numeric, they are ignored.
	
	vector<string> in_Tokens = tokenize(theString);
	vector<t_float> out_Tokens;
	t_float num;
  
	for (unsigned int i = 0; i < in_Tokens.size(); i++)
	{
		// only add to vector if token is a number:
		if (fromString<float>(num, in_Tokens[i])) out_Tokens.push_back(num);
		//if (fromString(num, in_Tokens[i])) out_Tokens.push_back(num);
	}

	return out_Tokens;
}

void pdMesg(t_symbol *target, t_symbol *operation, std::string s)
{
	// check that the receiver exists, and send using Pd's typedmess function:
	if (target->s_thing)
	{
		t_atom *atoms;
		int atomCount = 0;

		//std::cout << "operation: " << operation->s_name << " (" << s << ")" << std::endl;
		
		if (s.size())
		{
			atoms = atomsFromString(s, atomCount); // note atomCount is set by reference here
		}
		
		if (atomCount) typedmess(target->s_thing, operation, atomCount, atoms);
		else typedmess(target->s_thing, operation, 0, NULL);
		
	} else post ("Warning, there are no Pd receive element with name %s", target->s_name);
}