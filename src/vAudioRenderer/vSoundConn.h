#ifndef __vSoundConn_H
#define __vSoundConn_H

#include "vAudioRenderer.h"
#include "vSoundNode.h"

class vSoundConn
{
	
public:
	
	vSoundConn(vSoundNode *src, vSoundNode *snk);
	~vSoundConn();
	
	t_symbol *id;
	
	vSoundNode *src;
	vSoundNode *snk;
	
	t_symbol *type;
	
	// for dynamic patching:
	t_int obj_x;
	t_int obj_y;
	
	bool mute; // internal mute when gain is below gainThreshold

	t_int thru;
	t_float distanceEffect;
	t_float rolloffEffect;
	t_float dopplerEffect;
	t_float diffractionEffect;
	
	
	void createPdObj(t_vAudioRenderer *x);
	void deletePdObj(t_vAudioRenderer *x);
	
};

#endif