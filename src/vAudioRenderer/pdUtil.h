// ===================================================================
// Dynamic Patching library for Pd
// Maintained by Mike Wozniewski (mike@mikewoz.com)
//
// ===================================================================
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// ===================================================================


#ifndef __pdUtil_H
#define __pdUtil_H

#include "m_pd.h"

#include <vector>
#include <string>
#include <iostream>


t_atom *atomsFromString (std::string theString, int &count);


std::vector<std::string> tokenize(const std::string& str, const std::string& delimiters = " ");
std::vector<t_float> floatsFromString (std::string theString);

void pdMesg(t_symbol *target, t_symbol *operation, std::string s);

// convert from INT -> STRING
std::string stringify(int i);

// convert from FLOAT -> STRING
std::string stringify(float x);

// convert from STRING -> FLOAT (or other type)
template <class T> bool fromString(T& t, const std::string& s)
{
	std::istringstream iss(s);
	return !(iss >> t).fail();
}

// Here is a macro do delete an object on a patch, at coordinates (x,y). We do
// this by simulating a mouse click and then sending the 'cut' message. Note
// however that we have to make the patch visible and put the patch into edit
// mode first. We hide and revert back to run mode afterwards (hopefully that
// won't bother anyone):
#define DELETE_PD_OBJ_AT_COORDS(patch, x, y) \
		pdMesg(patch, gensym("vis"), "1"); \
		pdMesg(patch, gensym("editmode"), "1"); \
		pdMesg(patch, gensym("mouse"), stringify((int)x)+" "+stringify((int)y)+" 0 0"); \
		pdMesg(patch, gensym("cut"), ""); \
		pdMesg(patch, gensym("vis"), "0"); \
		pdMesg(patch, gensym("editmode"), "0");
/*
#define DELETE_PD_OBJ_AT_COORDS(patch, x, y) \
		pdMesg(patch, gensym("vis"), "1"); \
		pdMesg(patch, gensym("editmode"), "1"); \
		pdMesg(patch, gensym("mouse"), stringify((int)x)+" "+stringify((int)y)+" 0 0"); \
		pdMesg(patch, gensym("cut"), ""); \
		pdMesg(patch, gensym("editmode"), "0");
*/
#endif