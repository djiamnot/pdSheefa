// ===================================================================
// Audioscape library for PureData
// Copyright (c) 2007
//
// Collaborators:
//    Shared Reality Lab (SRE), McGill University Centre for Intelligent Machines (CIM)
//       www.cim.mcgill.ca/sre
//    La Société des Arts Technologiques (SAT)
//       www.sat.qc.ca
//
// Project Directors:
//    Science - Jeremy R. Cooperstock (SRE/CIM)
//    Arts - Zack Settel
//
// Conception:
//    Zack Settel
//
// Development Team:
//    Mike Wozniewski (SRE/CIM): Researcher, Head Developer
//    Zack Settel: Artist, Researcher, Audio/DSP programming
//    Jean-Michel Dumas (SAT): Assistant Researcher
//    Mitchel Benovoy (SRE/CIM): Video Texture Programming
//    Stéphane Pelletier (SRE/CIM): Video Texture Programming
//    Pierre-Olivier Charlebois (SRE/CIM): Former Developer
//
// Funding by / Souventionné par:
//    Natural Sciences and Engineering Research Council of Canada (NSERC)
//    Canada Council for the Arts
//    NSERC/Canada Council for the Arts - New Media Initiative
//
// ===================================================================
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// ===================================================================


#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <regex.h>


#include "vAudioRenderer.h"
#include "vListener.h"
#include "vSoundNode.h"
#include "vSoundConn.h"
#include "vRolloffTable.h"

#include "g_canvas.h"



using namespace std;

// declaration of this Pd class:
t_class *vAudioRenderer_class;

// some helper symbols:
static t_symbol *symbol_SoundNode = gensym("SoundNode");
static t_symbol *symbol_Listener = gensym("Listener");
static t_symbol *symbol_SoundConnection = gensym("SoundConnection");
static t_symbol *symbol_NULL = gensym("NULL");


// globally-accessible pointer (to enforce that only one set of nodes and
// connections exists):
vAudioManager *globalAudioManager;



// *****************************************************************************
void vAudioRenderer_compute(t_vAudioRenderer *x, vSoundConn* conn)
{

	// If this connection is set to "thru", then there is no need to compute any
	// coefficients. The audio signal in Pd will bypass all gains, filters, and
	// delays
	if (conn->thru)
	{
		return;
	}
	
	// If one of the connected nodes has been deactivated, then there is no need
	// to compute anything. Enable the mute (and send the status change if this
	// has just happened)
	if ((!conn->src->active) || (!conn->snk->active))
	{
		if (!conn->mute)
		{
			conn->mute = true;
			SETSYMBOL (x->coeffAtoms+0, gensym("mute"));
			SETFLOAT (x->coeffAtoms+1, 1);
			outlet_anything(x->coeff_outlet, conn->id, 2, x->coeffAtoms);
		}
		return;
	}
	

	SETSYMBOL(x->coeffAtoms+0, gensym("bang"));
	outlet_anything(x->coeff_outlet, conn->id, 1, x->coeffAtoms);
	
	
	
	/*
	Vector3 conn_vector = conn->snk->pos - conn->src->pos;
	
	// DISTANCE:
	
	double distance = (double)conn_vector.Mag();
	double distanceScalar = 1 / (1.0 + pow(distance,(double)conn->distanceEffect*.01));
	
	// SIMPLE MUTE MECHANISM (TODO: also mute any connected regions)
	
	if (distanceScalar < x->gainThreshold)
	{
		if (!conn->mute)
		{
			conn->mute = true;
			SETSYMBOL (x->coeffAtoms+0, gensym("mute"));
			SETFLOAT (x->coeffAtoms+1, 1);
			outlet_anything(x->coeff_outlet, conn->id, 2, x->coeffAtoms);
		}
		return;
	}
	
	
	// ROLLOFF: 

	// incidence (radians) between source and the connection_vector:
	Vector3 srcDir = EulerToQuat(conn->src->rot) * Vector3(0,1,0);
	double srcIncidence = AngleBetweenVectors(srcDir, conn_vector);
	double srcIncidenceGain = conn->src->rolloff->getValue( (srcIncidence * conn->src->spread) / M_PI );
	
	
	// incidence (radians) between sink and the connection_vector:
	Vector3 snkDir = EulerToQuat(conn->snk->rot) * Vector3(0,1,0);
	double snkIncidence = AngleBetweenVectors(Vector3(0,0,0)-snkDir, conn_vector);

	
	// if connected sink is a listener, IGNORE it (i.e. set the sink's IncidenceGain to unity) since the incidence(s) for the sink's vmic(s) is/are calculated in the connection patch

	double snkIncidenceGain = conn->snk->rolloff->getValue( (snkIncidence * conn->snk->spread) / M_PI );

	

	double rolloffScaler = (double) (1.0 - (.01*conn->rolloffEffect  * (1.0 - srcIncidenceGain*snkIncidenceGain)));


	
	
	// combined amplitude scalar:
	double G1 = distanceScalar * rolloffScaler; // * 0.00001;
	
	// incidence filter:
	// - range is 500Hz -> 21750 + 500 Hz)
	// - feeds Pd object [lop~ 500]
	double Lpf1 = 500 + ( 21550 * (0.5 - ( .5*cos(pow(rolloffScaler,4)*M_PI) ) ) );
	
	// absorption filter:
	// - function of distance
	// - range is 50Hz -> 22000Hz
	// - feeds Pd object [lop~ 22000]
	double Lpf2 = 22000 - ( 3500*log(distance+1) );
	if (Lpf2 < 50) Lpf2=50;
	if (Lpf2 > 22000) Lpf2=22000;
	
	// variable delay for Doppler:
	// - feeds Pd object [vd~ $0-dop]
	double vdel1 = distance * 2.89855; // speed of sound
	
	
	// If we've gotten this far, then make sure that the mute is not on:
	if (conn->mute)
	{
		conn->mute = false;
		SETSYMBOL (x->coeffAtoms+0, gensym("mute"));
		SETFLOAT (x->coeffAtoms+1, 0);
		outlet_anything(x->coeff_outlet, conn->id, 2, x->coeffAtoms);
	}
	
	// Now output coeffs:
							   
	SETFLOAT (x->coeffAtoms+0, (t_float) G1);
	SETFLOAT (x->coeffAtoms+1, (t_float) Lpf1);
	SETFLOAT (x->coeffAtoms+2, (t_float) Lpf2);
	SETFLOAT (x->coeffAtoms+3, (t_float) vdel1);		
	outlet_anything(x->coeff_outlet, conn->id, 4, x->coeffAtoms);
	*/
}


// *****************************************************************************
// dynamic patching functions:
// *****************************************************************************

t_symbol *vAudioRenderer_getDynamicPatch(t_vAudioRenderer *x, std::string which)
{
	t_symbol *dollarZero = canvas_realizedollar(x->x_canvas, gensym("$0"));
	t_symbol *dynamicPatch = gensym((char*)("pd-" + string(dollarZero->s_name) + "-" + which).c_str());
	

	// Check to see if the subpatcher exists:
	if (dynamicPatch->s_thing)
	{
		// Found the patch! just return the symbol
		
	} else {
		// If not, then we try to create it directly underneath the current
		// object by reading the x,y position from the t_object...

		t_object *thisObj = &(x->x_obj);
		
		int offset = 20;
		if (which=="SoundConnections") offset += 20;
		
		string creationString = stringify((int)thisObj->te_xpix) + " " + stringify((int)thisObj->te_ypix+offset) + " DynamicPatchHolder \\$0-" + which;
		
		int atomCount;
		t_atom *atomList = atomsFromString(creationString, atomCount);

		// send message to "host" canvas to instantiate subpatch:
		typedmess((_class **) x->x_canvas, gensym("obj"), atomCount, atomList); 

		pdMesg(dynamicPatch, gensym("vis"), "0");
		
		vAudioRenderer_clearDynamicPatch(x, which);
	}

	return dynamicPatch;
}

t_symbol *vAudioRenderer_getNodeSubpatch(t_vAudioRenderer *x)
{
	return vAudioRenderer_getDynamicPatch(x, "SoundNodes");
}

t_symbol *vAudioRenderer_getConnSubpatch(t_vAudioRenderer *x)
{
	return vAudioRenderer_getDynamicPatch(x, "SoundConnections");
}

void vAudioRenderer_clearDynamicPatch(t_vAudioRenderer *x, std::string which)
{
	t_symbol *subpatch = vAudioRenderer_getDynamicPatch(x, which);
	
	pdMesg(subpatch, gensym("clear"), "0");
	
	// create a [cnv], warning users to not edit the patch:
	pdMesg(subpatch, gensym("obj"), string("0 0 cnv 10 450 40 empty empty WARNING:__THIS__IS__A__DYNAMIC__PATCH!___ 10 10 1 16 -261234 -66577 0"));
	pdMesg(subpatch, gensym("obj"), string("10 23 cnv 10 1 1 empty empty ...DO-NOT.directly.edit.this_and_DO-NOT.move.objects! 0 5 0 14 -233017 -66577 0"));

	if (which=="SoundNodes") x->dynamicNodeCount = 0;
	if (which=="SoundConnections") x->dynamicConnCount = 0;
	
	pdMesg(subpatch, gensym("vis"), "0");
}



// *****************************************************************************
// *** PD OBJECT METHODS:
// *****************************************************************************


// *****************************************************************************
static void vAudioRenderer_outputNodeList (t_vAudioRenderer *x)
{
	t_atom *atomList;

	vector<t_symbol*> validNodes;

	for (nodeIterator n = globalAudioManager->vSoundNodeList.begin(); n != globalAudioManager->vSoundNodeList.end(); n++)
	{
		if (!(*n)->isListener) validNodes.push_back((*n)->id);
	}

	if (validNodes.size())
	{
		atomList = (t_atom*) getbytes((validNodes.size()+1) * sizeof(t_atom));
		SETSYMBOL (atomList, symbol_SoundNode);
		for (int i=0; i<validNodes.size(); i++)
		{
			SETSYMBOL (atomList+i+1, validNodes[i]);
		}
		
		outlet_anything(x->event_outlet, gensym("nodeList"), validNodes.size()+1, atomList);
	}
	
	else 
	{
		atomList = (t_atom*) getbytes(2*sizeof(t_atom));
		SETSYMBOL (atomList+0, symbol_SoundNode);
		SETSYMBOL (atomList+1, gensym("NULL"));
		outlet_anything(x->event_outlet, gensym("nodeList"), 2, atomList);
	}
}

static void vAudioRenderer_outputListenerList (t_vAudioRenderer *x)
{
	t_atom *atomList;

	if (globalAudioManager->vListenerList.size())
	{
		atomList = (t_atom*) getbytes((globalAudioManager->vListenerList.size()+1) * sizeof(t_atom));

		SETSYMBOL (atomList, symbol_Listener);
		for (int i=0; i<globalAudioManager->vListenerList.size(); i++)
		{
			SETSYMBOL (atomList+i+1, globalAudioManager->vListenerList[i]->node->id);
		}
		outlet_anything(x->event_outlet, gensym("nodeList"), globalAudioManager->vListenerList.size()+1, atomList);
	}

	else
	{
		atomList = (t_atom*) getbytes(2*sizeof(t_atom));
		SETSYMBOL (atomList+0, symbol_Listener);
		SETSYMBOL (atomList+1, gensym("NULL"));
		outlet_anything(x->event_outlet, gensym("nodeList"), 2, atomList);
	}
}

static void vAudioRenderer_outputConnList (t_vAudioRenderer *x)
{
	t_atom *atomList;

	if (globalAudioManager->vSoundConnList.size())
	{
		atomList = (t_atom*) getbytes((globalAudioManager->vSoundConnList.size()+1) * sizeof(t_atom));
		
		SETSYMBOL (atomList, symbol_SoundConnection);
		for (int i=0; i<globalAudioManager->vSoundConnList.size(); i++)
		{
			SETSYMBOL (atomList+i+1, globalAudioManager->vSoundConnList[i]->id);
		}
		outlet_anything(x->event_outlet, gensym("nodeList"), globalAudioManager->vSoundConnList.size()+1, atomList);
	}
	
	else 
	{
		atomList = (t_atom*) getbytes(2*sizeof(t_atom));
		SETSYMBOL (atomList+0, symbol_SoundConnection);
		SETSYMBOL (atomList+1, gensym("NULL"));
		outlet_anything(x->event_outlet, gensym("nodeList"), 2, atomList);
	}
}


static void vAudioRenderer_bang (t_vAudioRenderer *x)
{
	
	// compute a connection if updateFlag is set on any node:
	for (connIterator c = globalAudioManager->vSoundConnList.begin(); c != globalAudioManager->vSoundConnList.end(); c++)
	{
		if (((*c)->src->updateFlag) || ((*c)->snk->updateFlag))
		{
			vAudioRenderer_compute(x,(*c));
		}
	}
	
	// reset all update flags:
	for (nodeIterator n = globalAudioManager->vSoundNodeList.begin(); n != globalAudioManager->vSoundNodeList.end(); n++)
	{
		(*n)->updateFlag = false;
	}

}

static void vAudioRenderer_connect (t_vAudioRenderer *x, t_symbol *src, t_symbol *snk)
{

	//post("got connect message: %s -> %s", src->s_name, snk->s_name);

	// check if exists first:
	vSoundConn* conn = globalAudioManager->getConnection(src,snk);
	if (!conn) {
		
		// Check src and snk against the connectFilter. If either match, then 
		// proceed with the connection:
		int srcRegexStatus = regexec(&x->connectRegex, src->s_name, (size_t)0, NULL, 0);
		int snkRegexStatus = regexec(&x->connectRegex, snk->s_name, (size_t)0, NULL, 0);

		// check if both nodes exist
		vSoundNode *srcNode = globalAudioManager->getNode(src);
		vListener *listener = globalAudioManager->getListener(snk);
		vSoundNode *snkNode;
		
		bool isNormalConnection = true;
		if (listener)
		{
			snkNode = listener->node;
			isNormalConnection = false;
		}
		else snkNode = globalAudioManager->getNode(snk);
		

		if (isNormalConnection || ((srcRegexStatus==0) || (snkRegexStatus==0)))
		{
			if (srcNode && snkNode)
			{
				// create connection:
				conn = new vSoundConn(srcNode, snkNode);

				// register the connection with both the AudioRenderer and the 
				// sink node (for backwards connectivity computation):
				globalAudioManager->vSoundConnList.push_back(conn);
				snkNode->connectFROM.push_back(conn);

				// if this is a listener connection, set the proper type:
				if (listener)
				{
					conn->type = gensym("spin.ListenerConnection");
				}

				// dynamically create the Pd object:
				conn->createPdObj(x);
				
				t_atom *atomList = (t_atom*) getbytes(2*sizeof(t_atom));
				
				// send the listenerType if needed:
				if (listener)
				{
					SETSYMBOL (atomList+0, gensym("setType"));
					SETSYMBOL (atomList+1, listener->type);
					outlet_anything(x->coeff_outlet, conn->id, 2, atomList);
				}
				
				// send an event from the event_outlet:
				/*
				SETSYMBOL (atomList+0, src);
				SETSYMBOL (atomList+1, snk);
				outlet_anything(x->event_outlet, gensym("connect"), 2, atomList);
				*/
				
				vAudioRenderer_outputConnList(x);
			}
		}
	}
}

static void vAudioRenderer_disconnect (t_vAudioRenderer *x, t_symbol *src, t_symbol *snk)
{
	connIterator c;
	for (c = globalAudioManager->vSoundConnList.begin(); c != globalAudioManager->vSoundConnList.end(); c++)
	{
		if ( ((*c)->src->id==src) && ((*c)->snk->id==snk) )
		{
			// grab a pointer to the actual connection:
			vSoundConn *conn = (*c);
			
			// remove it from the list:
			globalAudioManager->vSoundConnList.erase(c);	
			
			// delete the Pd object:
			conn->deletePdObj(x);
			
			// send an event from the event_outlet:
			/*
			t_atom *atomList = (t_atom*) getbytes(2*sizeof(t_atom));
			SETSYMBOL (atomList+0, conn->src->id);
			SETSYMBOL (atomList+1, conn->snk->id);
			outlet_anything(x->event_outlet, gensym("disconnect"), 2, atomList);
			*/
			
			// delete the vSoundConn instance:
			delete conn;	
			
			vAudioRenderer_outputConnList(x);
			
			break;
		}
	}
	
}

vListener* vAudioRenderer_createListener (t_vAudioRenderer *x, t_symbol *id)
{
	// ignore "NULL"
	if (id==gensym("NULL")) return NULL;
	
	// check if it already exists:
	vListener *L = globalAudioManager->getListener(id);

	if (!L)
	{
		// if not, create a new vListener:
		L = new vListener(x, id);
		
		// add it to the globalAudioManager->vListenerList:
		globalAudioManager->vListenerList.push_back(L);

	}
	
	return L;
}

vSoundNode* vAudioRenderer_createSoundNode (t_vAudioRenderer *x, t_symbol *id)
{
	// ignore "NULL"
	if (id==gensym("NULL")) return NULL;
	
	// check if it already exists:
	vSoundNode *n = globalAudioManager->getNode(id);

	if (!n) {
	
		// if not, create a new vSoundNode:
		n = new vSoundNode(id);
		
		// give it one of the rolloffs living in the shared list:
		n->setRolloff(x, gensym("default"));
		
		// add it to the globalAudioManager->vSoundNodeList:
		globalAudioManager->vSoundNodeList.push_back(n);
		
		// create the Pd patch:
		n->createPdObj(x);

	}

	return n;
}

static void vAudioRenderer_createNode (t_vAudioRenderer *x, t_symbol *id, t_symbol *type)
{
	if (type==gensym("SoundNode"))
	{
		vAudioRenderer_createSoundNode(x,id);
		
		// send a createNode event from the event_outlet:
		/*
		t_atom *atomList = (t_atom*) getbytes(3*sizeof(t_atom));
		SETSYMBOL (atomList+0, id);
		SETSYMBOL (atomList+1, gensym("SoundNode"));
		outlet_anything(x->event_outlet, gensym("createNode"), 2, atomList);
		*/

		// send a soundnode list:
		vAudioRenderer_outputNodeList(x);

	}
	if (type==gensym("Listener"))
	{
		vAudioRenderer_createListener(x,id);

		// send a createNode event from the event_outlet:
		/*
		t_atom *atomList = (t_atom*) getbytes(3*sizeof(t_atom));
		SETSYMBOL (atomList+0, id);
		SETSYMBOL (atomList+1, gensym("Listener"));
		outlet_anything(x->event_outlet, gensym("createNode"), 2, atomList);
		*/

		// send a listener list:
		vAudioRenderer_outputListenerList(x);
	}
}


static void vAudioRenderer_deleteNode (t_vAudioRenderer *x, t_symbol *id)
{
	// first check if the node is a listener and delete that first:
	vListener *listener = globalAudioManager->getListener(id);
	vSoundNode *node;
	
	if (listener)
	{
		node = listener->node;
		for (listenerIterator L = globalAudioManager->vListenerList.begin(); L != globalAudioManager->vListenerList.end(); L++)
		{
			if ((*L)==listener)
			{
				globalAudioManager->vListenerList.erase(L);
				break;
			}
		}
		delete listener;

		vAudioRenderer_outputListenerList(x);
	}
	
	else
	{
		node = globalAudioManager->getNode(id);
	}
	
	
	if (node)
	{

		// remove all connections that this node is part of:
		std::vector<vSoundConn*> connections = globalAudioManager->getConnectionsForNode(id);
		for (connIterator c = connections.begin(); c != connections.end(); c++)
		{
			vAudioRenderer_disconnect(x,(*c)->src->id, (*c)->snk->id);
		}
		
		//  delete the actual pd object:
		node->deletePdObj(x);
		
		// send an event (before it's actually deleted):
		/*
		t_atom a;
		SETSYMBOL (&a, node->id);
		outlet_anything(x->event_outlet, gensym("deleteNode"), 1, &a);
		*/
		
		// remove node from the globalAudioManager->vSoundNodeList:
		nodeIterator n;
		for (n = globalAudioManager->vSoundNodeList.begin(); n != globalAudioManager->vSoundNodeList.end(); n++)
		{
			if ((*n) == node)
			{
				globalAudioManager->vSoundNodeList.erase(n);
				delete node;
				break;
			}
		}
		
		vAudioRenderer_outputNodeList(x);

	}
}
	
static void vAudioRenderer_clear(t_vAudioRenderer *x)
{
	// clear connections before nodes:
	while (globalAudioManager->vSoundConnList.size())
	{
		vAudioRenderer_disconnect(x, globalAudioManager->vSoundConnList[0]->src->id, globalAudioManager->vSoundConnList[0]->snk->id);
	}
	
	// delete nodes (NOTE: this includes listeners) 
	while (globalAudioManager->vListenerList.size())
	{
		vAudioRenderer_deleteNode(x, globalAudioManager->vListenerList[0]->node->id);
	}
	
	vAudioRenderer_clearDynamicPatch(x, "SoundNodes");
	vAudioRenderer_clearDynamicPatch(x, "SoundConnections");
	
	vAudioRenderer_outputNodeList(x);
	vAudioRenderer_outputListenerList(x);
	vAudioRenderer_outputConnList(x);
}


// *****************************************************************************
static void vAudioRenderer_debug (t_vAudioRenderer *x)
{
	listenerIterator L;
	nodeIterator n;
	connIterator c;
	
	vector<vSoundNode*> listenerNodes;

	post("\n=====================================================");
	
	post("[vAudioRenderer]:: connectFilter = %s", x->connectFilter->s_name);
	post("[vAudioRenderer]:: gainThreshold = %.2f", x->gainThreshold);
	post("[vAudioRenderer]:: %d listeners:", globalAudioManager->vListenerList.size());
	for (L = globalAudioManager->vListenerList.begin(); L != globalAudioManager->vListenerList.end(); L++)
	{
		post("  %s:", (*L)->node->id->s_name);
		post("    pos: (%.3f,%.3f,%.3f)",(*L)->node->pos.x,(*L)->node->pos.y,(*L)->node->pos.z);
		post("    rot: (%.3f,%.3f,%.3f)",(*L)->node->rot.x,(*L)->node->rot.y,(*L)->node->rot.z);
		post("    active:\t%d", (*L)->node->active);
		post("    plugin:\t%s", (*L)->node->plugin->s_name);
		startpost("    listening to:");
		for (c = (*L)->node->connectFROM.begin(); c != (*L)->node->connectFROM.end(); c++)
		{
			poststring((*c)->src->id->s_name);
		}
		endpost();
		
		listenerNodes.push_back((*L)->node);
	}
	
	post("[vAudioRenderer]:: %d nodes:", globalAudioManager->vSoundNodeList.size() - globalAudioManager->vListenerList.size());
	for (n = globalAudioManager->vSoundNodeList.begin(); n != globalAudioManager->vSoundNodeList.end() ; n++)
	{
		// only nodes that are not listenerNodes (above):
		if (find(listenerNodes.begin(),listenerNodes.end(),*n) == listenerNodes.end())
		{
			post("  %s:", (*n)->id->s_name);
			post("    pos: (%.3f,%.3f,%.3f)",(*n)->pos.x,(*n)->pos.y,(*n)->pos.z);
			post("    rot: (%.3f,%.3f,%.3f)",(*n)->rot.x,(*n)->rot.y,(*n)->rot.z);
			post("    active:\t%d", (*n)->active);
			post("    plugin:\t%s", (*n)->plugin->s_name);
			post("    rolloff:\t%s", (*n)->rolloff->tableName->s_name);
			post("    spread:\t%.3f", (*n)->spread);
			post("    length:\t%.3f", (*n)->length);
		}
	}

	post("[vAudioRenderer]:: %d connections:", globalAudioManager->vSoundConnList.size());
	for (c = globalAudioManager->vSoundConnList.begin(); c != globalAudioManager->vSoundConnList.end(); c++)
	{
		post("  %s:", (*c)->id->s_name);
		post("    type:\t%s", (*c)->type->s_name);
		post("    thru:\t%d", (*c)->thru);
		post("    mute:\t%d", (int)(*c)->mute);
		post("    distanceEffect:\t%.1f", (*c)->distanceEffect);
		post("    rolloffEffect:\t%.1f", (*c)->rolloffEffect);
		post("    dopplerEffect:\t%.1f", (*c)->dopplerEffect);
		post("    diffractionEffect:\t%.1f", (*c)->diffractionEffect);
	}
	
}

static void vAudioRenderer_setGainThreshold (t_vAudioRenderer *x, t_floatarg f)
{
	x->gainThreshold = f;
	
	// Now set update flags for every connected source node (which will result
	// in an update for every connection):
	connIterator c;
	for (c = globalAudioManager->vSoundConnList.begin(); c != globalAudioManager->vSoundConnList.end(); c++)
	{
		(*c)->src->updateFlag = true;
	}
}

static void vAudioRenderer_setConnectFilter (t_vAudioRenderer *x, t_symbol *s)
{
	// we like specifying just one asterisk ( * ), so we need to convert to a
	// regular expression:
	if (s==gensym("*")) s = gensym(".*");

	if (regcomp(&x->connectRegex, s->s_name, REG_EXTENDED|REG_NOSUB) != 0)
	{
		post("vAudioRenderer error: bad regex pattern passed to setConnectFilter(): %s ", s->s_name);
	    return;
	}
	
	x->connectFilter = s;
}


// *****************************************************************************
static void vAudioRenderer_nodeList (t_vAudioRenderer *x, t_symbol *s, int argc, t_atom *argv)
{
	// there need to be at least 2 args in order to be a legal message:
	// argv[0] is the nodeType
	// argv[1+] is the list of nodes or argv[1] will be "NULL" if list is empty
	
	if (argc<2) return;
	
	std::vector<t_symbol*> targetList;

	// we build a list of nodes to delete, so that we don't invalidate iterators
	std::vector<t_symbol*> nodesToDelete;



	// we need to create a soundNode if it doesn't exist:
	if ((argv[0].a_type == A_SYMBOL) && (argv[0].a_w.w_symbol==symbol_SoundNode))
	{
		/*
		startpost("got nodelist:");
		postatom(argc, argv);
		endpost();
		*/

		for (int i=1; i<argc; i++)
		{
			if (argv[i].a_w.w_symbol != symbol_NULL)
			{
				//vAudioRenderer_createSoundNode(x, argv[i].a_w.w_symbol);
				vAudioRenderer_createNode(x, argv[i].a_w.w_symbol, symbol_SoundNode);
				targetList.push_back(argv[i].a_w.w_symbol);
			}
		}

		nodeIterator n;
		for (n=globalAudioManager->vSoundNodeList.begin(); n!=globalAudioManager->vSoundNodeList.end(); n++)
		{
			if (std::find(targetList.begin(), targetList.end(), (*n)->id) == targetList.end())
			{
				if (!(*n)->isListener)
				{
					nodesToDelete.push_back((*n)->id);
				}

			}
		}
	}

	// do the same for listeners
	else if ((argv[0].a_type == A_SYMBOL) && (argv[0].a_w.w_symbol==symbol_Listener))
	{
		/*
		startpost("got nodelist:");
		postatom(argc, argv);
		endpost();
		*/

		for (int i=1; i<argc; i++)
		{
			if (argv[i].a_w.w_symbol != symbol_NULL)
			{
				//vAudioRenderer_createListener(x, argv[i].a_w.w_symbol);
				vAudioRenderer_createNode(x, argv[i].a_w.w_symbol, symbol_Listener);
				targetList.push_back(argv[i].a_w.w_symbol);
			}
		}

		listenerIterator L;
		for (L = globalAudioManager->vListenerList.begin(); L != globalAudioManager->vListenerList.end(); L++)
		{
			if (std::find(targetList.begin(), targetList.end(), (*L)->node->id) == targetList.end())
			{
				nodesToDelete.push_back((*L)->node->id);
			}
		}
	}


	// We also need to remove delete any nodes that are in the renderer but are
	// not in the nodeList (ie, have been deleted from the server)
	// ... Must be careful though, since each listener has a corresponding
	// soundNode, which shouldn't be deleted (unless the listener is deleted)



	while (nodesToDelete.size())
	{
		post("deleting %s", nodesToDelete.back()->s_name);
		vAudioRenderer_deleteNode(x, nodesToDelete.back());
		nodesToDelete.pop_back();
	}

}


// *****************************************************************************
static void vAudioRenderer_call (t_vAudioRenderer *x, t_symbol *s, int argc, t_atom *argv)
{
	/*
	startpost("Got call message:");
	postatom(argc, argv);
	endpost();
	*/

	// argv[0]: node id
	// argv[1]: method
	// argv[2+]: method args
	
	// there need to be at least 2 args in order to be a legal message:
	if (argc<2) return;
	
	// the first 2 args also need to be symbolic:
	if ((argv[0].a_type != A_SYMBOL) || (argv[1].a_type != A_SYMBOL)) return;
	
	string method = argv[1].a_w.w_symbol->s_name;
	
	// see if we are interested in this node or listener:
	
	vListener *listener = globalAudioManager->getListener(argv[0].a_w.w_symbol);
	vSoundNode *node;
	
	if (listener)
	{
		if (method=="setType")
		{
			listener->type = atom_getsymbol(argv+2);
			listener->refreshConnections(x);
		}
		
		node = listener->node;
	}
	else
	{
		node = globalAudioManager->getNode(argv[0].a_w.w_symbol);
	}
	
	
	if (node)
	{
		// now apply the method:
		
		if (method=="global6DOF")
		{
			node->pos = Vector3(atom_getfloat(argv+2),atom_getfloat(argv+3),atom_getfloat(argv+4));
			node->rot = Vector3(atom_getfloat(argv+5),atom_getfloat(argv+6),atom_getfloat(argv+7));
			node->updateFlag=true;
		}
		else if (method=="setActive")
		{
			node->active = atom_getfloat(argv+2);
			node->updateFlag=true;
		}
		else if (method=="setPlugin") node->plugin = atom_getsymbol(argv+2);
		else if (method=="setRolloff")
		{
			node->setRolloff(x, atom_getsymbol(argv+2));
			node->updateFlag=true;
		}
		else if (method=="setSpread")
		{
			node->spread = atom_getfloat(argv+2);
			node->updateFlag=true;
		}
		else if (method=="setLength")
		{
			node->length = atom_getfloat(argv+2);
			node->updateFlag=true;
		}
		else if (method=="connect") vAudioRenderer_connect(x,node->id,atom_getsymbol(argv+2));
		else if (method=="disconnect") vAudioRenderer_disconnect(x,node->id,atom_getsymbol(argv+2));
		
	} else {
		
		vSoundConn *conn = globalAudioManager->getConnection(argv[0].a_w.w_symbol);
		if (conn)
		{
			if (method=="setThru")
			{
				conn->thru = (int) atom_getfloat(argv+2);
				SETSYMBOL(x->coeffAtoms+0, gensym("thru"));
				SETFLOAT(x->coeffAtoms+1, conn->thru);
				outlet_anything(x->coeff_outlet, conn->id, 2, x->coeffAtoms);
			}
			else if (method=="setDistanceEffect") conn->distanceEffect = atom_getfloat(argv+2);
			else if (method=="setRolloffEffect") conn->rolloffEffect = atom_getfloat(argv+2);
			else if (method=="setDopplerEffect") conn->dopplerEffect = atom_getfloat(argv+2);
			else if (method=="setDiffractionEffect") conn->diffractionEffect = atom_getfloat(argv+2);

			// force update
			conn->snk->updateFlag = true;
		}
	}

}


static void vAudioRenderer_anything (t_vAudioRenderer *x, t_symbol *s, int argc, t_atom *argv)
{
	// This method is here to prevent Pd from printing messages from SPIN that
	// do not have corresponding AudioRenderer methods. ie:
	//
	// error: vAudioRenderer: no method for 'methodName'
}


// *****************************************************************************
// *** CONSTRUCTOR:
// *****************************************************************************

static void *vAudioRenderer_new (t_symbol *idString)
{
	t_vAudioRenderer *x = (t_vAudioRenderer *) pd_new (vAudioRenderer_class);

	// Get the canvas on which this Pd object resides. NOTE: this must be done
	// in the constructor, otherwise canvas_getcurrent() returns garbage.
	x->x_canvas = canvas_getcurrent();
	
	x->dynamicNodeCount = 0;
	x->dynamicConnCount = 0;
	
	vAudioRenderer_setConnectFilter(x, gensym(".*")); // match everything
	
	x->gainThreshold = 0.004;
	
	x->coeff_outlet = outlet_new(&x->x_obj, &s_list);
	x->event_outlet = outlet_new(&x->x_obj, &s_list);

	return (void *) x;
}


// ***********************************************************
// ********************** DESTRUCTOR: ************************
// ***********************************************************
static void vAudioRenderer_free (t_vAudioRenderer * x)
{
	//vAudioRenderer_clear(x);
	regfree(&(x->connectRegex));
}

// ***********************************************************
// ******************* PD SETUP FUNCTION: ********************
// ***********************************************************
extern "C" void computeConnection_setup(void);
extern "C" void vAudioRenderer_setup (void)
{
	
	// Now we create a new Pd class:
	vAudioRenderer_class = class_new(gensym("vAudioRenderer"), (t_newmethod)vAudioRenderer_new, (t_method)vAudioRenderer_free, sizeof(t_vAudioRenderer), CLASS_DEFAULT, A_DEFSYMBOL, 0);
	// Note: for some reason, setting up new Pd methods and classes with zero
	// arguments won't work. For example, class_new() and class_addmethod()
	// needs to have at least one A_DEFLOAT or something in order to instantiate
	// properly. why?? because of c++? ????

	// We create ONE instance of the vAudioManager class, and share it between
	// all renderers:
	globalAudioManager = new vAudioManager();
	
	
	computeConnection_setup();
	
	
	// Declare all Pd method handlers:
	class_addbang(vAudioRenderer_class, (t_method)vAudioRenderer_bang);

	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_createSoundNode, gensym("createSoundNode"), A_DEFSYMBOL, 0);
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_createListener, gensym("createListener"), A_DEFSYMBOL, 0);
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_createNode, gensym("createNode"), A_DEFSYMBOL, A_DEFSYMBOL, 0);
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_deleteNode, gensym("deleteNode"), A_DEFSYMBOL, 0);
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_clear, gensym("clear"), A_DEFFLOAT, 0);
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_debug, gensym("debug"), A_DEFFLOAT, 0);

	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_setGainThreshold, gensym("setGainThreshold"), A_DEFFLOAT, 0);

	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_setConnectFilter, gensym("setConnectFilter"), A_DEFSYMBOL, 0);

	
	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_nodeList, gensym("nodeList"), A_GIMME, 0);

	class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_call, gensym("call"), A_GIMME, 0);
	//class_addmethod(vAudioRenderer_class, (t_method)vAudioRenderer_connMsg, gensym("connMsg"), A_GIMME, 0);

	class_addanything(vAudioRenderer_class, (t_method)vAudioRenderer_anything);

}
