#include "m_pd.h"
//#include <stdio.h>
//#include <stdlib.h>
#include <string.h>

#define MAXSIZE 100

static t_class *replacestring_class;

typedef struct _replacestring
{
	t_object x_obj;
	t_symbol *x_s;
	t_symbol *x_char;
	t_symbol *x_replacement;
} t_replacestring;


int isSpace(char ch)
{
	char space[]={9,13,10,32,0xFF,0};
	return strchr(space, ch) ? 1 : 0;
}

static void replacestring_symbol(t_replacestring *x, t_symbol *s)
{
	unsigned int i;
	char outString[MAXPDSTRING];
	char *find;
	char searchChar;

	strcpy(outString, s->s_name);

	
	if (x->x_char==gensym("space"))
	{
		for (i=0; i<strlen(outString); i++)
		{
			if (isSpace(outString[i])) strncpy(outString+i, x->x_replacement->s_name, 1);
		}
	}
	
	else {
		while (find = strchr(outString, x->x_char->s_name[0]))
		{
			strncpy(outString, x->x_replacement->s_name, 1);
		}
	}

	
	outlet_symbol(x->x_obj.ob_outlet, gensym(outString));

}


static void replacestring_bang(t_replacestring *x)
{
	if (x->x_s) replacestring_symbol (x, x->x_s);
}

static void *replacestring_new(t_symbol *s, int argc, t_atom *argv)
{
	t_replacestring *x = (t_replacestring *)pd_new(replacestring_class);
	x->x_s = gensym("NULL");
	x->x_char=gensym("space");
	x->x_replacement=gensym("_");

	if (argc > 0)
		if (argv[0].a_type == A_SYMBOL) x->x_char = argv[0].a_w.w_symbol;
	if (argc > 1)
		if (argv[1].a_type == A_SYMBOL) x->x_replacement = argv[1].a_w.w_symbol;
	
	symbolinlet_new(&x->x_obj, &x->x_char);
	symbolinlet_new(&x->x_obj, &x->x_replacement);
	outlet_new(&x->x_obj, &s_symbol);
	return (x);
}


void replacestring_setup(void)
{
	replacestring_class = class_new(gensym("replacestring"), (t_newmethod)replacestring_new, 0, sizeof(t_replacestring), 0, A_GIMME, 0);
	class_addbang(replacestring_class, replacestring_bang);
	class_addsymbol(replacestring_class, replacestring_symbol);
}
