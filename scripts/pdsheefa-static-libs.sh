#!/bin/bash    

# to prepare a pdsheefa bundle, a static version of liblo needs to be compiled, and OSCrxtx needs to be linked against the static lib.

# do the following

echo "can't automate this yet"

exit



The current way to make a fat liblo.a is to compile on i386 and x86_64 machines


i386 machine:

./configure --enable-static
make

the cp /usr/local/lib/liblo.a ~/svn/pdsheefa/admin/lib/liblo.i386.a
svn commit ~/svn/pdsheefa/admin/lib/liblo.i386.a


and then the same on an x86_64 machine:


using ./configure --enable-static
make

the cp /usr/local/lib/liblo.a ~/svn/pdsheefa/admin/lib/liblo.x86_64.a
svn commit ~/svn/pdsheefa/admin/lib/liblo.x86_64.a


cd ~/svn/pdsheefa/admin/lib

then use 

lipo -create liblo.i386.a liblo.x86_64.a -output liblo.a

svn commit liblo.a




#cd ~/svn 
#svn co https://liblo.svn.sourceforge.net/svnroot/liblo/trunk liblo
cd ~/svn/liblo
sh autogen.sh
./configure --enable-static build=i386 CFLAGS="-m32"
make
sudo make install

done


