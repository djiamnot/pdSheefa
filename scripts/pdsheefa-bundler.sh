         #!/bin/bash          
		# arg1: version number 
		# writes .tar.gz to ../tarballs
		
		
		if [ -z $1 ] ; then
			echo missing version number arg., exiting
			exit
		fi
		
		
#		DIR_PATH=${0%pdsheefa-bundler.sh}

		reldir=`dirname $0`
		cd $reldir
		DIR_PATH=`pwd`/..

		CALLING_DIR=`pwd`


		#echo DIRPATH=  $DIR_PATH
		



		OS="$(uname)"
		ARCH="$(uname -m)"
		
		PDSHEEFA_DIR=$DIR_PATH
		XJ_DIR=$PDSHEEFA_DIR/xjimmies
#		SPINW_DIR=~/svn/spinwidgets
#		AS_DIR=~/svn/audioscape

		PDOBJECTS_DIR=pdsheefa



		if [ $OS = Darwin ] ; then
			PD_EXTERNS_DIR=~/Library/Pd
#			XJIMMIES_EXTERNS_DIR=$PD_EXTERNS_DIR/xjimmies
			MAC_OS_VER=`sw_vers -productVersion`
			MAC_OS_VER=${MAC_OS_VER:0:4}
			MAC_OS_VER=.macOS_${MAC_OS_VER}
			ARCHIVENAME=$PDOBJECTS_DIR-$OS-universal-$1.tar.gz 
		fi

		if [ $OS = Linux ] ; then
			PD_EXTERNS_DIR=/usr/local/lib/pd-externals			
#			XJIMMIES_EXTERNS_DIR=~/pd-externals/xjimmies
			ARCHIVENAME=$PDOBJECTS_DIR-$OS-$ARCH-$1.tar.gz 
		fi

		XJIMMIES_EXTERNS_DIR=$PD_EXTERNS_DIR/xjimmies

		PDSHEEFA_EXTERNS_DIR=$PD_EXTERNS_DIR/$PDOBJECTS_DIR
		
		if [ -d $PDSHEEFA_EXTERNS_DIR ] ; then
			echo $PDSHEEFA_EXTERNS_DIR found	> /dev/null		
		else
			echo  $PDSHEEFA_EXTERNS_DIR not found, exiting
			exit
		fi
		

		if [ -d $XJIMMIES_EXTERNS_DIR ] ; then
			echo $XJIMMIES_EXTERNS_DIR found > /dev/null			
		else
			echo $XJIMMIES_EXTERNS_DIR not found, exiting
			exit
		fi

		#echo XJ_DIR:  $XJ_DIR
		#echo PDSDIR: $PDSHEEFA_DIR

 

		DATE="$(date)"  
 		info1=" xjimmies: $(svn info $XJ_DIR  | grep "Last Changed Rev:" ) \n"
		info2=" pdsheefa: $(svn info $PDSHEEFA_DIR  | grep "Last Changed Rev:" ) \n" 
		
		
		echo 
		echo filename: $ARCHIVENAME "\n"created:   $DATE "\n"compiled on: OS:$OS-$ARCH$MAC_OS_VER "\n"contains: "\n"$info1 $info2 > $PD_EXTERNS_DIR/archive-info 
		cd $PD_EXTERNS_DIR
		mkdir pdsheefa-$1
		cp -rf $XJIMMIES_EXTERNS_DIR/*  pdsheefa-$1/.
		cp -rf pdsheefa/* pdsheefa-$1
		cp archive-info pdsheefa-$1
		#overwrite README in repository with the bundle version
		cp $PDSHEEFA_DIR/scripts/README-BUNDLE pdsheefa-$1/README.txt
#		tar -cvzf $PDSHEEFA_DIR/$ARCHIVENAME $PDOBJECTS_DIR-$1 > /dev/null
		echo "\n" writing $PDSHEEFA_DIR/$ARCHIVENAME
		tar -cvzf $PDSHEEFA_DIR/$ARCHIVENAME $PDOBJECTS_DIR-$1 > /dev/null
#		echo "\n"tarball info: $(du -sh  $PDSHEEFA_DIR/$ARCHIVENAME) 
		echo "\n"tarball info: $(du -sh  $ARCHIVENAME) 
		cat $PD_EXTERNS_DIR/archive-info
		rm $PD_EXTERNS_DIR/archive-info
		rm -rf $PD_EXTERNS_DIR/pdsheefa-$1
		exit


		
		

