#!/bin/sh

FilesToInstall="./LICENSE.txt ./README.txt ./patches/pdsheefa-meta.pd"

echo "\nInstalling"
for f in $FilesToInstall ; do
  echo "  ${f}"
done
echo "into the following directories:"

# find all directory names in ./src, ignore those that start with a dot
# (eg, .svn)
ExternalsList=$(find ./src -maxdepth 1 -mindepth 1 -type d \( -iname "*" ! -iname ".*" \) )
for dir in $ExternalsList ; do
  echo "  ${dir}"
  cp ${FilesToInstall} ${dir}
done

# repeat for ./extra folder:
ExternalsList=$(find ./extra -maxdepth 1 -mindepth 1 -type d \( -iname "*" ! -iname ".*" \) )
for dir in $ExternalsList ; do
  echo "  ${dir}"
  cp ${FilesToInstall} ${dir}
done
