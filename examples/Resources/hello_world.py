import spin
import libSPINPyWrap

__spin_behavior_class__ = "Script"

class Script( spin.ScriptBase ):

    scale = 0

    def __init__(self, id):
        self._nodeID = id
        print "Loaded hello_world script for node '", self._nodeID, "'."
        print "... this will scale an object between 0 and 1 by increments of 0.1"

    def run(self): 

        libSPINPyWrap.callback(self._nodeID, "setScale", [self.scale, self.scale, self.scale], 0)
        self.scale = (self.scale+0.1) % 1 # scale plus 0.1 mod 1

        ret = libSPINPyWrap.callback(self._nodeID, "getScale", [], 0)
        print "scaled ", self._nodeID, " to: ", ret.getVector()

