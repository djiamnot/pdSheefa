# NOTE: This Makefile is the master makefile for all resources
#       needed by pdworks
# march 2011

# Choose your Pd externals to build:


#defined only when making bundles for OSX
#export PDSHEEFA_STATIC_LIBS=1



VERSION-NUMBER=0.4.2


MY_OS=($(shell uname -s))
MY_ARCH=($(shell uname -m))


ifeq ($(shell uname -s),Linux)
	INSTALL_DIR=/usr/local/lib/pd-externals/pdsheefa
	DIST_COMMAND=echo $(shell sh ./scripts/pdsheefa-bundler.sh $(VERSION-NUMBER))
endif

ifeq ($(shell uname -s),Darwin)
	INSTALL_DIR=~/Library/Pd/pdsheefa
	DIST_COMMAND=echo $(shell sudo sh ./scripts/pdsheefa-bundler.sh $(VERSION-NUMBER))
endif

EXTERNS=$(shell find ./src -maxdepth 1 -mindepth 1 -type d \( -iname "*" ! -iname ".*" \) )
EXTRAS=$(shell find ./extra -maxdepth 1 -mindepth 1 -type d \( -iname "*" ! -iname ".*" \) )

########################################################
## YOU SHOULDN'T NEED TO CHANGE ANYTHING BEYONG HERE! ##
########################################################

regular_make: externs

all: externs extras

externs:
	make -C xjimmies
	@echo ""; 
	@echo "**********************************"; 
	@echo "Building pdsheefa for $(MY_OS) $(MY_ARCH) " 
ifdef PDSHEEFA_STATIC_LIBS
	@echo ""; 
	@echo "LINKING WITH STATIC LIBS (macOS only)"; 
endif
	@echo ""; 
	@for i in $(EXTERNS); do \
	echo ""; \
	echo ">>>>>>>>>>>>>>>>>>>>> Making $$i <<<<<<<<<<<<<<<<<<<<<"; \
	($(MAKE) -C $$i ); \
	done

extras:
	@echo ""; 
	@for i in $(EXTRAS); do \
	echo ""; \
	echo ">>>>>>>>>>>>>>>>>>>>> Making $$i <<<<<<<<<<<<<<<<<<<<<"; \
	($(MAKE) -C $$i ); \
	done

install: install-externs install-patches

install-all: install-externs install-patches install-extras

install-externs:
	make -C xjimmies install-externs
	-mkdir -p  $(INSTALL_DIR); 
	@for i in $(EXTERNS); do \
	($(MAKE) -C $$i install) ; \
	done

install-extras:
	-mkdir -p  $(INSTALL_DIR); 
	@for i in $(EXTRAS); do \
	($(MAKE) -C $$i install) ; \
	done

install-patches:
	make -C xjimmies install-patches
	-mkdir -p  $(INSTALL_DIR); 
	-mkdir -p  $(INSTALL_DIR)/hidProfiles; 
	-mkdir -p  $(INSTALL_DIR)/rolloffs; 
	-mkdir -p  $(INSTALL_DIR)/examples; 
	cp ./patches/*.pd $(INSTALL_DIR)/.; 
	cp ./patches/plugins/*.pd $(INSTALL_DIR)/.; 
	cp ./patches/hidProfiles/*.pd $(INSTALL_DIR)/hidProfiles/.; \
	cp ./patches/rolloffs/*.txt $(INSTALL_DIR)/rolloffs/.; \
	cp ./patches/*.aif* $(INSTALL_DIR)/.; 
	#cp ./patches/LICENSE.txt  ./patches/README.txt $(INSTALL_DIR)/.; 
	cp ./examples/*.pd  $(INSTALL_DIR)/examples/.; 
	svn export --force ./examples/Resources $(INSTALL_DIR)/examples/Resources

dist:
#	echo $(shell sudo sh ./scripts/pdsheefa-bundler.sh $(VERSION-NUMBER))
	$(DIST_COMMAND)
	
uninstall:
	-rm -rf $(INSTALL_DIR)/*; \
	make -C xjimmies uninstall

clean:
	make -C xjimmies clean
	@for i in $(EXTERNS); do \
	($(MAKE) -C $$i clean); \
	done
	@for i in $(EXTRAS); do \
	($(MAKE) -C $$i clean); \
	done
