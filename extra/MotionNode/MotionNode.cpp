/**
 * MotionNode external
 * by Mike Wozniewski (mike@mikewoz.com)
 */

#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <pthread.h>

// includes for MotionNode SDK
#include <Client.hpp>
#include <LuaConsole.hpp>
#include <File.hpp>
#include <Format.hpp>

// include necessary for PD
#include "m_pd.h"


static pthread_mutex_t pthreadLock = PTHREAD_MUTEX_INITIALIZER;


// The SDK uses hardcoded ports?! ... seems bad:
// Use 32079 for preview data service.
// Use 32078 for sensor data service
// Use 32077 for raw data service.
// Use 32076 for configurable data service.
// Use 32075 for console service.
const unsigned PortPreview = 32079;
const unsigned PortSensor = 32078;
const unsigned PortRaw = 32077;
const unsigned PortConfigurable = 32076;
const unsigned PortConsole = 32075;

#define PI 3.14159265358979323846
#define RAD2DEG 57.29578

/**
 * MotionNode assumes Euler angles are defined by rotation around X, Y, then Z
 * with Y-up. Some libraries (eg, OpenSceneGraph) have Z-up, so we provide a
 * convenience function to provide these other Euler angles.
 */
MotionNode::SDK::Format::PreviewElement::data_type QuatToZupEuler(MotionNode::SDK::Format::PreviewElement::data_type q)
{
	MotionNode::SDK::Format::PreviewElement::data_type result(3);
	// based on:
	// en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles

	// but with Y and Z axes swapped
	
	double pitch, roll, yaw;
	
	roll = asin( 2* (q[1]*q[2] - q[0]*q[3]) );
	yaw = atan2( 2* (q[1]*q[3] + q[2]*q[0]) , 1 - (2* (q[3]*q[3] + q[2]*q[2])) );
	pitch =  atan2( 2* (q[1]*q[0] + q[3]*q[2]) , 1 - (2* (q[2]*q[2] + q[0]*q[0])) );
	
//	result[0] = PI - pitch;
	result[0] = pitch - PI;
	result[1] = -roll;
	result[2] = yaw;
	
	return result;
}


static t_class *MotionNode_class;
 
typedef struct _MotionNode {

	t_object  x_obj; //obj needed by pd   	

	t_symbol *configFile;
	
	MotionNode::SDK::Client *MotionNodeClient;
	MotionNode::SDK::Format::preview_service_type previewData;
	
	bool isConnected;

	// pthread stuff
    pthread_t pthreadID; // id of child thread
    pthread_attr_t pthreadAttr; 
    
	t_outlet *dataOutlet;
	t_outlet *statusOutlet;

} t_MotionNode;


void *MotionNode_poll(void *arg)
{
	using MotionNode::SDK::Client;
	using MotionNode::SDK::Format;

	t_MotionNode *x = (t_MotionNode *)arg;
	
	// set status to connected before going into the loop:
	x->isConnected = true;
	outlet_float(x->statusOutlet, (float)x->isConnected);

	try
	{
		while (x->isConnected)
		{
		
			// Use waitForData() to wait until there is incoming data on the open
			// connection. Default timeout is 5 seconds, but we can pass an arg to
			// override this.
			if (x->MotionNodeClient->waitForData())
			{
				
				// Read data samples in a loop. This is a blocking call so
				// we can simply wait on an open connection until a data
				// sample comes in.
				Client::data_type data;
				while (x->MotionNodeClient->readData(data) && (x->isConnected))
				{
					// must use a mutex, because the bang() method can read from
					// x->data at the same time:
					pthread_mutex_lock(&pthreadLock);
			        x->previewData = Format::Preview(data.begin(), data.end());
					pthread_mutex_unlock(&pthreadLock);
				}
				
			} // if waitForData()
			else {
				post("MotionNode: no data available");
			}
		}
		
	}

	catch (std::runtime_error & e)
	{
		post("MotionNode: ERROR: %s", e.what());
	}
	
	// destroy client upon exit from this thread:
	try
	{
		x->MotionNodeClient->close();
		delete x->MotionNodeClient;
	}
	catch (std::runtime_error & e)
	{
		post("MotionNode: ERROR: %s", e.what());
	}
	
	x->isConnected = false;
	post("MotionNode: disconnected");
	outlet_float(x->statusOutlet, (float)x->isConnected);
	
	
	
	return 0;
}

void MotionNode_bang(t_MotionNode *x)
{
	using MotionNode::SDK::Client;
	using MotionNode::SDK::Format;
	
	int i;
	t_atom outAtoms[16]; // max list size is 4x4 matrix
	
	pthread_mutex_lock(&pthreadLock);
	
	if (!x->previewData.empty())
	{
		Format::preview_service_type::iterator itr;
		for (itr=x->previewData.begin(); itr!=x->previewData.end(); ++itr)
		{
			Format::PreviewElement::data_type d;

			d = itr->second.getQuaternion(false); // false="global"
			for (i=0; i<4; i++) SETFLOAT (outAtoms+i, d[i]);
			outlet_anything(x->dataOutlet, gensym("quaternion"), 4, outAtoms);
			
			d = QuatToZupEuler(d);
			for (i=0; i<3; i++) SETFLOAT (outAtoms+i, d[i]);
			outlet_anything(x->dataOutlet, gensym("euler"), 3, outAtoms);
			
			/*
			d = itr->second.getEuler();
			for (i=0; i<3; i++) SETFLOAT (outAtoms+i, d[i]);
			outlet_anything(x->dataOutlet, gensym("euler2"), 3, outAtoms);
			*/		
			
			d = itr->second.getAccelerate();
			for (i=0; i<3; i++) SETFLOAT (outAtoms+i, d[i]);
			outlet_anything(x->dataOutlet, gensym("acceleration"), 3, outAtoms);

			/*
			d = itr->second.getMatrix(false); // false="global"
			for (i=0; i<16; i++) SETFLOAT (outAtoms+i, d[i]);
			outlet_anything(x->dataOutlet, gensym("matrix"), 16, outAtoms);
			*/
		}
	}
	
	pthread_mutex_unlock(&pthreadLock);
}

void MotionNode_init(t_MotionNode *x, t_symbol *host)
{
	using MotionNode::SDK::Client;
	using MotionNode::SDK::LuaConsole;

	try
	{
		// their SDK doesn't parse addresses properly, so we'll at 
		// least make a special case for "localhost":
		if (host==gensym("localhost") || host==gensym("")) host = gensym("127.0.0.1");

		// Open a connection to the console service, and use Lua to set all
		// sensors in reading mode:
		Client client(host->s_name, PortConsole);
		post("MotionNode: connected to CONSOLE service at %s:%d. Initializing sensor(s)", host->s_name, PortConsole);

	    // Scan for devices and start reading:
	    std::string lua_chunk =
	      "if not node.is_reading() then"
	      "   node.close()"
	      "   node.scan()"
	      "   node.start()"
	      " end"
	      " if node.is_reading() then"
	      "   print('Reading from ' .. node.num_reading() .. ' device(s)')"
	      " else"
	      "   print('Failed to start reading')"
	      " end"
	      ;

	    LuaConsole::result_type result = LuaConsole::SendChunk(client, lua_chunk);
	    if (LuaConsole::Success == result.first) {
	    	post("MotionNode: Success: %s", result.second.c_str());
	    } else {
	    	post("MotionNode: initialization failed. Manual start via web interface required. %s", (result.second).c_str());
	    	return;
	    }

	    client.close();
	}

	catch (std::runtime_error & e)
	{
		post("MotionNode: ERROR: %s", e.what());
		return;
	}
}

void MotionNode_connect(t_MotionNode *x, t_symbol *host)
{
	using MotionNode::SDK::Client;

	try
	{
		// their SDK doesn't parse addresses properly, so we'll at 
		// least make a special case for "localhost":
		if (host==gensym("localhost")) host = gensym("127.0.0.1");
		
	    // Now, open a connection to the previiew service, which will allow us
	    // to get data such as quaternions, euler angles, matrices, etc.
		x->MotionNodeClient = new Client(host->s_name, PortPreview);
		post("MotionNode: connected to data PREVIEW service at %s:%d", host->s_name, PortPreview);
	}

	catch (std::runtime_error & e)
	{
		post("MotionNode: ERROR: %s", e.what());
		return;
	}
	
	
	// start polling thread:
	if(pthread_attr_init(&x->pthreadAttr) < 0)
		printf("MotionNode: warning: could not prepare child thread\n");
	if(pthread_attr_setdetachstate(&x->pthreadAttr, PTHREAD_CREATE_DETACHED) < 0)
		printf("MotionNode: warning: could not prepare child thread\n");
	if(pthread_create( &x->pthreadID, &x->pthreadAttr, MotionNode_poll, x) < 0)
		printf("MotionNode: could not create new thread\n");

}

void MotionNode_disconnect(t_MotionNode *x)
{
	// set the thread loop condition to false:
	x->isConnected = false;
}



void *MotionNode_new(t_symbol *addr)
{
	t_MotionNode *x = (t_MotionNode *)pd_new(MotionNode_class);

	x->dataOutlet = outlet_new(&x->x_obj, &s_list);
	x->statusOutlet = outlet_new(&x->x_obj, &s_list);

	return (void *)x;
}

void MotionNode_free(t_MotionNode *x)
{
	x->MotionNodeClient->close();
	delete x->MotionNodeClient;
}

extern "C" void MotionNode_setup(void) {
	
	MotionNode_class = class_new(gensym("MotionNode"), (t_newmethod)MotionNode_new, (t_method)MotionNode_free, sizeof(t_MotionNode), CLASS_DEFAULT, A_DEFSYMBOL, 0);

	class_addbang   (MotionNode_class, MotionNode_bang);
	class_addmethod (MotionNode_class, (t_method)MotionNode_init, gensym("init"), A_DEFSYMBOL, 0);
	class_addmethod (MotionNode_class, (t_method)MotionNode_connect, gensym("connect"), A_DEFSYMBOL, 0);
	class_addmethod (MotionNode_class, (t_method)MotionNode_disconnect, gensym("disconnect"), A_DEFFLOAT, 0);	

}
