
// includes for connection to Vicon
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include "VrtSDK11.h"
#include "VrtSDK11ErrorCodes.h"

// include necessary for PD
#include "m_pd.h"


#define MAX_VICON_NAME_SIZE 80


static t_class *vicon2pd_class;
 
typedef struct _vicon2pd {

	t_object  x_obj; //obj needed by pd   	

	t_symbol *host;
	
	t_int numBodies;
	t_int numMarkers;

	t_atom data[7];

	t_outlet *generic_outlet;

} t_vicon2pd;


void vicon2pd_bang(t_vicon2pd *x)
{
	int i;
	float X, Y, Z, rX, rY, rZ, rW;
	char buf[MAX_VICON_NAME_SIZE];
	
	if (!ViconIsConnected()) return;
	
	// Get data:
#ifndef EXTERNAL
	ViconGetFrame(0,0);
#else
	ViconGetFrame(0);
#endif

	/*
	// Display Time stamp
	double dTimestamp;
	ViconGetFrameTimeStamp(&dTimestamp);
	post("  TimeStamp: %.0f\n", dTimestamp);
	*/
	
	/*
	for (int I = 0; I < v2pd->nNumMarkers; I++)
	{
		ViconGetMarkerName( I, v2pd->name );
		ViconGetMarkerByName( v2pd->name, &v2pd->X, &v2pd->Y, &v2pd->Z, &v2pd->V );
		//post("\n"+ name +": X) "+ v2pd->X + " - Y) "+ v2pd->Y + " - Z) "+ v2pd->Z + " - V) "+ v2pd->V);
		post("\n%s: %.2f, %.2f, %.2f, %.2f", v2pd->name, v2pd->X, v2pd->Y, v2pd->Z, v2pd->V);
		
		SETFLOAT(v2pd->data+0, I);
		SETFLOAT(v2pd->data+1, v2pd->X);
		SETFLOAT(v2pd->data+2, v2pd->Y);
		SETFLOAT(v2pd->data+3, v2pd->Z);
		SETFLOAT(v2pd->data+4, v2pd->V);
		outlet_anything(v2pd->out, &s_list, 5, v2pd->data);

	}
	*/


	for (i=0; i < x->numBodies; i++)
	{
		

		ViconGetBodyQuaternionByIndex( i, &X, &Y, &Z, &rX, &rY, &rZ, &rW );
		
		ViconGetBodyName( i, buf );
		
		SETSYMBOL(x->data+0, gensym(buf));
		SETFLOAT(x->data+1, X);
		SETFLOAT(x->data+2, Y);
		SETFLOAT(x->data+3, Z);
		SETFLOAT(x->data+4, rX);
		SETFLOAT(x->data+5, rY);
		SETFLOAT(x->data+6, rZ);
		
		outlet_list(x->generic_outlet, gensym("list"), 7, x->data);
	}

}


void vicon2pd_debug(t_vicon2pd *x)
{
	post("\n");
	post("***************************************");
	post("        [vicon2pd] DEBUG INFO:");
	post("\n");
	
	if (!ViconIsConnected())
	{
		post("Currently not connected to Vicon server");
		post("\n");
		return;
	}

	int i;
	char buf[MAX_VICON_NAME_SIZE];

	post("Bodies (%d):", x->numBodies);
	for (i=0; i < (x->numBodies); i++)
	{
		ViconGetMarkerName( i, buf );
		post("  %3d  %s", i, buf);
	}
	
	post("Markers (%d):", x->numMarkers);
	for (i=0; i < (x->numMarkers); i++)
	{
		ViconGetMarkerName( i, buf );
		post("  %3d  %s", i, buf);
	}
	
	post("\n");
	
}

void vicon2pd_connect(t_vicon2pd *x, t_symbol *addr)
{
	
	int err;
	
	x->host = addr;
	
	// connect:
	err = ViconConnect(x->host->s_name);
	
	if (ViconIsConnected())
	{
		post("Connected to Vicon server");
	} else {
		post("\nERROR: failed to connect to Vicon server (ERROR CODE: %d)", err);
		return;
	}
	

	// Retrieve number of bodies:
	err = ViconGetNumBodies((int *)(&x->numBodies));
	if ( err != VRT_OK )
	{
		post("\nERROR: could not get number of bodies");
	}

	// Retrieve number of markers:
	err = ViconGetNumMarkers((int *)(&x->numMarkers) );
	if ( err != VRT_OK )
	{
		post ("\nERROR: could not get number of markers (ERROR CODE: %d)", err);
	}

	
	post("  Number of bodies: %d", x->numBodies);
	post("  Number of Markers: %d", x->numMarkers);
	
}

void vicon2pd_refresh(t_vicon2pd *x)
{
	ViconClose();
	x->numBodies = 0;
	x->numMarkers = 0;
	vicon2pd_connect(x, x->host);
}

void vicon2pd_free(t_vicon2pd *x)
{
	ViconClose();
}

void *vicon2pd_new(t_symbol *addr)
{
	t_vicon2pd *x = (t_vicon2pd *)pd_new(vicon2pd_class);

	x->numBodies = 0;
	x->numMarkers = 0;
	
	x->generic_outlet = outlet_new(&x->x_obj, &s_list);

	return (void *)x;
}

extern "C" void vicon2pd_setup(void) {
	
	vicon2pd_class = class_new(gensym("vicon2pd"), (t_newmethod)vicon2pd_new, (t_method)vicon2pd_free, sizeof(t_vicon2pd), CLASS_DEFAULT, A_DEFSYMBOL, 0);

	class_addbang   (vicon2pd_class, vicon2pd_bang);
	class_addmethod (vicon2pd_class, (t_method)vicon2pd_debug, gensym("debug"), A_DEFFLOAT, 0);
	class_addmethod (vicon2pd_class, (t_method)vicon2pd_connect, gensym("connect"), A_DEFSYMBOL, 0);
	class_addmethod (vicon2pd_class, (t_method)vicon2pd_refresh, gensym("refresh"), A_DEFSYMBOL, 0);

}
