//-----------------------------------------------------------------------------
//	Vicon Real Time API.  DLL Entry point
//
//	Writen By:	Gus Lopez
//	Date:		10/22/01
//
//	(c)2001 Vicon Motion Systems, Inc.
//
//	Description:
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Aerospace Mechatronics Laboratory,
// McGill University, Montreal, Canada
//
// Modified By: Mikael Persson
// Date:		08/14/2007
//
// Modifications:
//		- Changed the GetFrame function such that only axis/angle and quaternion rotation representations
//		  are stored and the other representations are computed on demand, because
//		  typically, one host application will store its representation and not
//		  require it on multiple occasions so it could be costly to compute them in GetFrame().
//		- Changed all the IsConnected calls to bIsConnected == FALSE because the call to
//		  the function IsConnected() is already a more expensive operation then bIsConnected == FALSE.
//		- Added a request for closure by the ViconClose() function because we have observed
//		  that after multiple runs of the dll, the tarsus output started to loose performance,
//		  presumably because of opening multiple session with tarsus without closing them
//		  after fixing that there was no more problems of performance after multiple runs.
//		- Changes were made to the output error code to output the WSAGetLastError() whenever
//		  a winsock error occurs.
//		- Changed the function GetBodyAngleAxis to output the axis/angle without this aweful
//		  weird rotation applied to it.
//		- Added a MACRO GETDATA to handle the case where no channel index was found for a DOF.
//		- Fixed the indexing in Get***Name because the index was sent as a signed int and it was not
//		  verified that the value was positive.
//		- Fixed several other small bugs and issues all over the code.
//-----------------------------------------------------------------------------

// VrtSDK11.cpp : Defines the entry point for the DLL application.
//


#include "VrtSDK11.h"

#include <iostream>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <limits>
#include <math.h>

#include <stdio.h>
#include <string.h>

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "ClientCodes.h"

#include <assert.h>

#include "VrtSDK11ErrorCodes.h"
#include "VrtSDK11Local.h"




//-----------------------------------------------------------------------------
//	The recv call may return with a half-full buffer.
//	revieve keeps going until the buffer is actually full.

ErrorCodes eLastError;


bool receive(int Socket, char * pBuffer, int BufferSize)
{
  int result;
  result = read(Socket, pBuffer, BufferSize);
  if(result < 0)
    return false;
  return true;
}

//-----------------------------------------------------------------------------
//	There are also some helpers to make the code a little less ugly.

bool receive(int Socket, int & Val)
{
	return receive(Socket, (char*) & Val, sizeof(Val));
}

bool receive(int Socket, unsigned int & Val)
{
	return receive(Socket, (char*) & Val, sizeof(Val));
}

bool receive(int Socket, double & Val)
{
	return receive(Socket, (char*) & Val, sizeof(Val));
}

//-----------------------------------------------------------------------------

static	int SocketHandle;
static  struct sockaddr_in Endpoint;

static	std::vector< MarkerChannel >	MarkerChannels;
static	std::vector< BodyChannel >		BodyChannels;
static	std::vector< LocalBodyChannel >		LocalBodyChannels;

static	int	FrameChannel;

static double timestamp;
static	std::vector< MarkerData > markerPositions;
static  std::vector< BodyData > bodyPositions;
static  std::vector< LocalBodyData > localBodyPositions;

static 	std::vector< double > data;

// Mikael Persson: This will safely get a data value.
#define GETDATA(X) (((X) == -1) ? 0.0 : data[X])

static char buff[2040];



//-----------------------------------------------------------------------------


int ViconConnect(char *chIpAddr)
{
//	::MessageBox(NULL, chIpAddr, "ViconInit", 1);


	// *********Start of Init function****************

	// Create Socket

  SocketHandle = socket(AF_INET, SOCK_STREAM, 0);
  if (SocketHandle < 0) {
    perror("socket");
    return (-1);
  }

	//	Find Endpoint

	struct hostent*		pHostInfoEntry;

	static const int port = 800;

	memset(&Endpoint, 0, sizeof(Endpoint));
	Endpoint.sin_family	= AF_INET;
	Endpoint.sin_port	= htons(port);

	pHostInfoEntry = gethostbyname(chIpAddr);

	if(pHostInfoEntry)
		memcpy(&Endpoint.sin_addr,	pHostInfoEntry->h_addr,
										pHostInfoEntry->h_length);
	else
		Endpoint.sin_addr.s_addr = inet_addr(chIpAddr);

	if(Endpoint.sin_addr.s_addr == INADDR_NONE)
	{
	  perror ("inet_addr");
	  return -1;
	}

	//	Create Socket

	int result = connect( SocketHandle, (struct sockaddr*) & Endpoint, sizeof(Endpoint) );

	if(result < 0)
	{
	  perror ("Connect");
	  return -1;
	}
		std::vector< std::string > info;
		char * pBuff;

		//- Get Info - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		//	Request the channel information

		pBuff = buff;

		* ((int *) pBuff) = ClientCodes::EInfo;
		pBuff += sizeof(int);
		* ((int *) pBuff) = ClientCodes::ERequest;
		pBuff += sizeof(int);


		if (send (SocketHandle, buff, pBuff - buff, 0) < 0) {
		  perror("write");
		  return -1;
		}

		int packet;
		int type;

		if (read(SocketHandle, &packet, sizeof(packet)) < 0)
		{
			eLastError = ConnectFailed;
			//return VRT_ERRF_CONNECT | VRT_FAILED_REPLY;
			//throw std::string("Error Recieving Info Header");
      printf("Error Recieving Info Header\n");
		}

		if (read(SocketHandle, &type, sizeof(type)) < 0)
		{
			eLastError = ConnectFailed;
			//return VRT_ERRF_CONNECT | VRT_FAILED_REPLY;
			//throw std::string("Error Recieving Info Reply Header");
		  printf("Error Recieving Info Reply Header\n");
    }


		if(type != ClientCodes::EReply)
		{
		  printf ("packet is %ld, type is %ld\n", packet, type);

      eLastError = ConnectFailed;			
      //return VRT_ERRF_CONNECT | VRT_WRONG_REPLY;
			//throw std::string("Bad Packet: Info not Replied");
		  printf("Bad Packet: Info not Replied\n");
    }

		if(packet != ClientCodes::EInfo)
		{
			eLastError = ConnectFailed;
			//return VRT_ERRF_CONNECT | VRT_WRONG_REPLY;
			//throw std::string("Bad Reply Type: Info packet was not Replied");
		  printf("Bad Reply Type: Info packet was not Replied\n");
    }

		int size;

		if(!receive(SocketHandle, size))
		{
			eLastError = ConnectFailed;
			//return VRT_ERRF_CONNECT | VRT_WRONG_REPLY;
			//throw std::string("Error Recieving Info Size");
		  printf("Error Recieving Info Size\n");
    }

		info.resize(size);

		// Mikael Persson: Added one resize of the data array to the number of channels
		data.resize(size);

		std::vector< std::string >::iterator iInfo;

		//printf("got info (size=%d):", size);


		sleep(1); // added by mikewoz to force a wait until all data is received
				  // (especially for large models)
	
		for(iInfo = info.begin(); iInfo != info.end(); iInfo++)
		{
			
			int s;
			char c[255];
			char * p = c;

			if(!receive(SocketHandle, s))
			{
				eLastError = ConnectFailed;
				//return VRT_ERRF_CONNECT | VRT_FAILED_REPLY;
				//throw std::string();
        printf("cannot receive\n");
			}

			if(!receive(SocketHandle, c, s))
			{
				eLastError = ConnectFailed;
				//return VRT_ERRF_CONNECT | VRT_FAILED_REPLY;
				//throw std::string();
        printf("cannot receive\n");
			}
			

			p += s;

			*p = 0;

			*iInfo = std::string(c);
		}

		//- Parse Info - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		//	The info packets now contain the channel names.
		//	Identify the channels with the various dof's.

//		std::vector< MarkerChannel >	MarkerChannels;
//		std::vector< BodyChannel >		BodyChannels;
//		int	FrameChannel;

		for(iInfo = info.begin(); iInfo != info.end(); iInfo++)
		{
			//	Extract the channel type

			int openBrace = iInfo->find('<');

			if(openBrace == (int) iInfo->npos)
			{
				eLastError = ConnectFailed;
				return VRT_ERRF_CONNECT | VRT_CORRUPT_DATA;
				//throw std::string("Bad Channel Id: Wrong OpenBrace Position");
			}

			int closeBrace = iInfo->find('>');

			if(closeBrace == (int)iInfo->npos)
			{
				eLastError = ConnectFailed;
				return VRT_ERRF_CONNECT | VRT_CORRUPT_DATA;
				//throw std::string("Bad Channel Id: Wrong CloseBrace Position");
			}

			closeBrace++;

			std::string Type = iInfo->substr(openBrace, closeBrace-openBrace);

			//	Extract the Name

			std::string Name = iInfo->substr(0, openBrace);

			int space = Name.rfind(' ');

			if(space != (int)Name.npos)
				Name.resize(space);

			std::vector< MarkerChannel >::iterator iMarker;
			std::vector< BodyChannel >::iterator iBody;
			std::vector< LocalBodyChannel >::iterator iLocalBody;
			std::vector< std::string >::const_iterator iTypes;



			iMarker = std::find(	MarkerChannels.begin(),
									MarkerChannels.end(), Name);

			iLocalBody = std::find(LocalBodyChannels.begin(), LocalBodyChannels.end(), Name);
			iBody = std::find(BodyChannels.begin(), BodyChannels.end(), Name);


			if(iMarker != MarkerChannels.end())
			{
				//	The channel is for a marker we already have.
				iTypes = std::find(	ClientCodes::MarkerTokens.begin(), ClientCodes::MarkerTokens.end(), Type);
				if(iTypes != ClientCodes::MarkerTokens.end())
					iMarker->operator[](iTypes - ClientCodes::MarkerTokens.begin()) = iInfo - info.begin();
			}
			else
			if(iLocalBody != LocalBodyChannels.end())
			{
				//	The channel is for a local body we already have.
				iTypes = std::find(ClientCodes::LocalBodyTokens.begin(), ClientCodes::LocalBodyTokens.end(), Type);
				if(iTypes != ClientCodes::LocalBodyTokens.end())
					iLocalBody->operator[](iTypes - ClientCodes::LocalBodyTokens.begin()) = iInfo - info.begin();
			}
			else
			if(iBody != BodyChannels.end())
			{
				//	The channel is for a body we already have.
				iTypes = std::find(ClientCodes::BodyTokens.begin(), ClientCodes::BodyTokens.end(), Type);
				if(iTypes != ClientCodes::BodyTokens.end())
					iBody->operator[](iTypes - ClientCodes::BodyTokens.begin()) = iInfo - info.begin();
			}
			else

			if((iTypes = std::find(ClientCodes::MarkerTokens.begin(), ClientCodes::MarkerTokens.end(), Type))
					!= ClientCodes::MarkerTokens.end())
			{
				//	Its a new marker.
				MarkerChannels.push_back(MarkerChannel(Name));
				MarkerChannels.back()[iTypes - ClientCodes::MarkerTokens.begin()] = iInfo - info.begin();
			}
			else
			if((iTypes = std::find(ClientCodes::LocalBodyTokens.begin(), ClientCodes::LocalBodyTokens.end(), Type))
					!= ClientCodes::LocalBodyTokens.end())
			{
				//	Its a new local body.
				LocalBodyChannels.push_back(LocalBodyChannel(Name));
				LocalBodyChannels.back()[iTypes - ClientCodes::LocalBodyTokens.begin()] = iInfo - info.begin();
			}
			else
			if((iTypes = std::find(ClientCodes::BodyTokens.begin(), ClientCodes::BodyTokens.end(), Type))
					!= ClientCodes::BodyTokens.end())
			{
				//	Its a new body.
				BodyChannels.push_back(BodyChannel(Name));
				BodyChannels.back()[iTypes - ClientCodes::BodyTokens.begin()] = iInfo - info.begin();
			}
			else

			if(Type == "<F>")
			{
				FrameChannel = iInfo - info.begin();
			}
			//Get Time code channels.
			else
			{
				//	It could be a new channel type.
			}

		}
#ifndef EXTERNAL
	if(!privateRtConnect(chIpAddr))
		return VRT_FAIL;
#endif

	// Mikael Persson: Added One resize of the position arrays
	markerPositions.resize(MarkerChannels.size());
	bodyPositions.resize(BodyChannels.size());
	localBodyPositions.resize(LocalBodyChannels.size());

	isYup = false;
	bIsConnected = VRT_TRUE;
	return VRT_OK;
}

int ViconHasDataChanged()
{
	if (!mInfoChanged)
		return 0;

	mInfoChanged = false;
	return 1;
}

// Close connection with Tarsus
int ViconClose()
{
	if(bIsConnected == VRT_FALSE)
		return VRT_OK;

	char * pBuff;

	//	Request the channel closure

	pBuff = buff;

	* ((int *) pBuff) = ClientCodes::EClose;
	pBuff += sizeof(int);
	* ((int *) pBuff) = ClientCodes::ERequest;
	pBuff += sizeof(int);

	if(send(SocketHandle, buff, pBuff - buff, 0) < 0)
	  return -1;

	if(close(SocketHandle) < 0)
	  return -1;

	SocketHandle = -1;

	data.clear();
	BodyChannels.clear();
	bodyPositions.clear();
	MarkerChannels.clear();
	markerPositions.clear();
	LocalBodyChannels.clear();
	localBodyPositions.clear();

	bIsConnected = VRT_FALSE;
	return VRT_OK;
}

// Get one frame of data.
#ifndef EXTERNAL
int ViconGetFrame(int getUnlabeled, int getCircles)
#else
int ViconGetFrame(int getUnlabeled)
#endif
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETFRAME | VRT_NOT_CONNECTED;
	}

	char * pBuff;
	pBuff = buff;

	* ((int *) pBuff) = ClientCodes::EData;
	pBuff += sizeof(int);
	* ((int *) pBuff) = ClientCodes::ERequest;
	pBuff += sizeof(int);

	if(send(SocketHandle, buff, pBuff - buff, 0) < 0)
	{
		{
			ViconClose();
			if(ViconConnect(inet_ntoa(Endpoint.sin_addr)))
			  return -1;
			else {
			  if(send(SocketHandle, buff, pBuff - buff, 0) < 0)
				  return -1;
			}
		}
	}

	int packet;
	int type;
	int size;

	//	Get and check the packet header.

	if(!receive(SocketHandle, packet))
	{
		eLastError = GetFrameFailed;
		return VRT_ERRF_GETFRAME | VRT_FAILED_REPLY;
	}

	if(!receive(SocketHandle, type))
	{
		eLastError = GetFrameFailed;
		return VRT_ERRF_GETFRAME | VRT_FAILED_REPLY;
	}

	if(type != ClientCodes::EReply)
	{
		eLastError = GetFrameFailed;
		return VRT_ERRF_GETFRAME | VRT_WRONG_REPLY;
	}

	if(packet != ClientCodes::EData)
	{
		if(packet == ClientCodes::EInfoChange)
		{
			mInfoChanged = true;
			return VRT_OK;
		}
		eLastError = GetFrameFailed;
		return VRT_ERRF_GETFRAME | VRT_WRONG_REPLY;
	}

	if(!receive(SocketHandle, size))
	{
		eLastError = GetFrameFailed;
		return VRT_ERRF_GETFRAME | VRT_FAILED_REPLY;
	}

	//	Get the data.
	//data.resize( size ); Mikael Persson: this was already done once and for all.
	if(size != (int)data.size())
	{
		eLastError = GetFrameFailed;
		return VRT_ERRF_GETFRAME | VRT_CORRUPT_DATA;
	}

	std::vector< double >::iterator iData;

	for(iData = data.begin(); iData != data.end(); iData++)
	{
		if(!receive(SocketHandle, *iData))
		{
		eLastError = GetFrameFailed;
		return VRT_ERRF_GETFRAME | VRT_FAILED_REPLY;
		}
	}

	//- Look Up Channels - - - - - - - - - - - - - - - - - - - - - - -
	//  Get the TimeStamp

	timestamp = GETDATA(FrameChannel);

	//	Get the channels corresponding to the markers.
	//	Z is up
	//	The values are in millimeters

	std::vector< MarkerChannel >::iterator iMarker;
	std::vector< MarkerData >::iterator iMarkerData;

	for(
			iMarker = MarkerChannels.begin(),
			iMarkerData = markerPositions.begin();
			iMarker != MarkerChannels.end(); iMarker++, iMarkerData++)
	{
		if (!isYup)
		{
			iMarkerData->X = GETDATA(iMarker->X);
			iMarkerData->Y = GETDATA(iMarker->Y);
			iMarkerData->Z = GETDATA(iMarker->Z);
		}
		else
		{
			iMarkerData->X = GETDATA(iMarker->X);
			iMarkerData->Y = GETDATA(iMarker->Z);
			iMarkerData->Z = -(GETDATA(iMarker->Y));
		}

		if(GETDATA(iMarker->O) > 0.5)
			iMarkerData->Visible = false;
		else
			iMarkerData->Visible = true;
	}

	//	Get the channels corresponding to the bodies.
	//=================================================================
	//	The bodies are in global space
	//	The world is Z-up
	//	The translational values are in millimeters
	//	The rotational values are in radians
	//=================================================================

	std::vector< BodyChannel >::iterator iBody;
	std::vector< BodyData >::iterator iBodyData;

	for(	iBody = BodyChannels.begin(),
			iBodyData = bodyPositions.begin();
			iBody != BodyChannels.end(); iBody++, iBodyData++)
	{

		if (!isYup)
		{
			iBodyData->TX = GETDATA(iBody->TX);
			iBodyData->TY = GETDATA(iBody->TY);
			iBodyData->TZ = GETDATA(iBody->TZ);

			iBodyData->AX = GETDATA(iBody->RX);
			iBodyData->AY = GETDATA(iBody->RY);
			iBodyData->AZ = GETDATA(iBody->RZ);

			/* Mikael Persson: this was taken out because of competition with LocalBodyData.
			iBodyData->tx = GETDATA(iBody->tx);
			iBodyData->ty = GETDATA(iBody->ty);
			iBodyData->tz = GETDATA(iBody->tz);

			iBodyData->ax = GETDATA(iBody->rx);
			iBodyData->ay = GETDATA(iBody->ry);
			iBodyData->az = GETDATA(iBody->rz);*/
		}
		else
		{
			iBodyData->TX = GETDATA(iBody->TX);
			iBodyData->TY = GETDATA(iBody->TZ);
			iBodyData->TZ = -(GETDATA(iBody->TY));

			iBodyData->AX = GETDATA(iBody->RX);
			iBodyData->AY = GETDATA(iBody->RZ);
			iBodyData->AZ = -(GETDATA(iBody->RY));

			/* Mikael Persson: this was taken out because of competition with LocalBodyData.
			iBodyData->tx = GETDATA(iBody->tx);
			iBodyData->ty = GETDATA(iBody->tz);
			iBodyData->tz = -(GETDATA(iBody->ty));

			iBodyData->ax = GETDATA(iBody->rx);
			iBodyData->ay = GETDATA(iBody->rz);
			iBodyData->az = -(GETDATA(iBody->ry));*/

		}



		// *************************************************************
		//	The channel data is in the angle-axis form.
		//	The following converts this to a quaternion.
		//=============================================================
		//	An angle-axis is vector, the direction of which is the axis
		//	of rotation and the length of which is the amount of
		//	rotation in radians.
		//=============================================================

		double len, tmp;

		len = sqrt(	iBodyData->AX * iBodyData->AX +
					iBodyData->AY * iBodyData->AY +
					iBodyData->AZ * iBodyData->AZ);

		iBodyData->QW = cos(len / 2.0);
		tmp = sin(len / 2.0);
		if (len < 1e-10)
		{
			iBodyData->QX = iBodyData->AX;
			iBodyData->QY = iBodyData->AY;
			iBodyData->QZ = iBodyData->AZ;
		}
		else
		{
			iBodyData->QX = iBodyData->AX * tmp/len;
			iBodyData->QY = iBodyData->AY * tmp/len;
			iBodyData->QZ = iBodyData->AZ * tmp/len;
		}

		/* Mikael Persson: this was taken out because of competition with LocalBodyData.
		// Now for local body
		len = sqrt(	iBodyData->ax * iBodyData->ax +
					iBodyData->ay * iBodyData->ay +
					iBodyData->az * iBodyData->az);

		iBodyData->Qw = cos(len / 2.0);
		tmp = sin(len / 2.0);
		if (len < 1e-10)
		{
			iBodyData->Qx = iBodyData->ax;
			iBodyData->Qy = iBodyData->ay;
			iBodyData->Qz = iBodyData->az;
		}
		else
		{
			iBodyData->Qx = iBodyData->ax * tmp/len;
			iBodyData->Qy = iBodyData->ay * tmp/len;
			iBodyData->Qz = iBodyData->az * tmp/len;
		}*/

		//	The following converts global angle-axis to a rotation matrix.
		/* Mikael Persson: This is not very useful because it is a lot of unrequested calculations, better to do that only on request.
		// And since the quaternion representation is kept in memory, the euler angles and rotation matrix can be calculated more efficiently
		double c, s, x, y, z;

		if (len < 1e-15)
		{
			iBodyData->GlobalRotation[0][0] = iBodyData->GlobalRotation[1][1] = iBodyData->GlobalRotation[2][2] = 1.0;
			iBodyData->GlobalRotation[0][1] = iBodyData->GlobalRotation[0][2] = iBodyData->GlobalRotation[1][0] =
			iBodyData->GlobalRotation[1][2]	= iBodyData->GlobalRotation[2][0] = iBodyData->GlobalRotation[2][1] = 0.0;
		}
		else
		{
			x = GETDATA(iBody->RX)/len;
			y = GETDATA(iBody->RY)/len;
			z = GETDATA(iBody->RZ)/len;

			c = cos(len);
			s = sin(len);

			iBodyData->GlobalRotation[0][0] = c + (1-c)*x*x;
			iBodyData->GlobalRotation[0][1] =     (1-c)*x*y + s*(-z);
			iBodyData->GlobalRotation[0][2] =     (1-c)*x*z + s*y;
			iBodyData->GlobalRotation[1][0] =     (1-c)*y*x + s*z;
			iBodyData->GlobalRotation[1][1] = c + (1-c)*y*y;
			iBodyData->GlobalRotation[1][2] =     (1-c)*y*z + s*(-x);
			iBodyData->GlobalRotation[2][0] =     (1-c)*z*x + s*(-y);
			iBodyData->GlobalRotation[2][1] =     (1-c)*z*y + s*x;
			iBodyData->GlobalRotation[2][2] = c + (1-c)*z*z;
		}

		// now convert rotation matrix to nasty Euler angles (yuk)
		// you could convert direct from angle-axis to Euler if you wish

		//	'Look out for angle-flips, Paul...'
		//  Algorithm: GraphicsGems II - Matrix Techniques VII.1 p 320
		assert(fabs(iBodyData->GlobalRotation[0][2]) <= 1);
		iBodyData->EulerY = asin(-iBodyData->GlobalRotation[2][0]);

		if(fabs(cos(y)) >
			std::numeric_limits<double>::epsilon() ) 	// cos(y) != 0 Gimbal-Lock
		{
			iBodyData->EulerX = atan2(iBodyData->GlobalRotation[2][1], iBodyData->GlobalRotation[2][2]);
			iBodyData->EulerZ = atan2(iBodyData->GlobalRotation[1][0], iBodyData->GlobalRotation[0][0]);
		}
		else
		{
			iBodyData->EulerZ = 0;
			iBodyData->EulerX = atan2(iBodyData->GlobalRotation[0][1], iBodyData->GlobalRotation[1][1]);
		}


		//========================================================

//		double len, tmp;

		len = sqrt(	GETDATA(iBody->rx) * GETDATA(iBody->rx) +
					GETDATA(iBody->ry) * GETDATA(iBody->ry) +
					GETDATA(iBody->rz) * GETDATA(iBody->rz));
		if (len < 1e-15)
		{
			iBodyData->LocalRotation[0][0] = iBodyData->LocalRotation[1][1] = iBodyData->LocalRotation[2][2] = 1.0;
			iBodyData->LocalRotation[0][1] = iBodyData->LocalRotation[0][2] = iBodyData->LocalRotation[1][0] =
				iBodyData->LocalRotation[1][2]	= iBodyData->LocalRotation[2][0] = iBodyData->LocalRotation[2][1] = 0.0;
		}
		else
		{
			x = GETDATA(iBody->rx)/len;
			y = GETDATA(iBody->ry)/len;
			z = GETDATA(iBody->rz)/len;

			c = cos(len);
			s = sin(len);

			iBodyData->LocalRotation[0][0] = c + (1-c)*x*x;
			iBodyData->LocalRotation[0][1] =     (1-c)*x*y + s*(-z);
			iBodyData->LocalRotation[0][2] =     (1-c)*x*z + s*y;
			iBodyData->LocalRotation[1][0] =     (1-c)*y*x + s*z;
			iBodyData->LocalRotation[1][1] = c + (1-c)*y*y;
			iBodyData->LocalRotation[1][2] =     (1-c)*y*z + s*(-x);
			iBodyData->LocalRotation[2][0] =     (1-c)*z*x + s*(-y);
			iBodyData->LocalRotation[2][1] =     (1-c)*z*y + s*x;
			iBodyData->LocalRotation[2][2] = c + (1-c)*z*z;
		}

		// now convert rotation matrix to nasty Euler angles (yuk)
		// you could convert direct from angle-axis to Euler if you wish

		//	'Look out for angle-flips, Paul...'
		//  Algorithm: GraphicsGems II - Matrix Techniques VII.1 p 320
		assert(fabs(iBodyData->LocalRotation[0][2]) <= 1);
		iBodyData->Euler_y = asin(-iBodyData->LocalRotation[2][0]);

		if(fabs(cos(y)) >
			std::numeric_limits<double>::epsilon() ) 	// cos(y) != 0 Gimbal-Lock
		{
			iBodyData->Euler_x = atan2(iBodyData->LocalRotation[2][1], iBodyData->LocalRotation[2][2]);
			iBodyData->Euler_z = atan2(iBodyData->LocalRotation[1][0], iBodyData->LocalRotation[0][0]);
		}
		else
		{
			iBodyData->Euler_z = 0;
			iBodyData->Euler_x = atan2(iBodyData->LocalRotation[0][1], iBodyData->LocalRotation[1][1]);
		}
		*/
//========================================================
	}

	//	Mikael Persson: Get the channels corresponding to the local bodies.
	//=================================================================
	//	The bodies are in global space
	//	The world is Z-up
	//	The translational values are in millimeters
	//	The rotational values are in radians
	//=================================================================

	std::vector< LocalBodyChannel >::iterator iLocalBody;
	std::vector< LocalBodyData >::iterator iLocalBodyData;

	for(	iLocalBody = LocalBodyChannels.begin(),
			iLocalBodyData = localBodyPositions.begin();
			iLocalBody != LocalBodyChannels.end(); iLocalBody++, iLocalBodyData++)
	{
		if (!isYup)
		{
			iLocalBodyData->tx = GETDATA(iLocalBody->tx);
			iLocalBodyData->ty = GETDATA(iLocalBody->ty);
			iLocalBodyData->tz = GETDATA(iLocalBody->tz);

			iLocalBodyData->ax = GETDATA(iLocalBody->rx);
			iLocalBodyData->ay = GETDATA(iLocalBody->ry);
			iLocalBodyData->az = GETDATA(iLocalBody->rz);
		}
		else
		{
			iLocalBodyData->tx = GETDATA(iLocalBody->tx);
			iLocalBodyData->ty = GETDATA(iLocalBody->tz);
			iLocalBodyData->tz = -(GETDATA(iLocalBody->ty));

			iLocalBodyData->ax = GETDATA(iLocalBody->rx);
			iLocalBodyData->ay = GETDATA(iLocalBody->rz);
			iLocalBodyData->az = -(GETDATA(iLocalBody->ry));
		}



		// *************************************************************
		//	The channel data is in the angle-axis form.
		//	The following converts this to a quaternion.
		//=============================================================
		//	An angle-axis is vector, the direction of which is the axis
		//	of rotation and the length of which is the amount of
		//	rotation in radians.
		//=============================================================

		double len, tmp;

		len = sqrt(	iLocalBodyData->ax * iLocalBodyData->ax +
					iLocalBodyData->ay * iLocalBodyData->ay +
					iLocalBodyData->az * iLocalBodyData->az);

		iLocalBodyData->qw = cos(len / 2.0);
		tmp = sin(len / 2.0);
		if (len < 1e-10)
		{
			iLocalBodyData->qx = iLocalBodyData->ax;
			iLocalBodyData->qy = iLocalBodyData->ay;
			iLocalBodyData->qz = iLocalBodyData->az;
		}
		else
		{
			iLocalBodyData->qx = iLocalBodyData->ax * tmp/len;
			iLocalBodyData->qy = iLocalBodyData->ay * tmp/len;
			iLocalBodyData->qz = iLocalBodyData->az * tmp/len;
		}
	}


	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//  The marker and body data now resides in the arrays
	//	markerPositions & bodyPositions.

	if (getUnlabeled)
	{
		int tmp_err = privateRtGetFrame();
		if(tmp_err)
			return tmp_err;
	}

//For internal builds only.
#ifndef EXTERNAL
	if (getCircles)
		privateRtGetFrameCircles();
#endif

	return VRT_OK;
}



int ViconIsConnected(void)
{
	if ( bIsConnected )
		return VRT_TRUE;

	return VRT_FALSE;
}


void ViconGetLastError(char *chError)
{

	switch (eLastError)
	{
		case ConnectFailed:
			strcpy( chError, "Connection to real- engine did not succeeded!" );
			break;

		case NotConnected:
			strcpy( chError, "No active connection with real-time engine available!" );
			break;

		case GetFrameFailed:
			strcpy( chError, "Failed to retrieve frame data!" );
			break;

		default:
		  strcpy (chError, "No errors reported.");
		  break;

	}
}


int ViconGetFrameTimeStamp(double *dTimeStamp)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETFRAMETIMESTAMP | VRT_NOT_CONNECTED;
	}
	*dTimeStamp = timestamp;
	return VRT_OK;
}



// Get number of markers available

int ViconGetNumMarkers( int *markerNum )
{

	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETNUMMARKERS | VRT_NOT_CONNECTED;
	}
	*markerNum = MarkerChannels.size();
	return VRT_OK;
}

int ViconGetMarkerName( int markerNum, char *name )
{
	//char buff[80];

	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETMARKERNAME | VRT_NOT_CONNECTED;
	}

	if( (markerNum < 0) || (markerNum >= (int)MarkerChannels.size()) )
		return VRT_ERRF_GETMARKERNAME | VRT_INVALIDARG;

	/* Mikael Persson: This is weird and inefficient (one string copy operation becomes two string copies and one iteration!!)
	std::string::iterator iS;
	std::string::iterator iBuff;

	std::string & rName = MarkerChannels[ markerNum ].Name;

	for( iS = rName.begin(), iBuff = buff; iS != rName.end(); iS++, iBuff++ )
		*iBuff = *iS;
	buff[iBuff-buff] = '\0'; //null-terminated

	//strcpy( name, rName.c_str ); //illegal
	strcpy( name, buff );
	*/

	// Mikael Persson: This is the old-fashion but more efficient, stable and reliable way to copy a string.
	memmove(name,MarkerChannels[markerNum].Name.data(),MarkerChannels[markerNum].Name.size());
	name[MarkerChannels[markerNum].Name.size()] = '\0'; //null-termination

	return VRT_OK;
}

//Mikael Persson: This was added for efficiency gain (this is a real-time engine after all!).
int ViconGetMarkerByIndex(int markerNum, float* a_rX, float* a_rY, float* a_rZ, int* a_rV)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETMARKERBYINDEX | VRT_NOT_CONNECTED;
	}

	if( (markerNum < 0) || (markerNum >= (int)MarkerChannels.size()) )
		return VRT_ERRF_GETMARKERBYINDEX | VRT_INVALIDARG;

	const MarkerData & rMarker = *(markerPositions.begin() + (markerNum));

	*a_rX = (float)rMarker.X;
	*a_rY = (float)rMarker.Y;
	*a_rZ = (float)rMarker.Z;

	*a_rV = rMarker.Visible ? 1 : 0;
	return VRT_OK;
}

int ViconGetMarkerByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, int *a_rV )
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETMARKERBYNAME | VRT_NOT_CONNECTED;
	}

	std::string Name( a_pName );
	std::vector< MarkerChannel >::iterator iMarker;
	iMarker = std::find(	MarkerChannels.begin(),
							MarkerChannels.end(), Name);

	/* Mikael Persson: Once the marker is found, you only need to call the index function.
	if( iMarker == MarkerChannels.end() )
		return E_INVALIDARG;

	const MarkerData & rMarker = *(markerPositions.begin() + (iMarker - MarkerChannels.begin()));

	*a_rX = (float)rMarker.X;
	*a_rY = (float)rMarker.Y;
	*a_rZ = (float)rMarker.Z;

	*a_rV = rMarker.Visible ? 1 : 0;
	return S_OK; */

	return ViconGetMarkerByIndex(iMarker - MarkerChannels.begin(),a_rX,a_rY,a_rZ,a_rV);
}


// Get number of bodies available
int ViconGetNumBodies( int *bodyNum )
{

	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETNUMBODIES | VRT_NOT_CONNECTED;
	}

	*bodyNum = BodyChannels.size();
	return VRT_OK;
}

// Get name of body number bodyNum
int ViconGetBodyName( int bodyNum, char *name )
{
	//char buff[80]; //Mikael Persson: Remark: it is not good programming practice to have global and local variables with the same name, even if the compiler may accept that.

	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETBODYNAME | VRT_NOT_CONNECTED;
	}

	if( (bodyNum < 0) || (bodyNum >= (int)BodyChannels.size()) )
		return VRT_ERRF_GETBODYNAME | VRT_INVALIDARG;

	/* Mikael Persson: This is weird and inefficient (one string copy operation becomes two string copies and one iteration!!)
	std::string::iterator iS;
	std::string::iterator iBuff;

	std::string & rName = BodyChannels[ bodyNum ].Name;

	for( iS = rName.begin(), iBuff = buff; iS != rName.end(); iS++, iBuff++ )
		*iBuff = *iS;
	buff[iBuff-buff] = '\0'; //null-terminated

	//strcpy( name, rName.c_str ); //illegal
	strcpy( name, buff );
	*/

	// Mikael Persson: This is the old-fashion way more efficient, stable and reliable way to copy a string.
	memmove(name,BodyChannels[bodyNum].Name.data(),BodyChannels[bodyNum].Name.size());
	name[BodyChannels[bodyNum].Name.size()] = '\0'; //null-termination

	return VRT_OK;
}

//Mikael Persson: This was added for efficiency gain (this is a real-time engine after all!).
// Get body data in angle-axis format
int ViconGetBodyAngleAxisByIndex(int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETBODYANGLEAXISBYINDEX | VRT_NOT_CONNECTED;
	}

	if( (bodyNum < 0) || (bodyNum >= (int)BodyChannels.size()) )
		return VRT_ERRF_GETBODYANGLEAXISBYINDEX | VRT_INVALIDARG;

	const BodyData & rBody = *(bodyPositions.begin() + (bodyNum));

	*a_rX = (float)rBody.TX;
	*a_rY = (float)rBody.TY;
	*a_rZ = (float)rBody.TZ;

	*a_rEX = (float)rBody.AX;
	*a_rEY = (float)rBody.AY;
	*a_rEZ = (float)rBody.AZ;

	return VRT_OK;
}

// Get body data in angle-axis format
int ViconGetBodyAngleAxisByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETBODYANGLEAXISBYNAME | VRT_NOT_CONNECTED;
	}

	std::string Name( a_pName );
	std::vector< BodyChannel >::iterator iBody;
	iBody = std::find(	BodyChannels.begin(),
						BodyChannels.end(), Name);

	/* Mikael Persson: Once the body is found, you only need to call the index function.
	const BodyData & rBody = *(bodyPositions.begin() + (iBody - BodyChannels.begin()));

	*a_rX = (float)rBody.TX;
	*a_rY = (float)rBody.TY;
	*a_rZ = (float)rBody.TZ;

	*a_rEX = (float)rBody.AX;
	*a_rEY = (float)rBody.AY;
	*a_rEZ = (float)rBody.AZ;

	return VRT_OK;
	*/
	return ViconGetBodyAngleAxisByIndex(iBody - BodyChannels.begin(),a_rX,a_rY,a_rZ,a_rEX,a_rEY,a_rEZ);
}

//Mikael Persson: This was added for efficiency gain (this is a real-time engine after all!).
// Get body data in quaternion format
int ViconGetBodyQuaternionByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rQX, float *a_rQY, float *a_rQZ, float *a_rQW)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETBODYQUATERNIONBYINDEX | VRT_NOT_CONNECTED;
	}

	if( (bodyNum < 0) || (bodyNum >= (int)BodyChannels.size()) )
		return VRT_ERRF_GETBODYQUATERNIONBYINDEX | VRT_INVALIDARG;

	const BodyData & rBody = *(bodyPositions.begin() + (bodyNum));

	*a_rX = (float)rBody.TX;
	*a_rY = (float)rBody.TY;
	*a_rZ = (float)rBody.TZ;

	*a_rQX = (float)rBody.QX;
	*a_rQY = (float)rBody.QY;
	*a_rQZ = (float)rBody.QZ;
	*a_rQW = (float)rBody.QW;

	return VRT_OK;
}

// Get body data in quaternion format
int ViconGetBodyQuaternionByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rQX, float *a_rQY, float *a_rQZ, float *a_rQW)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETBODYQUATERNIONBYNAME | VRT_NOT_CONNECTED;
	}

	std::string Name( a_pName );
	std::vector< BodyChannel >::iterator iBody;
	iBody = std::find(	BodyChannels.begin(),
						BodyChannels.end(), Name);

	/* Mikael Persson: Once the body is found, you only need to call the index function.
	const BodyData & rBody = *(bodyPositions.begin() + (iBody - BodyChannels.begin()));

	*a_rX = (float)rBody.TX;
	*a_rY = (float)rBody.TY;
	*a_rZ = (float)rBody.TZ;

	*a_rQX = (float)rBody.QX;
	*a_rQY = (float)rBody.QY;
	*a_rQZ = (float)rBody.QZ;
	*a_rQW = (float)rBody.QW;

	return VRT_OK;
	*/
	return ViconGetBodyQuaternionByIndex(iBody - BodyChannels.begin(),a_rX,a_rY,a_rZ,a_rQX,a_rQY,a_rQZ,a_rQW);
}

//Mikael Persson: This was added for efficiency gain (this is a real-time engine after all!).
//Gets the body data in Euler Angle format (Tait-Bryan convention, i.e. 3-2-1 body-fixed).
int ViconGetBodyEulerAnglesByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETBODYEULERANGLESBYINDEX | VRT_NOT_CONNECTED;
	}

	if( (bodyNum < 0) || (bodyNum >= (int)BodyChannels.size()) )
		return VRT_ERRF_GETBODYEULERANGLESBYINDEX | VRT_INVALIDARG;

	const BodyData & rBody = *(bodyPositions.begin() + (bodyNum));

	*a_rX = (float)rBody.TX;
	*a_rY = (float)rBody.TY;
	*a_rZ = (float)rBody.TZ;

	*a_rEX = atan2(2.0*((float)rBody.QW*(float)rBody.QX+(float)rBody.QY*(float)rBody.QZ),(1.0-2.0*(rBody.QX*rBody.QX+rBody.QY*rBody.QY)));
	*a_rEY = asin(2.0*((float)rBody.QW*(float)rBody.QY-(float)rBody.QZ*(float)rBody.QX));
	*a_rEZ = atan2(2.0*((float)rBody.QW*(float)rBody.QZ+(float)rBody.QX*(float)rBody.QY),(1.0-2.0*(rBody.QY*rBody.QY+rBody.QZ*rBody.QZ)));

	return VRT_OK;
}

//Gets the body data in Euler Angle format (Tait-Bryan convention, i.e. 3-2-1 body-fixed).
int ViconGetBodyEulerAnglesByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETBODYEULERANGLESBYNAME | VRT_NOT_CONNECTED;
	}

	std::string Name( a_pName );
	std::vector< BodyChannel >::iterator iBody;
	iBody = std::find(	BodyChannels.begin(),
						BodyChannels.end(), Name);

	/* Mikael Persson: Once the body is found, you only need to call the index function.
	const BodyData & rBody = *(bodyPositions.begin() + (iBody - BodyChannels.begin()));


	*a_rX = (float)rBody.TX;
	*a_rY = (float)rBody.TY;
	*a_rZ = (float)rBody.TZ;

	*a_rEX = (float)rBody.EulerX;
	*a_rEY = (float)rBody.EulerY;
	*a_rEZ = (float)rBody.EulerZ;

	return VRT_OK;
	*/
	return ViconGetBodyEulerAnglesByIndex(iBody - BodyChannels.begin(),a_rX,a_rY,a_rZ,a_rEX,a_rEY,a_rEZ);
}


int ViconGetBodyRotationMatrixByIndex(int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float a_rGlobalRotationMatrix[3][3])
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETBODYROTATIONMATRIXBYINDEX | VRT_NOT_CONNECTED;
	}

	if( (bodyNum < 0) || (bodyNum >= (int)BodyChannels.size()) )
		return VRT_ERRF_GETBODYROTATIONMATRIXBYINDEX | VRT_INVALIDARG;

	const BodyData & rBody = *(bodyPositions.begin() + (bodyNum));

	*a_rX = (float)rBody.TX;
	*a_rY = (float)rBody.TY;
	*a_rZ = (float)rBody.TZ;

	a_rGlobalRotationMatrix[0][0]=1.0-2.0*((float)rBody.QY*(float)rBody.QY+(float)rBody.QZ*(float)rBody.QZ);
	a_rGlobalRotationMatrix[1][0]=2.0*((float)rBody.QX*(float)rBody.QY+(float)rBody.QW*(float)rBody.QZ);
	a_rGlobalRotationMatrix[2][0]=2.0*((float)rBody.QX*(float)rBody.QZ-(float)rBody.QW*(float)rBody.QY);

	a_rGlobalRotationMatrix[0][1]=2.0*((float)rBody.QX*(float)rBody.QY-(float)rBody.QW*(float)rBody.QZ);
	a_rGlobalRotationMatrix[1][1]=1.0-2.0*((float)rBody.QX*(float)rBody.QX+(float)rBody.QZ*(float)rBody.QZ);
	a_rGlobalRotationMatrix[2][1]=2.0*((float)rBody.QW*(float)rBody.QX+(float)rBody.QY*(float)rBody.QZ);

	a_rGlobalRotationMatrix[0][2]=2.0*((float)rBody.QW*(float)rBody.QY+(float)rBody.QX*(float)rBody.QZ);
	a_rGlobalRotationMatrix[1][2]=2.0*((float)rBody.QY*(float)rBody.QZ-(float)rBody.QW*(float)rBody.QX);
	a_rGlobalRotationMatrix[2][2]=1.0-2.0*((float)rBody.QX*(float)rBody.QX+(float)rBody.QY*(float)rBody.QY);

	return VRT_OK;
}

// Get body data in rotation matrix format
int ViconGetBodyRotationMatrixByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float a_rGlobalRotationMatrix[3][3])
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETBODYROTATIONMATRIXBYNAME | VRT_NOT_CONNECTED;
	}

	std::string Name( a_pName );
	std::vector< BodyChannel >::iterator iBody;
	iBody = std::find(	BodyChannels.begin(),
						BodyChannels.end(), Name);

	/* Mikael Persson: Once the body is found, you only need to call the index function.
	const BodyData & rBody = *(bodyPositions.begin() + (iBody - BodyChannels.begin()));

	float fTemp;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			a_rGlobalRotationMatrix[i][j] =	(float)rBody.GlobalRotation[i][j];
		}
	}

	return VRT_OK;
	*/
	return ViconGetBodyRotationMatrixByIndex(iBody - BodyChannels.begin(),a_rX,a_rY,a_rZ,a_rGlobalRotationMatrix);
}


int ViconGetNumLocalBodies( int *bodyNum )
{

	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETNUMLOCALBODIES | VRT_NOT_CONNECTED;
	}

	*bodyNum = LocalBodyChannels.size();
	return VRT_OK;
}

int ViconGetLocalBodyName( int bodyNum, char *name )
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETLOCALBODYNAME | VRT_NOT_CONNECTED;
	}

	if( (bodyNum < 0) || (bodyNum >= (int)LocalBodyChannels.size()) )
		return VRT_ERRF_GETLOCALBODYNAME | VRT_INVALIDARG;

	// Mikael Persson: This is the old-fashion way more efficient, stable and reliable way to copy a string.
	memmove(name,BodyChannels[bodyNum].Name.data(),BodyChannels[bodyNum].Name.size());
	name[BodyChannels[bodyNum].Name.size()] = '\0'; //null-termination

	return VRT_OK;
}

//Mikael Persson: This was added for efficiency gain (this is a real-time engine after all!).
// Get body data in angle-axis format
int ViconGetLocalBodyAngleAxisByIndex(int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETLOCALBODYANGLEAXISBYINDEX | VRT_NOT_CONNECTED;
	}

	if( (bodyNum < 0) || (bodyNum >= (int)LocalBodyChannels.size()) )
		return VRT_ERRF_GETLOCALBODYANGLEAXISBYINDEX | VRT_INVALIDARG;

	const LocalBodyData & rBody = *(localBodyPositions.begin() + (bodyNum));

	*a_rX = (float)rBody.tx;
	*a_rY = (float)rBody.ty;
	*a_rZ = (float)rBody.tz;

	*a_rEX = (float)rBody.ax;
	*a_rEY = (float)rBody.ay;
	*a_rEZ = (float)rBody.az;

	return VRT_OK;
}

// Get body data in angle-axis format
int ViconGetLocalBodyAngleAxisByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETLOCALBODYANGLEAXISBYNAME | VRT_NOT_CONNECTED;
	}

	std::string Name( a_pName );
	std::vector< LocalBodyChannel >::iterator iBody;
	iBody = std::find(	LocalBodyChannels.begin(),
						LocalBodyChannels.end(), Name);

	return ViconGetLocalBodyAngleAxisByIndex(iBody - LocalBodyChannels.begin(),a_rX,a_rY,a_rZ,a_rEX,a_rEY,a_rEZ);
}

//Mikael Persson: This was added for efficiency gain (this is a real-time engine after all!).
// Get body data in quaternion format
int ViconGetLocalBodyQuaternionByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rQX, float *a_rQY, float *a_rQZ, float *a_rQW)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETLOCALBODYQUATERNIONBYINDEX | VRT_NOT_CONNECTED;
	}

	if( (bodyNum < 0) || (bodyNum >= (int)LocalBodyChannels.size()) )
		return VRT_ERRF_GETLOCALBODYQUATERNIONBYINDEX | VRT_INVALIDARG;

	const LocalBodyData & rBody = *(localBodyPositions.begin() + (bodyNum));

	*a_rX = (float)rBody.tx;
	*a_rY = (float)rBody.ty;
	*a_rZ = (float)rBody.tz;

	*a_rQX = (float)rBody.qx;
	*a_rQY = (float)rBody.qy;
	*a_rQZ = (float)rBody.qz;
	*a_rQW = (float)rBody.qw;

	return VRT_OK;
}

// Get body data in quaternion format
int ViconGetLocalBodyQuaternionByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rQX, float *a_rQY, float *a_rQZ, float *a_rQW)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETLOCALBODYQUATERNIONBYNAME | VRT_NOT_CONNECTED;
	}

	std::string Name( a_pName );
	std::vector< LocalBodyChannel >::iterator iBody;
	iBody = std::find(	LocalBodyChannels.begin(),
						LocalBodyChannels.end(), Name);

	return ViconGetLocalBodyQuaternionByIndex(iBody - LocalBodyChannels.begin(),a_rX,a_rY,a_rZ,a_rQX,a_rQY,a_rQZ,a_rQW);
}

//Mikael Persson: This was added for efficiency gain (this is a real-time engine after all!).
//Gets the body data in Euler Angle format (Tait-Bryan convention, i.e. 3-2-1 body-fixed).
int ViconGetLocalBodyEulerAnglesByIndex(int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETLOCALBODYEULERANGLESBYINDEX | VRT_NOT_CONNECTED;
	}

	if( (bodyNum < 0) || (bodyNum >= (int)LocalBodyChannels.size()) )
		return VRT_ERRF_GETLOCALBODYEULERANGLESBYINDEX | VRT_INVALIDARG;

	const LocalBodyData & rBody = *(localBodyPositions.begin() + (bodyNum));

	*a_rX = (float)rBody.tx;
	*a_rY = (float)rBody.ty;
	*a_rZ = (float)rBody.tz;

	*a_rEX = atan2(2.0*((float)rBody.qw*(float)rBody.qx+(float)rBody.qy*(float)rBody.qz),(1.0-2.0*(rBody.qx*rBody.qx+rBody.qy*rBody.qy)));
	*a_rEY = asin(2.0*((float)rBody.qw*(float)rBody.qy-(float)rBody.qz*(float)rBody.qx));
	*a_rEZ = atan2(2.0*((float)rBody.qw*(float)rBody.qz+(float)rBody.qx*(float)rBody.qy),(1.0-2.0*(rBody.qy*rBody.qy+rBody.qz*rBody.qz)));

	return VRT_OK;
}

//Gets the body data in Euler Angle format (Tait-Bryan convention, i.e. 3-2-1 body-fixed).
int ViconGetLocalBodyEulerAnglesByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETLOCALBODYEULERANGLESBYNAME | VRT_NOT_CONNECTED;
	}

	std::string Name( a_pName );
	std::vector< LocalBodyChannel >::iterator iBody;
	iBody = std::find(	LocalBodyChannels.begin(),
						LocalBodyChannels.end(), Name);

	return ViconGetBodyEulerAnglesByIndex(iBody - LocalBodyChannels.begin(),a_rX,a_rY,a_rZ,a_rEX,a_rEY,a_rEZ);
}


int ViconGetLocalBodyRotationMatrixByIndex(int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float a_rGlobalRotationMatrix[3][3])
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETLOCALBODYROTATIONMATRIXBYINDEX | VRT_NOT_CONNECTED;
	}

	if( (bodyNum < 0) || (bodyNum >= (int)LocalBodyChannels.size()) )
		return VRT_ERRF_GETLOCALBODYROTATIONMATRIXBYINDEX | VRT_INVALIDARG;

	const LocalBodyData & rBody = *(localBodyPositions.begin() + (bodyNum));

	*a_rX = (float)rBody.tx;
	*a_rY = (float)rBody.ty;
	*a_rZ = (float)rBody.tz;

	a_rGlobalRotationMatrix[0][0]=1.0-2.0*((float)rBody.qy*(float)rBody.qy+(float)rBody.qz*(float)rBody.qz);
	a_rGlobalRotationMatrix[1][0]=2.0*((float)rBody.qx*(float)rBody.qy+(float)rBody.qw*(float)rBody.qz);
	a_rGlobalRotationMatrix[2][0]=2.0*((float)rBody.qx*(float)rBody.qz-(float)rBody.qw*(float)rBody.qy);

	a_rGlobalRotationMatrix[0][1]=2.0*((float)rBody.qx*(float)rBody.qy-(float)rBody.qw*(float)rBody.qz);
	a_rGlobalRotationMatrix[1][1]=1.0-2.0*((float)rBody.qx*(float)rBody.qx+(float)rBody.qz*(float)rBody.qz);
	a_rGlobalRotationMatrix[2][1]=2.0*((float)rBody.qw*(float)rBody.qx+(float)rBody.qy*(float)rBody.qz);

	a_rGlobalRotationMatrix[0][2]=2.0*((float)rBody.qw*(float)rBody.qy+(float)rBody.qx*(float)rBody.qz);
	a_rGlobalRotationMatrix[1][2]=2.0*((float)rBody.qy*(float)rBody.qz-(float)rBody.qw*(float)rBody.qx);
	a_rGlobalRotationMatrix[2][2]=1.0-2.0*((float)rBody.qx*(float)rBody.qx+(float)rBody.qy*(float)rBody.qy);

	return VRT_OK;
}

// Get body data in rotation matrix format
int ViconGetLocalBodyRotationMatrixByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float a_rGlobalRotationMatrix[3][3])
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_ERRF_GETLOCALBODYROTATIONMATRIXBYNAME | VRT_NOT_CONNECTED;
	}

	std::string Name( a_pName );
	std::vector< LocalBodyChannel >::iterator iBody;
	iBody = std::find(	LocalBodyChannels.begin(),
						LocalBodyChannels.end(), Name);

	return ViconGetLocalBodyRotationMatrixByIndex(iBody - LocalBodyChannels.begin(),a_rX,a_rY,a_rZ,a_rGlobalRotationMatrix);
}




/* Mikael Persson: I deprecated this because I'm using the above code (same as for regular Bodies).
int ViconGetLocalBodyAngleAxis( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_FAIL;
	}

	std::string Name( a_pName );
	std::vector< BodyChannel >::iterator iBody;
	iBody = std::find(	BodyChannels.begin(),
						BodyChannels.end(), Name);

	const BodyData & rBody = *(bodyPositions.begin() + (iBody - BodyChannels.begin()));

	*a_rX = (float)rBody.tx;
	*a_rY = (float)rBody.ty;
	*a_rZ = (float)rBody.tz;


	*a_rEX = (float)rBody.ax;
	*a_rEY = (float)rBody.ay;
	*a_rEZ = (float)rBody.az;

	return VRT_OK;
}


int ViconGetLocalBodyEulerAnglesByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_FAIL;
	}

	if( (bodyNum < 0) || (bodyNum >= BodyChannels.size()) )
		return E_INVALIDARG;

	const BodyData & rBody = *(bodyPositions.begin() + (bodyNum));

	*a_rX = (float)rBody.tx;
	*a_rY = (float)rBody.ty;
	*a_rZ = (float)rBody.tz;

	*a_rEX = atan2(2.0*((float)rBody.Qw*(float)rBody.Qx+(float)rBody.Qy*(float)rBody.Qz),(1.0-2.0*(rBody.Qx*rBody.Qx+rBody.Qy*rBody.Qy)));
	*a_rEY = asin(2.0*((float)rBody.Qw*(float)rBody.Qy-(float)rBody.Qz*(float)rBody.Qx));
	*a_rEZ = atan2(2.0*((float)rBody.Qw*(float)rBody.Qz+(float)rBody.Qx*(float)rBody.Qy),(1.0-2.0*(rBody.Qy*rBody.Qy+rBody.Qz*rBody.Qz)));

	return VRT_OK;
}

//Gets the local body data in Euler Angle format (Tait-Bryan convention, i.e. 3-2-1 body-fixed).
int ViconGetLocalBodyEulerAnglesByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ)
{
	//Make sure connection to Tarsus exists
	if(bIsConnected == VRT_FALSE)
	{
		eLastError = NotConnected;
		return VRT_FAIL;
	}

	std::string Name( a_pName );
	std::vector< BodyChannel >::iterator iBody;
	iBody = std::find(	BodyChannels.begin(),
						BodyChannels.end(), Name);

	/* Mikael Persson: Once the body is found, you only need to call the index function.
	const BodyData & rBody = *(bodyPositions.begin() + (iBody - BodyChannels.begin()));


	*a_rX = (float)rBody.tx;
	*a_rY = (float)rBody.ty;
	*a_rZ = (float)rBody.tz;


	*a_rEX = (float)rBody.Euler_x;
	*a_rEY = (float)rBody.Euler_y;
	*a_rEZ = (float)rBody.Euler_z;

	return VRT_OK;
	*//*
	return ViconGetLocalBodyEulerAnglesByIndex(
}
*/

void ViconSetYup()
{
	isYup = true;
}

void ViconResetYup()
{
	isYup = false;
}

//Private stream **************************************************************

int privateRtConnect(char *chIpAddr)
{

	// Create Socket

  SocketPrivateHandle = -1;

	struct protoent*	pProtocolInfoEntry;
	char*				protocol;
	int					type;

	protocol = "tcp";
	type = SOCK_STREAM;

	pProtocolInfoEntry = getprotobyname(protocol);
	assert(pProtocolInfoEntry);

	if(pProtocolInfoEntry)
		SocketPrivateHandle = socket(PF_INET, type, pProtocolInfoEntry->p_proto);

	if(SocketPrivateHandle < 0)
	{
		std::cout << "Socket Creation Error" << std::endl;
		return 0;
	}

	//	Extract Settings

//	bool Points(true);


	/* NOT USED - by Marcello Stani
	bool Help(false);
	bool Stream(false);
*/
	const std::string PointsToken("--points");
	const std::string HelpToken("--help");
	const std::string StreamToken("--stream");



	//	Find Endpoint

	struct hostent*		pHostInfoEntry;
	struct sockaddr_in	privateEndpoint;

	memset(&privateEndpoint, 0, sizeof(privateEndpoint));
	privateEndpoint.sin_family	= AF_INET;
	privateEndpoint.sin_port	= htons(803);

	pHostInfoEntry = gethostbyname(chIpAddr);

	if(pHostInfoEntry)
		memcpy(&privateEndpoint.sin_addr,	pHostInfoEntry->h_addr,
										pHostInfoEntry->h_length);
	else
		privateEndpoint.sin_addr.s_addr = inet_addr(chIpAddr);

	if(privateEndpoint.sin_addr.s_addr == INADDR_NONE)
	{
//		std::cout << "Bad Address: " << argv[argc - 1] << std::endl;
		return 0;
	}

	//	Create Socket

	int result = connect(	SocketPrivateHandle, (struct sockaddr*) & privateEndpoint, sizeof(privateEndpoint));

	if(result < 0)
	{
		std::cout << "Failed to create Socket" << std::endl;
		return 0;
	}

	//	A connection with the Vicon Realtime system is now open.
	//	The following section implements the new 'status client' interface.
	//
	//	Briefly:
	//
	//	The client sends a request that is a bit-vector that encodes the status
	//	information that the client requires.
	//
	//	The server (tarsus) sends the client the information ordered from least
	//	to most significant bits in the request.
	//
	//	If the streaming bit (the most significant) is set, then the server will
	//	send a reply as soon as the data is available... forever.
	ok = true;
	Points = true;
	return 1;
}

int privateRtGetFrame()
{
	//	Send request.
	//Make sure connection to Tarsus exists
		if(bIsConnected == VRT_FALSE)
		{
			eLastError = NotConnected;
			return VRT_FAIL;
		}

		unsigned int request = VStatusCodes::ETime;

		request |= VStatusCodes::EReconPoints;

		int result = send(SocketPrivateHandle, (const char *)(&request), sizeof(request), 0);

		if(result < 0)
		{
			std::cout << "Error Requesting" << std::endl;
			return 0;
		}

		//	If the request is streaming, receive forever.
		//	If the request is not streaming receive once.

//		for(j = 0; (j != 1 || Stream) && ok; j++)
		{
			//	Receive timestamp
			//	Time is the sample number measured at the
			//	Datastation frame-rate.

			unsigned int time;

			if(!receive(SocketPrivateHandle, time))
				return 0;

			std::cout << "Time:" << time << std::endl;

			if(Points)
			{
				//	Receive the points.
				//	The points are the same as the reconstructions without the extra
				//	camera information etc...
				//	x is the reconstruction position in mm with z-up.

				pointCount = 4;

				if(!receive(SocketPrivateHandle, pointCount))
						return 0;

				double x[3];
				pointList.clear();
				for(int i = 0; i < pointCount && ok; i++)
				{


					if(!receive(SocketPrivateHandle, (char*) & x[0], sizeof(double) * 3))
						return 0;

					//std::cout << x[0] << '\t' << x[1] << '\t' << x[2] << std::endl;
					pointList.insert(pointList.end(), x, x + 3);
				}
				pointListIterator = pointList.begin();

			}




		//	Sleep(1000);

			//	Finished recieving this field.
		}

	return 1;
}

int privateRtGetFrameCircles()
{

	unsigned int request = VStatusCodes::ETime;
	request |= VStatusCodes::ECircles;

	int result = send(SocketPrivateHandle, (const char *)(&request), sizeof(request), 0);

	if(result < 0)
	{
		std::cout << "Error Requesting" << std::endl;
		return 0;
	}

		//	If the request is streaming, receive forever.
		//	If the request is not streaming receive once.

			//	Receive timestamp
			//	Time is the sample number measured at the
			//	Datastation frame-rate.

	unsigned int time;

	if(!receive(SocketPrivateHandle, time))
		return 0;

	if(!receive(SocketPrivateHandle, cameraCount))
		return 0;

	VCamData2D	currentCam;
	VCircle		currentCircle;
	//int channel; NOT USED - by Marcello Stani
	int unUsedCameraNum;
	//int numCirclesForCurrentCam;
	float xyr[3];

	camList.clear();
	currentCam.circleList.clear();
	for (int i = 0; i < cameraCount; i++)
	{
		if(!receive(SocketPrivateHandle, unUsedCameraNum))
			return 0;

		if(!receive(SocketPrivateHandle, currentCam.camChannel))
			return 0;

		if(!receive(SocketPrivateHandle, currentCam.numCircles))
			return 0;

		for (int j = 0; j < currentCam.numCircles; j++)
		{
			if(!receive(SocketPrivateHandle, (char*) & xyr[0], sizeof(float) * 3))
				return 0;

			currentCircle.x = xyr[0];
			currentCircle.y = xyr[1];
			currentCircle.r = xyr[2];

			currentCam.circleList.insert(currentCam.circleList.end(), currentCircle);

		}

		camList.insert(camList.end(), currentCam);
		currentCam.circleList.clear();
	}


	return 1;
}

int privateRtGetNextPoint(double point[3])
{
	for(int i = 0; i < 3; i++)
	{
		point[i] = *pointListIterator;
		pointListIterator++;
	}

	return true;
}




int privateRtGetPointCount()
{
	return pointCount;
}


//NOTE: *********************************************************************************
//The following funtions are for internal builds only.  Not for use outside the company.
//***************************************************************************************

#ifndef EXTERNAL

int privateRtGetCameraCount()
{
	return cameraCount;

}

int privateRtGetCameraChannelCircleCount(int camChannel)
{
	camListIterator = camList.begin();
	for (int i = 0; i < camChannel; i++)
		camListIterator++;

	VCamData2D cam = *camListIterator;

	return cam.numCircles;
}

int privateRtGetCameraChannelTarsusChannelNum(int camChannel)
{
	camListIterator = camList.begin();
	for (int i = 0; i < camChannel; i++)
		camListIterator++;

	VCamData2D cam = *camListIterator;

	return cam.camChannel;
}

int privateRtGetCameraChannelCircle(int camChannel, int circleNum, float *x, float *y, float *r)
{
	camListIterator = camList.begin();
	for (int i = 0; i < camChannel; i++)
		camListIterator++;

	VCamData2D cam = *camListIterator;

	cam.circleListIterator = cam.circleList.begin();
	for (int i = 0; i < circleNum; i++)
		cam.circleListIterator++;

	VCircle circle = *cam.circleListIterator;
	*x = circle.x;
	*y = circle.y;
	*r = circle.r;

	return 1;
}

#endif
