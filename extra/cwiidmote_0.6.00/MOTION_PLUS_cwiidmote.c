// =================================================================== 
// Wiimote external for Puredata 
// Written by Mike Wozniewki (Feb 2007), www.mikewoz.com 
// 
// Requires the CWiid library (version 0.6.00) by L. Donnie Smith 
// 
// =================================================================== 
// This program is free software; you can redistribute it and/or modify 
// it under the terms of the GNU General Public License as published by 
// the Free Software Foundation; either version 2 of the License, or 
// (at your option) any later version. 
// 
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
// GNU General Public License for more details. 
// 
// You should have received a copy of the GNU General Public License 
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA 
// =================================================================== 

//  ChangeLog:
//  2008-04-14 Florian Krebs 
//  * adapt wiimote external for the actual version of cwiid (0.6.00)
//  2008-08-11 Mike Wozniewski
//  * fix for multiple wiimotes

//
// ********NOTE:
// **MOTIONPLUS** ADDITIONS by Sonny Sung Jun Bae (sung.bae@mail.mcgill.ca) 
// MARKED BY " // START: TODO: MOTIONPLUS ADDED "
// Once changes are confirmed, delete comments;; OR keep comments, because MotionPlus is still being figured out
// OSC Mapping: wii/1/motionplus/ptp   (p = phi, t = theta, p = psi)
// **********
// 
#include <stdio.h> 
#include <unistd.h> 
#include <sys/select.h> 
#include <bluetooth/bluetooth.h> 
#include <m_pd.h> 
#include <math.h> 
#include <cwiid.h> 


#define PI 3.14159265358979323 
#define SCALER_UNIT_X (1./1024.)
#define SCALER_UNIT_Y (1./768.)

#define CWIID_POLL_MODE 1


// The followind is support for Pd polling functions:
typedef void (*t_fdpollfn)(void *ptr, int fd);
void sys_addpollfn(int fd, t_fdpollfn fn, void *ptr);
void sys_rmpollfn(int fd);

struct acc { 
	unsigned char x; 
	unsigned char y; 
	unsigned char z; 
};

/* Wiimote Callback */
cwiid_mesg_callback_t cwiid_callback; 
 


// CLASS STATIC VARS

// enum wiiTX {WII_NO,E_TYPE,SUB_TYPE, DATA1,DATA2,DATA3,DATA4,TX_MESSLEN};
// enum wiiTX {E_TYPE,SUB_TYPE, DATA1,DATA2,DATA3,DATA4,TX_MESSLEN};
enum wiiTX {SUB_TYPE, DATA1,DATA2,DATA3,DATA4,TX_MESSLEN};

static t_atom wii_tx_mess[TX_MESSLEN+1];
// START: TODO: MOTIONPLUS ADDED
// s_motionplus and s_ptp added
static t_symbol *s_accel, *s_nc_accel, *s_button, *s_nc_button, *s_nc_stick, \
				*s_ir, *s_xys, *s_xyz, *s_xy, *s_pry, *s_A, *s_B, *s_1, *s_2, \
				*s_Down, *s_Home, *s_Left, *s_Minus, *s_Plus, *s_Right, *s_Up, \
				*s_connect, *s_nunchuck, *s_joy, *s_C, *s_Z, *s_motionplus, *s_ptp;
//END: TODO: MOTIONPLUS ADDED


// class and struct declarations for wiimote pd external: 
static t_class *cwiidmote_class; 

typedef struct _cwiidmote 
{ 
	t_object x_obj; // standard pd object (must be first in struct) 
	 
	t_clock *x_clock; // Pd clock used for periodic polling

	cwiid_wiimote_t *cwiidmote; // individual wiimote handle per pd object, represented in libcwiid 
 
	t_float connected; 
	//int wiimoteID; 
	
	unsigned int buttonstate;
	t_symbol *hardwareID;

	// START: TODO: MOTIONPLUS ADDED	
	// toggle_mp added
	t_float toggle_acc, toggle_ir, toggle_nc, toggle_mp; 
 	// END: TODO: MOTIONPLUS ADDED

	//struct acc acc_zero, acc_one; // acceleration 
	//struct acc nc_acc_zero, nc_acc_one; // nunchuck acceleration

	struct acc_cal wii_calib, nc_calib;
 
	t_float nc_button_C;
	t_float nc_button_Z;

	// outlets: 
	t_outlet *outlet_left; 
	t_outlet *outlet_right; 

} t_cwiidmote; 



 
// For now, we have a big huge array for a wiimote list, because Cwiid returns
// a new id with each new connection (even for the same wiimote address).
// ie, the first connected wiimote will have id 0, the next will have id 1, but
// if the first disconnects and reconnects, it will get id 2.

// Thus, the g_wiimoteList stores pd object pointers for connected wiimotes,
// indexed by the current id.
#define MAX_cwiidmote_ID 1024 
#define MAX_WII_COUNT 255

t_cwiidmote *g_cwiidmoteList[MAX_cwiidmote_ID+1]; 
 
 
// ============================================================== 
void cwiidmote_debug(t_cwiidmote *x) 
{ 
	post("\n======================"); 
	
	if (x->connected) 
	{
		post("cwiidmote (hardwareID: %s) (cwiid id %d) is connected",  x->hardwareID->s_name,cwiid_get_id(x->cwiidmote)); 
	}
	else 
		post("cwiidmote is NOT connected"); 

	if (x->toggle_acc) post("acceleration: ON"); 
	else post("acceleration: OFF"); 
	if (x->toggle_ir)  post("IR:           ON"); 
	else post("IR:           OFF"); 
	if (x->toggle_nc)  post("Nunchuck:     ON"); 
	else post("Nunchuck:     OFF");
	// START: TODO: MOTIONPLUS ADDED 
	if (x->toggle_mp)  post("MotionPlus:   ON");
	else post("MotionPlus:   OFF");
	// END: TODO: MOTIONPLUS ADDED
	post(""); 
	post("Accelerometer calibration: zero=(%d,%d,%d) one=(%d,%d,%d)",x->wii_calib.zero[CWIID_X],x->wii_calib.zero[CWIID_Y],x->wii_calib.zero[CWIID_Z],x->wii_calib.one[CWIID_X],x->wii_calib.one[CWIID_Y],x->wii_calib.one[CWIID_Z]); 
	post("Nunchuck calibration:      zero=(%d,%d,%d) one=(%d,%d,%d)",x->nc_calib.zero[CWIID_X],x->nc_calib.zero[CWIID_Y],x->nc_calib.zero[CWIID_Z],x->nc_calib.one[CWIID_X],x->nc_calib.one[CWIID_Y],x->nc_calib.one[CWIID_Z]); 
	
 
} 
 
// ============================================================== 
 
// Button handler: 
void cwiidmote_btn(t_cwiidmote *x, struct cwiid_btn_mesg *mesg) 
{ 
	int i;
	t_symbol *buttonName;

	//post("Buttons: %X %X", (mesg->buttons & 0xFF00)>>8, data->btn_data.buttons & 0x00FF); 
	
	for (i=0;i<16;i++)
	{
		if (  ((mesg->buttons >> i) & 1) != ((x->buttonstate >> i ) & 0x1)  )
		{
			switch (1<<i)
			{
				case CWIID_BTN_2 :
					buttonName=s_2;
					break;
				case  CWIID_BTN_1 :
					buttonName=s_1;
					break;
				case  CWIID_BTN_B :
					buttonName=s_B;
					break;
				case  CWIID_BTN_A :
					buttonName=s_A;
					break;
				case  CWIID_BTN_MINUS :
					buttonName=s_Minus;
					break;
				case  CWIID_BTN_HOME :
					buttonName=s_Home;
					break;
				case  CWIID_BTN_LEFT :
					buttonName=s_Left;
					break;
				case  CWIID_BTN_RIGHT :
					buttonName=s_Right;
					break;
				case  CWIID_BTN_DOWN :
					buttonName=s_Down;
					break;
				case  CWIID_BTN_UP :
					buttonName=s_Up;
					break;
				case  CWIID_BTN_PLUS :
					buttonName=s_Plus;
					break;
				default :
					buttonName=gensym("unknown");
					break;
			}	
			
			SETSYMBOL(wii_tx_mess+SUB_TYPE, buttonName);  
			SETFLOAT(wii_tx_mess+DATA1, (t_float) ((mesg->buttons >> i ) & 0x1)); 

			
			outlet_anything(x->outlet_left, s_button, 2, wii_tx_mess);
		}
	}
	x->buttonstate = mesg->buttons;
} 

 
void cwiidmote_acc(t_cwiidmote *x, struct cwiid_acc_mesg *mesg) 
{ 
	if (x->toggle_acc) 
	{ 
		double a_x, a_y, a_z; 

		a_x = ((double)mesg->acc[CWIID_X] - x->wii_calib.zero[CWIID_X]) / (x->wii_calib.one[CWIID_X] - x->wii_calib.zero[CWIID_X]); 
		a_y = ((double)mesg->acc[CWIID_Y] - x->wii_calib.zero[CWIID_Y]) / (x->wii_calib.one[CWIID_Y] - x->wii_calib.zero[CWIID_Y]); 
		a_z = ((double)mesg->acc[CWIID_Z] - x->wii_calib.zero[CWIID_Z]) / (x->wii_calib.one[CWIID_Z] - x->wii_calib.zero[CWIID_Z]); 

		
		/* 
		double a, roll, pitch; 
		a = sqrt(pow(a_x,2)+pow(a_y,2)+pow(a_z,2)); 
		roll = atan(a_x/a_z); 
		if (a_z <= 0.0) roll += PI * ((a_x > 0.0) ? 1 : -1); 
		roll *= -1; 
		pitch = atan(a_y/a_z*cos(roll)); 
		*/ 
		SETSYMBOL(wii_tx_mess+SUB_TYPE, s_xyz); 
		SETFLOAT(wii_tx_mess+DATA1, (t_float) a_x);
		SETFLOAT(wii_tx_mess+DATA2, (t_float) a_y);
		SETFLOAT(wii_tx_mess+DATA3, (t_float) a_z);
		
		outlet_anything(x->outlet_left, s_accel, 4, wii_tx_mess); 
	} 	 
} 
 
void cwiidmote_ir(t_cwiidmote *x, struct cwiid_ir_mesg *mesg) 
{ 
	unsigned int i; 
 
	if (x->toggle_ir) 
	{ 
		//post("IR (valid,x,y,size) #%d: %d %d %d %d", i, data->ir_data.ir_src[i].valid, data->ir_data.ir_src[i].x, data->ir_data.ir_src[i].y, data->ir_data.ir_src[i].size); 
		for (i=0; i<CWIID_IR_SRC_COUNT; i++) 
		{		 
			if (mesg->src[i].valid) 
			{ 
				SETSYMBOL(wii_tx_mess+SUB_TYPE, s_xys); 
				SETFLOAT(wii_tx_mess+DATA1, (t_float) i+1);
				SETFLOAT(wii_tx_mess+DATA2, SCALER_UNIT_X*mesg->src[i].pos[CWIID_X]);
				SETFLOAT(wii_tx_mess+DATA3, SCALER_UNIT_Y*mesg->src[i].pos[CWIID_Y]);
				SETFLOAT(wii_tx_mess+DATA4, mesg->src[i].size); 
				outlet_anything(x->outlet_left, s_ir, 5, wii_tx_mess); 
			} 
		} 
	} 
} 
 
 
void cwiidmote_nunchuk(t_cwiidmote *x, struct cwiid_nunchuk_mesg *mesg) 
{ 
	double a_x, a_y, a_z; 

	a_x = ((double)mesg->acc[CWIID_X] - x->nc_calib.zero[CWIID_X]) / (x->nc_calib.one[CWIID_X] - x->nc_calib.zero[CWIID_X]); 
	a_y = ((double)mesg->acc[CWIID_Y] - x->nc_calib.zero[CWIID_Y]) / (x->nc_calib.one[CWIID_Y] - x->nc_calib.zero[CWIID_Y]); 
	a_z = ((double)mesg->acc[CWIID_Z] - x->nc_calib.zero[CWIID_Z]) / (x->nc_calib.one[CWIID_Z] - x->nc_calib.zero[CWIID_Z]); 
 
	/* 
	double a, roll, pitch; 
	a = sqrt(pow(a_x,2)+pow(a_y,2)+pow(a_z,2)); 
	roll = atan(a_x/a_z); 
	if (a_z <= 0.0) roll += PI * ((a_x > 0.0) ? 1 : -1); 
	roll *= -1; 
	pitch = atan(a_y/a_z*cos(roll)); 
	*/ 
	 
	
	// BUTTONS:
	if ((t_float)(mesg->buttons & CWIID_NUNCHUK_BTN_C) != x->nc_button_C)
	{
		x->nc_button_C = (t_float) (mesg->buttons & CWIID_NUNCHUK_BTN_C);
		SETSYMBOL(wii_tx_mess+SUB_TYPE, s_button);
		SETSYMBOL(wii_tx_mess+DATA1, s_C);
		SETFLOAT(wii_tx_mess+DATA2, x->nc_button_C);
		outlet_anything(x->outlet_left, s_nunchuck, 3, wii_tx_mess);
	}
	if ((t_float)(mesg->buttons & CWIID_NUNCHUK_BTN_Z) != x->nc_button_Z)
	{
		x->nc_button_Z = (t_float) (mesg->buttons & CWIID_NUNCHUK_BTN_Z);
		SETSYMBOL(wii_tx_mess+SUB_TYPE, s_button);
		SETSYMBOL(wii_tx_mess+DATA1, s_Z);
		SETFLOAT(wii_tx_mess+DATA2, x->nc_button_Z);
		outlet_anything(x->outlet_left, s_nunchuck, 3, wii_tx_mess);
	}
	
	//ACCEL EVENTS
	SETSYMBOL(wii_tx_mess+SUB_TYPE, s_accel); 
	SETSYMBOL(wii_tx_mess+DATA1, s_xyz); 
	SETFLOAT(wii_tx_mess+DATA2, a_x); 
	SETFLOAT(wii_tx_mess+DATA3, a_y); 
	SETFLOAT(wii_tx_mess+DATA4, a_z); 
	outlet_anything(x->outlet_left, s_nunchuck, 5, wii_tx_mess); 

	// JOYSTICK EVENTS
	SETSYMBOL(wii_tx_mess+SUB_TYPE, s_joy);
	SETFLOAT(wii_tx_mess+DATA1, mesg->stick[CWIID_X]); 
	SETFLOAT(wii_tx_mess+DATA2, mesg->stick[CWIID_Y]); 
	outlet_anything(x->outlet_left, s_nunchuck, 3, wii_tx_mess); 		
} 

// START: TODO: MOTIONPLUS ADDED 
void cwiidmote_motionplus(t_cwiidmote *x, struct cwiid_motionplus_mesg *mesg) 
{ 
	if (x->toggle_mp) 
	{ 
		double phi, theta, psi; 

		phi = (double)mesg->angle_rate[CWIID_PHI]; 
		theta = (double)mesg->angle_rate[CWIID_THETA]; 
		psi = (double)mesg->angle_rate[CWIID_PSI]; 

		SETSYMBOL(wii_tx_mess+SUB_TYPE, s_ptp); 
		SETFLOAT(wii_tx_mess+DATA1, (t_float) phi);
		SETFLOAT(wii_tx_mess+DATA2, (t_float) theta);
		SETFLOAT(wii_tx_mess+DATA3, (t_float) psi);
		
		outlet_anything(x->outlet_left, s_motionplus, 4, wii_tx_mess); 
	} 
} 
// END: TODO: MOTIONPLUS ADDED 

void cwiidmote_process_mesg(t_cwiidmote *x, int mesg_count, union cwiid_mesg mesg_array[])
{
	int i;
	unsigned char buf[7];

	for (i=0; i < mesg_count; i++)
	{
		switch (mesg_array[i].type) { 
			case CWIID_MESG_STATUS: 
				post("Battery: %d%", (int) (100.0 * mesg_array[i].status_mesg.battery / CWIID_BATTERY_MAX)); 
				switch (mesg_array[i].status_mesg.ext_type) { 
					case CWIID_EXT_NONE: 
						post("No nunchuck attached"); 
						break; 
					case CWIID_EXT_NUNCHUK: 
						post("Nunchuck extension attached"); 
						if (cwiid_get_acc_cal(x->cwiidmote, CWIID_EXT_NUNCHUK, &(x->nc_calib))) {
							post("Unable to retrieve Nunchuk calibration"); 
						} 
						break; 
					case CWIID_EXT_CLASSIC: 
						post("Classic controller attached. There is no support for this yet."); 
						break;
					// START: TODO: MOTIONPLUS ADDED 
					case CWIID_EXT_MOTIONPLUS:
						post("Motionplus extension attached");
						break;
					// END: TODO: MOTIONPLUS ADDED
					case CWIID_EXT_UNKNOWN: 
						post("Unknown extension attached"); 
						break; 
				} 
				break; 
			case CWIID_MESG_BTN:
				cwiidmote_btn(x, &mesg_array[i].btn_mesg); 
				break; 
			case CWIID_MESG_ACC: 
				cwiidmote_acc(x, &mesg_array[i].acc_mesg); 
				break; 
			case CWIID_MESG_IR: 
				cwiidmote_ir(x, &mesg_array[i].ir_mesg); 
				break; 
			case CWIID_MESG_NUNCHUK: 
				cwiidmote_nunchuk(x, &mesg_array[i].nunchuk_mesg); 
				break; 
			case CWIID_MESG_CLASSIC: 
				// todo 
				break; 
			// START: TODO: MOTIONPLUS ADDED 
			case CWIID_MESG_MOTIONPLUS:
				cwiidmote_motionplus(x, &mesg_array[i].motionplus_mesg);
				break;
			// END: TODO: MOTIONPLUS ADDED
			default: 
				break; 
		}
	}
}

void cwiidmote_poll(t_cwiidmote *x)
{
	union cwiid_mesg *mesg_array;
	int mesg_count;
	struct timespec t;
	struct cwiid_state currState;

	if (x->connected) 
	{ 
		if (!cwiid_get_mesg(x->cwiidmote, &mesg_count, &mesg_array, &t))
		{
			cwiidmote_process_mesg(x, mesg_count, mesg_array);
		}
	}
	clock_delay(x->x_clock, 5);
}


// The CWiid library invokes a callback function whenever events are 
// generated by the wiimote. This function is specified when connecting 
// to the wiimote (in the cwiid_open function).
void cwiid_callback(cwiid_wiimote_t *wiimote, int mesg_count, union cwiid_mesg mesg_array[], struct timespec *timestamp)
{
	t_cwiidmote *x = NULL;
	
	// get handle of pd instance from wiimote (stored in the big g_wiimoteList array):
	int thisID = cwiid_get_id(wiimote);
	if ((thisID >= 0) && (thisID < MAX_cwiidmote_ID))
	{
		x = g_cwiidmoteList[thisID];
	}
	
	if (x == NULL)
	{
		post("ERROR: got a message for an unregistered cwiidmote id: %d", thisID);
		return;
	}

	cwiidmote_process_mesg(x, mesg_count, mesg_array);
}

 
// ============================================================== 
void cwiidmote_setReportMode(t_cwiidmote *x, t_floatarg r) 
{ 
	unsigned char rpt_mode; 
 
	if (r >= 0) rpt_mode = (unsigned char) r; 
	else { 
		rpt_mode = CWIID_RPT_STATUS | CWIID_RPT_BTN; 
		if (x->toggle_ir) rpt_mode |= CWIID_RPT_IR; 
		if (x->toggle_acc) rpt_mode |= CWIID_RPT_ACC; 
		if (x->toggle_nc) rpt_mode |= CWIID_RPT_EXT;
		// START: TODO: MOTIONPLUS ADDED
		if (x->toggle_mp) rpt_mode |= CWIID_RPT_MOTIONPLUS;
		// END: TODO: MOTIONPLUS ADDED
	} 
	if (x->connected) 
	{ 
		if (cwiid_command(x->cwiidmote, CWIID_CMD_RPT_MODE, rpt_mode)) { 
			post("cwiidmote error: problem setting report mode."); 
		}
		if (x->toggle_nc)
		{
			if (cwiid_get_acc_cal(x->cwiidmote, CWIID_EXT_NUNCHUK, &(x->nc_calib)))
			{
				post("Unable to retrieve Nunchuk calibration"); 
			}
		}
	}
} 
 
void cwiidmote_reportAcceleration(t_cwiidmote *x, t_floatarg f) 
{ 
	x->toggle_acc = f;
	cwiidmote_setReportMode(x, -1); // update
} 
 
void cwiidmote_reportIR(t_cwiidmote *x, t_floatarg f) 
{ 
	x->toggle_ir = f; 
	cwiidmote_setReportMode(x, -1); // update
} 
 
void cwiidmote_reportNunchuck(t_cwiidmote *x, t_floatarg f) 
{ 
	x->toggle_nc = f; 
	cwiidmote_setReportMode(x, -1); // update
} 

// START: TODO: MOTIONPLUS ADDED
void cwiidmote_reportMotionPlus(t_cwiidmote *x, t_floatarg f) 
{ 
	x->toggle_mp = f; 
	cwiidmote_setReportMode(x, -1); // update
} 
	 
// END: TODO: MOTIONPLUS ADDED

void cwiidmote_setRumble(t_cwiidmote *x, t_floatarg f) 
{ 
	if (x->connected) 
	{ 
		if (cwiid_command(x->cwiidmote, CWIID_CMD_RUMBLE, f)) post("wiiremote error: problem setting rumble."); 
	} 
} 
 
void cwiidmote_setLED(t_cwiidmote *x, t_floatarg f) 
{ 
	// some possible values: 
	// CWIID_LED0_ON		0x01 
	// CWIID_LED1_ON		0x02 
	// CWIID_LED2_ON		0x04 
	// CWIID_LED3_ON		0x08 
	if (x->connected) 
	{ 
		if (cwiid_command(x->cwiidmote, CWIID_CMD_LED, f)) post("wiiremote error: problem setting LED."); 
	} 
} 

void cwiidmote_beep(t_cwiidmote *x)
{
	if (x->connected) 
	{ 
		cwiid_beep(x->cwiidmote);
	}
}
 
 
 
// ============================================================== 
// The following function attempts to connect to a wiimote at a 
// specific address, provided as an argument. eg, 00:19:1D:70:CE:72 
// This address can be discovered by running the following command 
// in a console: 
//   hcitool scan | grep Nintendo
 
void cwiidmote_doConnect(t_cwiidmote *x, t_symbol *addr) 
{ 
	unsigned char buf[7]; 
	char uniqueid[255]="unammed";
	
	bdaddr_t bdaddr; 
	// determine address: 
	if (addr==gensym("NULL")) {
		post("Searching automatically...");		
		bdaddr = *BDADDR_ANY;
	} 
	else {
		str2ba(addr->s_name, &bdaddr);
		post("Connecting to given address...");
		post("Press buttons 1 and 2 simultaneously.");
	} 

	// connect: 
	x->cwiidmote = cwiid_open(&bdaddr,CWIID_FLAG_MESG_IFC);
	
	if (x->cwiidmote == NULL) {
		post("cwiidmote error: unable to connect");

	} else {
		
		
		int thisID = cwiid_get_id(x->cwiidmote);
		
		// put this pd object pointer into the global list:
		if ((thisID<0) || (thisID>MAX_cwiidmote_ID))
		{
			post("ERROR. This cwiidmote cannot be registered because id %d is invalid. The maximum possible id is %d.", thisID, MAX_cwiidmote_ID);
			cwiid_close(x->cwiidmote);
			return;
		}
		g_cwiidmoteList[thisID] = x;		
		
		
		ba2str(&bdaddr,uniqueid);
		x->hardwareID=gensym(uniqueid);
		post("wii  has successfully connected. id=%d   hardwareID=%s",  thisID,x->hardwareID->s_name);
		
		if (cwiid_get_acc_cal(x->cwiidmote, CWIID_EXT_NONE, &(x->wii_calib))) {
			post("Unable to retrieve accelerometer calibration"); 
		}

		x->connected = 1;
		cwiidmote_setReportMode(x,-1);
		
		SETFLOAT(wii_tx_mess,  (t_float) x->connected); 
		SETSYMBOL(wii_tx_mess+1, x->hardwareID); 
		outlet_anything(x->outlet_right, s_connect, 2, wii_tx_mess); 


		if (CWIID_POLL_MODE)
		{
			// This mode sets up polling from Pd
			
			// CWIID_FLAG_NONBLOCK causes cwiid_get_mesg to fail instead of
			// block if no messages are ready. 
			cwiid_enable(x->cwiidmote, CWIID_FLAG_NONBLOCK);

			// START: TODO: MOTIONPLUS ADDED
			cwiid_enable(x->cwiidmote, CWIID_FLAG_MOTIONPLUS);
			// END: TODO: MOTIONPLUS ADDED

			// create a Pd clock, providing the polling function, and call it once to start:
			x->x_clock = clock_new(x, (t_method)cwiidmote_poll);
			cwiidmote_poll(x);
		}
		
		else {
			// This mode tells cwiid to call a callback function (defined above)
			if (cwiid_set_mesg_callback(x->cwiidmote, &cwiid_callback)) {
				fprintf(stderr, "Unable to set message callback\n");
			}
		}
		
		

		

	}
} 
 
// The following function attempts to discover a wiimote. It requires 
// that the user puts the wiimote into 'discoverable' mode before being 
// called. This is done by pressing the red button under the battery 
// cover, or by pressing buttons 1 and 2 simultaneously.
 
void cwiidmote_discover(t_cwiidmote *x) 
{ 
	post("Put the cwiidmote into discover mode by pressing buttons 1 and 2 simultaneously.");
		
	cwiidmote_doConnect(x, gensym("NULL")); 
	if (!(x->connected)) 
	{ 
		post("wii Error: could not find any cwiidmotes. Please ensure that bluetooth is enabled, and that the 'hcitool scan' command lists your Nintendo device."); 
	} 
} 
 
void cwiidmote_doDisconnect(t_cwiidmote *x) 
{ 
	
	if (x->connected) 
	{
		clock_unset(x->x_clock);

		int cwiidmoteID = cwiid_get_id(x->cwiidmote);
		
		if (cwiid_close(x->cwiidmote)) { 
			post("cwiidmote error: problems when disconnecting."); 
		} 
		else {
			post("wii   disconnect successfull, resetting values");
			g_cwiidmoteList[cwiidmoteID] = 0; 
			x->connected = 0; 
			SETFLOAT(wii_tx_mess, (t_float) x->connected); 
			SETSYMBOL(wii_tx_mess+1, x->hardwareID); 
			outlet_anything(x->outlet_right, s_connect, 2, wii_tx_mess); 
		} 
	}
	else post("device is not connected");
	 
} 
 
 
// ============================================================== 
// ============================================================== 
 
static void *cwiidmote_new(t_symbol *s, int argc, t_atom *argv) 
{ 
	t_symbol *bdaddr_sym; // wiimote bdaddr 
	t_cwiidmote *x = (t_cwiidmote *)pd_new(cwiidmote_class); 
	
	x->hardwareID = gensym("");

	/*		ARGS NO LONGER USED  --  hardware ID is provided dynamically via mesage
 if (argc <1 || argc >2) 
	{
		error("cwiidmote: bad arg count");
		return(NULL);
	}
	if (argv->a_type != A_FLOAT) 
	{
		error("cwiidmote: first argument must be wii user assigned id number");
		return(NULL);
	}
	else x->wiiUid= atom_getfloat(argv);

	if (x->wiiUid <1 || x->wiiUid >MAX_WII_COUNT) post("warning: cwiidmote: argument %d is out of normal range", (int)x->wiiUid );
	argv++;
	
	
	if (argc > 1) 
	{
		if (argv->a_type != A_SYMBOL)
		{
			error("cwiidmote: second (optional) argument for wii hardware ID must be a symbol");
			return(NULL);
		}
		else x->hardwareID = atom_getsymbol(argv);			
	}
	
	 */

	//post("f=%f, sheefa=%s",f, sheefa->s_name);
	// create outlets: 
	x->outlet_left = outlet_new(&x->x_obj, &s_list); 
	x->outlet_right = outlet_new(&x->x_obj, &s_list); 

 
	// initialize toggles: 
	x->toggle_acc = 0; 
	x->toggle_ir = 0; 
	x->toggle_nc = 0; 
	// START: TODO: MOTIONPLUS ADDED 
	x->toggle_mp = 0;
	// END: TODO: MOTIONPLUS ADDED 
	x->buttonstate=0;
	x->connected = 0; 
	
	x->nc_button_Z = 0;
	x->nc_button_C = 0;
	
	return(x);
} 
 
 
static void cwiidmote_free(t_cwiidmote* x) 
{ 
	cwiidmote_doDisconnect(x); 
} 
 
void cwiidmote_setup(void) 
{
	post("[cwiidmote] external, v.0.6.00");
	
	int i; 
	for (i=0; i<=MAX_cwiidmote_ID; i++) g_cwiidmoteList[i] = 0; 
	
	cwiidmote_class = class_new(gensym("cwiidmote"), (t_newmethod)cwiidmote_new, (t_method)cwiidmote_free, sizeof(t_cwiidmote), CLASS_DEFAULT, A_GIMME,0); 
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_debug, gensym("debug"), 0); 
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_doConnect, gensym("connect"), A_SYMBOL, 0); 
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_doDisconnect, gensym("disconnect"), 0); 
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_discover, gensym("discover"), 0); 
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_setReportMode, gensym("setReportMode"), A_DEFFLOAT, 0); 
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_reportAcceleration, gensym("reportAcceleration"), A_DEFFLOAT, 0); 
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_reportNunchuck, gensym("reportNunchuck"), A_DEFFLOAT, 0); 
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_reportIR, gensym("reportIR"), A_DEFFLOAT, 0); 
	// START: TODO: MOTIONPLUS ADDED
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_reportMotionPlus, gensym("reportMotionPlus"), A_DEFFLOAT, 0); 
	// END: TODO: MOTIONPLUS ADDED
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_setRumble, gensym("setRumble"), A_DEFFLOAT, 0); 
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_setLED, gensym("setLED"), A_DEFFLOAT, 0);
	class_addmethod(cwiidmote_class, (t_method) cwiidmote_beep, gensym("beep"), 0);
 	class_sethelpsymbol(cwiidmote_class, gensym("help-cwiidmote"));
		
	// setup class static vars
	
	s_accel = gensym("accel");
	s_button = gensym("button");
	s_ir = gensym("ir");
	s_pry = gensym("pry");
	// START: TODO: MOTIONPLUS ADDED 
	s_ptp = gensym("ptp");
	// END: TODO:MOTIONPLUS ADDED
	s_xys = gensym("xys");
	s_xyz = gensym("xyz");
	s_xy = gensym("xy");
	s_1 = gensym("1");
	s_2 = gensym("2");
	s_A = gensym("A");
	s_B = gensym("B");
	s_Down = gensym("Down");
	s_Home = gensym("Home");
	s_Left = gensym("Left");
	s_Minus = gensym("Minus");
	s_Plus = gensym("Plus");
	s_Right = gensym("Right");
	s_Up = gensym("Up");
	s_connect = gensym("connect");
	s_nunchuck = gensym("nunchuck");
	// START: TODO: MOTIONPLUS ADDED
	s_motionplus = gensym("motionplus");
	// END: TODO: MOTIONPLUS ADDED
	s_joy = gensym("joy");
	s_C = gensym("C");
	s_Z = gensym("Z");
	
} 
 
 
