pdsheefa library sources
Current version  0.4  (April 2011)


The pdsheefa library provides a mix of abstractions and externs to provide for pd-based SPIN (http://spinframework.org/) client development.  

Included are objects for scene management ("show control") and audio rendering, among other things.

This library consolidates patches and externals taken from other (now frozen) repositories,
such as "audioscsape", spinwidgets, and pdsheefa-0.3.6.


DEPENDENCIES: 

The pdsheefa library automatically includes an external repository called "xjimmies" (https://code.sat.qc.ca/nslam/trunk/xjimmies), upon which, certain abstractions in pdsheefa depend for audio processing.




All externs and abstractions will be installed to:

for OSX: 
	~/Library/Pd/pdsheefa
	~/Library/Pd/xjimmies
	
for Linux:
	/usr/local/lib/pd-externals/pdsheefa
	~/pd-externals/xjimmies

note: you will need to add the above two directories to your pd search path, according to your OS

--z.settel 2011

--------------------------------
===================================================================
Wiimote external for Puredata under Linux (tested with Kernel 2.6.22-14)
Written by Mike Wozniewki (Feb 2007), www.mikewoz.com
Updated by Florian Krebs (Apr 2008)
Updated by Mike Wozniewski (Sep 2008)

Requires the CWiid library (version 0.6.00) by L. Donnie Smith
- This is available at http://www.abstrakraft.org/
- Documentation is available at http://www.wiili.org/index.php/CWiid

To build, edit Makefile and provide PD_PATH and CWIID_PATH (possibly not necessary if already installed via Ubuntu packages), then:

$ make
$ make install

You must have the appropriate bluetooth packages installed and working on your system in order to connect to the Wiimote.

To test this, type the following in a terminal:
> hcitool scan | grep Nintendo

you should get a response that looks something like:
  00:19:1D:70:CE:72    Nintendo RVL-CNT-01


