/* common.h -- those things we define often */

#define InRange(v,lo,hi) ((v)<=(hi)&&(v)>=(lo))
#define MAXIUM(a,b) ((a)>(b)?(a):(b))
#define MINIUM(a,b) ((a)<(b)?(a):(b))



#ifdef DEBUG
#define DBPOST post
#define DBCONSOLE printf
#else
#define DBPOST  if (1) 1; else post
#define DBCONSOLE if (1) 1; printf
#endif


#define RetKey 13
#define EnterKey 3
#define SpaceBar 32
#define BackSpace 8
#define BackSlash 0x5C
#define VertBar 0x7C
#define Grave 0x60
#define Tilde 0x7E
#define TabKey 9
#define ClearKey 27
#define OptionSpace 202
