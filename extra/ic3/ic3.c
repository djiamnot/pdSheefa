/* code for "obj1" pd class.  This takes two messages: floating-point
numbers, and "rats", and just prints something out for each message. */

#include <stdio.h> 
#include <stdlib.h> 
#include <memory.h>
#include <string.h>


#include "isense.h"

#define ESC 0X1B


#define DEBUG 1

#include "m_pd.h"
#include "common.h"


//==========================================================================================

// Library file name

#if defined(_WIN32) || defined(WIN32) || defined(__WIN32__)
#define ISD_LIB_NAME "isense"
#elif defined MACOSX
#include "dlcompat.h"
#define ISD_LIB_NAME "libisense"
#elif defined HP || defined HPUX
#include <dl.h>
#define ISD_LIB_NAME "libisense"
#else
#include <dlfcn.h>
#define ISD_LIB_NAME "libisense"
#endif

typedef void DLL;

static DLL *hLib = NULL;



static void dll_unload( DLL *dll )
{
#if defined(_WIN32) || defined(WIN32) || defined(__WIN32__)

    HINSTANCE hInst = (HINSTANCE) dll;
    FreeLibrary( hInst );

#else // UNIX

#if defined(LINUX) || defined(MACOSX)
    void *handle = (void *) dll;
    dlclose( handle );
#elif defined(HP) || defined(HPUX)
    shl_t handle = (shl_t) dll;
    shl_unload( handle );
#else
    dll = dll; // Suppress warning
#endif
#endif
}

static DLL *dll_load( const char *name )
{
#if defined(_WIN32) || defined(WIN32) || defined(__WIN32__)

    return (DLL *) LoadLibrary( name );

#else // UNIX
    char dllname[512];
    strcpy(dllname, name);

#if defined MACOSX
    strcat(dllname, ".dylib");
   printf("LIBNAME=%s\n", dllname);
    return (DLL *) dlopen(dllname, RTLD_NOW);

#elif defined LINUX || defined SUN
    strcat( dllname, ".so" );
    return (DLL *) dlopen(dllname, RTLD_NOW);

#elif defined HP || defined HPUX
    strcat(dllname, ".sl");
    return shl_load(dllname, BIND_DEFERRED|DYNAMIC_PATH, 0L);
#else
    /* This type of UNIX has no DLL support yet */
    name=name; /* Suppress warning */
    return (DLL *) NULL;
#endif
#endif
}

//==========================================================================================

// GLOBALS

#define MAXPORTS 64+1	// arbitrary maximum assignable port number 
int ic3_port[MAXPORTS];	//  port status array




    /* the data structure for each copy of "obj1".  In this case we
    on;y need pd's obligatory header (of type t_object). */
typedef struct obj1
{
    t_object x_ob;
    t_outlet *x_outyaw;
    t_outlet *x_outpitch;
    t_outlet *x_outroll;
    t_outlet *x_outinfo;
    float x_pitch;
    float x_yaw;
    float x_roll;
    int x_stationID;
    int x_portID;
    // tracker specific
    ISD_TRACKER_HANDLE       x_handle;
    ISD_STATION_INFO_TYPE    x_Station[ISD_MAX_STATIONS];
} t_ic3;


// class vars
t_class *ic3_class;
char ic3_errstr1[] = "ic3_tracker_error";
t_symbol *ic3_stationsym, *ic3_timesym, *ic3_cubesym, *ic3_enhancesym, *ic3_sensitvsym, *ic3_compassym, *ic3_predictsym, *ic3_portsym, *ic3_modelsym, *ic3_typesym, *ic3_statesym;

static t_canvas *ic3_canvas;



// class function Prototypes
void ic3_dumpTrackerStatus(t_ic3 *x);


//-------------class
Bool init_ic3(t_ic3 *x);
Bool read_ic3(t_ic3 *x);
Bool close_ic3(t_ic3 *x);


void ic3_bang(t_ic3 *x)
{

    if (x->x_handle < 1)
    {
        t_atom argv[1];
        
        // DBPOST("%s: ic3 not online",ic3_errstr1);
        SETFLOAT(argv, 0);
        outlet_anything(x->x_outinfo, ic3_statesym, 1, argv);
    }
    else
    {
        if (read_ic3(x) == TRUE)
        {    //post("Yaw: %f   Pitch: %f   Roll: %f", x->x_yaw, x->x_pitch, x->x_roll);
            outlet_float(x->x_outroll, x->x_roll);
            outlet_float(x->x_outpitch, x->x_pitch);
            outlet_float(x->x_outyaw, x->x_yaw);
        }
    }
}

void ic3_reset(t_ic3 *x)
{
    if (ic3_port[x->x_portID] == TRUE) close_ic3(x);

    init_ic3(x);
    ic3_dumpTrackerStatus( x );
}


/* this is called back when obj1 gets a "float" message (i.e., a
    number.) */
void ic3_float(t_ic3 *x, t_floatarg f)
{
    post("obj1: %f", f);
}

    /* this is called when obj1 gets the message, "rats". */
void ic3_info(t_ic3 *x)
{

    ic3_dumpTrackerStatus( x );
}



    /* this is called when a new "obj1" object is created. */
void *ic3_new(t_float portID, t_float stationID)
{
    t_ic3 *x = (void *)0;

    if (stationID == 0) stationID++;		// currently not used
     
    
    if (!InRange(stationID,1, ISD_MAX_STATIONS))
    {
        post("%s:stationID (%d) out of range", ic3_errstr1, (int)stationID);
       goto bail;
    }
    if (!InRange(portID,1, MAXPORTS-1))
    {
        post("%s:portId (%d) out of range", ic3_errstr1, (int)portID);
       goto bail;
    }
    
    
    x = (t_ic3 *)pd_new(ic3_class);
    x->x_stationID = (int) stationID;
    x->x_portID = (int) portID;
    x->x_pitch =  x->x_yaw =  x->x_roll = 0;

    x->x_outyaw = outlet_new(&x->x_ob, gensym("float"));
    x->x_outpitch = outlet_new(&x->x_ob, gensym("float"));
    x->x_outroll = outlet_new(&x->x_ob, gensym("float"));
    x->x_outinfo = outlet_new(&x->x_ob, gensym("anything"));




//  ic3 init

    if (ic3_port[x->x_portID])
    {
        post("%s:portId other ic3 on port (%d) already online", ic3_errstr1, (int)portID);
        goto bail;
    }

    if (init_ic3(x))  // call this last in the new method
        post("ic3: unit ready");
    else
        post("ic3: warning: unit not found");
bail:
    return (void *)x;
}

void ic3_free(t_ic3 *x)
{

    int  result = close_ic3(x);

     post("close = %d", result);
}


void ic3_set(t_ic3 *x, t_symbol *keysym, t_float val)
{
    char key;
    ISD_STATION_INFO_TYPE    Station[ISD_MAX_STATIONS];
    WORD station = 1;
    DWORD maxStations = 4;
    Bool vrbose = TRUE;
    
    key = keysym->s_name[0];

    if( x->x_handle < 1 )
    {
        post("%s: ic3 not online",ic3_errstr1);
        return;
    }

    printf( "ic3: setting infomation \n");
    // Clear station configuration info to make sure GetAnalogData and other flags are FALSE 
    memset((void *) Station, 0, sizeof(Station));

   //  post("testing key %c", key);
    
    switch(key)
    {
        case 'A':
        {
            BYTE AuxOutput[4];

            AuxOutput[0] = 255;
            AuxOutput[1] = 255;
            ISD_AuxOutput( x->x_handle, 1, AuxOutput, 4 );
        }
            break;

        case 'a':
        {
            BYTE AuxOutput[4];

            AuxOutput[0] = 0;
            AuxOutput[1] = 0;
            ISD_AuxOutput( x->x_handle, 1, AuxOutput, 4 );
        }
            break;

        case 'b':
            ISD_Boresight( x->x_handle, 1, FALSE );
            break;

        case 'B':
            ISD_Boresight( x->x_handle, 1, TRUE );
            break;

        case 'e': // IS-x products only, not for InterTrax 
        case 'E':

            // First get current station configuration 
            if( ISD_GetStationConfig( x->x_handle,
                                      &Station[station-1], station, vrbose ) )
            {
                if( Station[station-1].Enhancement++ == 3 )
                    Station[station-1].Enhancement = 1;

                
                // Send the new configuration to the tracker 
                if( ISD_SetStationConfig( x->x_handle, 
                                          &Station[station-1], station, vrbose ) )
                {
                    // display the results 
                    ic3_dumpTrackerStatus( x );
                }
            }
            break;

        case 'p': 
        case 'P': // IS-x products only, not for InterTrax 

            if( ISD_GetStationConfig( x->x_handle, 
                                      &Station[station-1], station, vrbose ))
            {
                Station[station-1].Prediction += 10;
                if( Station[station-1].Prediction > 50 ) 
                {
                    Station[station-1].Prediction = 0;
                }

                if( ISD_SetStationConfig( x->x_handle,
                                          &Station[station-1], station, vrbose ) )
                {
                    ic3_dumpTrackerStatus( x );
                }
            }
            break;

        case 'R':
            ISD_ResetHeading( x->x_handle, 1 );
            break;

        case 's': // IS-x products only, not for InterTrax 
        case 'S':

            // First get current station configuration 
            if( ISD_GetStationConfig( x->x_handle,
                                      &Station[station-1], station, vrbose ) )
            {
                if( Station[station-1].Sensitivity == 4 )
                    Station[station-1].Sensitivity = 1;
                else Station[station-1].Sensitivity++;


                // Send the new configuration to the tracker 
                if( ISD_SetStationConfig( x->x_handle, 
                                          &Station[station-1], station, vrbose ) )
                {
                    // display the results 
                    ic3_dumpTrackerStatus( x );
                }
            }
            break;
                
        case 't': 
        case 'T': // IS-x products only, not for InterTrax 

            if( ISD_GetStationConfig( x->x_handle, &Station[station-1], 
                                      station, vrbose ) )
            {
                Station[station-1].TimeStamped = !Station[station-1].TimeStamped;

                if( ISD_SetStationConfig( x->x_handle, 
                                          &Station[station-1], station, vrbose ) )
                {
                    ic3_dumpTrackerStatus( x );
                }
            }
            break;

        case '1':

            station = 1;
            printf("Current Station is set to %d \n", station);
            break;

        case '2': // IS-x products only, not for InterTrax 

            if( maxStations > 1 ) 
            {
                station = 2;
                printf("Current Station is set to %d \n", station);
            }
            break;

        case '3': // IS-x products only, not for InterTrax 

            if( maxStations > 2 ) 
            {
                station = 3;
                printf("Current Station is set to %d \n", station);
            }
            break;

        case '4': // IS-x products only, not for InterTrax 

            if( maxStations > 3 ) 
            {
                station = 4;
                printf("Current Station is set to %d \n", station);
            }
            break;

        case 'O':
            if( ISD_GetStationConfig( x->x_handle, &Station[station-1], 
                                      station, vrbose ) )
            {
                Station[station-1].TipOffset[0] += 0.1f;
                Station[station-1].TipOffset[0] += 0.2f;
                Station[station-1].TipOffset[0] += 0.3f;

                if( ISD_SetStationConfig( x->x_handle, &Station[station-1],
                                          station, vrbose ) )
                {
                    ic3_dumpTrackerStatus( x );
                }
            }
            break;

/*      case ESC:
        case 'q':
        case 'Q':
            done = TRUE;
            break;
*/
        case 'C':
        case 'c':
            if( ISD_GetStationConfig( x->x_handle, 
                                      &Station[station-1], station, vrbose ) )
            {
                if( Station[station-1].Compass == 2 ) 
                    Station[station-1].Compass = 0;
                else
                    Station[station-1].Compass = 2;

                if( ISD_SetStationConfig( x->x_handle,
                                          &Station[station-1], station, vrbose ) )
                {
                    ic3_dumpTrackerStatus( x );
                }
                else
                {
                    printf( "ISD_SetStationConfig failed\n" );
                }
            }
            else
            {
                printf("ISD_GetStationConfig failed\n" );
            }
            break;

        default:
            printf( "q -- quit\n");
            printf( "1 -- make station 1 current\n" );
            printf( "2 -- make station 2 current\n" );
            printf( "3 -- make station 3 current\n" );
            printf( "4 -- make station 4 current\n" );
            printf( "E -- change enhancement mode\n" );
            printf( "P -- change predition\n" );
            printf( "R -- to reset heading\n" );
            printf( "T -- Turn time stamps ON/OFF\n" );
            break;
    }


}

/* this is called once at setup time, when this code is loaded into Pd. */
void ic3_setup(void)
{
    char dllname[512];
    
    int i = MAXPORTS-1;

    // post("ic3_setup");
    ic3_class = class_new(gensym("ic3"), (t_newmethod)ic3_new, (t_method) ic3_free,
                          sizeof(t_ic3), 0, A_FLOAT,A_DEFFLOAT,0); // portID , <opt> stationID
    class_addmethod(ic3_class, (t_method)ic3_info, gensym("info"), 0);
    class_addmethod(ic3_class, (t_method)ic3_reset, gensym("reset"), 0);
    class_addmethod(ic3_class, (t_method)ic3_set, gensym("set"), A_SYMBOL,A_DEFFLOAT,0);
    class_addbang(ic3_class,ic3_bang);
    class_addfloat(ic3_class, ic3_float);
    // generate syms for messaging
    ic3_stationsym = gensym("station");
    ic3_timesym = gensym("time");
    ic3_cubesym = gensym("cube");
    ic3_enhancesym = gensym("enhancement");
    ic3_sensitvsym = gensym("sensitivity");
    ic3_compassym = gensym("compass");
    ic3_predictsym = gensym("prediction");
    ic3_portsym = gensym("port");
    ic3_modelsym = gensym("model");
    ic3_typesym = gensym("type");
    ic3_statesym = gensym("state");
    
    while (i >= 0)
        ic3_port[i--] = FALSE;
    
    ic3_canvas = canvas_getcurrent();

    //strcpy(dllname, canvas_getdir(ic3_canvas)->s_name);
    //strcat(dllname, "/");
    //strcat(dllname, ISD_LIB_NAME);
    strcpy(dllname, ISD_LIB_NAME);
    // post("canvas dir:  %s", dllname);
   
    hLib = dll_load( dllname );   // load dynamic lib now using file path of calling patcher

	 // the following error is innacurate. you can't know this from hLib!
    //if (!hLib) post("ic3 warning: could not load lib", dllname);
    //else  dll_unload( hLib );  // free the dll

}

// ______________________________
/*****************************************************************************/
const char *systemType( int Type ) 
{
    switch( Type ) 
    {
    case ISD_NONE:
        return "Unknown";
    case ISD_PRECISION_SERIES:
        return "IS Precision Series";
    case ISD_INTERTRAX_SERIES:
        return "InterTrax Series";
    }
    return "Unknown";
}


/*****************************************************************************/
const char *systemName( int Model ) 
{
    switch( Model ) 
    {
    case ISD_IS300:
        return "IS-300 Series";
    case ISD_IS600:
        return "IS-600 Series";
    case ISD_IS900:
        return "IS-900 Series";
    case ISD_INTERTRAX:
        return "InterTrax 30";
    case ISD_INTERTRAX_2:
        return "InterTrax2";
    case ISD_INTERTRAX_LS:
        return "InterTraxLS";
    case ISD_INTERTRAX_LC:
        return "InterTraxLC";
    case ISD_ICUBE2:
        return "InertiaCube2";
    case ISD_ICUBE2_PRO:
        return "InertiaCube2 Pro";
    case ISD_ICUBE3:
        return "InertiaCube3";
    case ISD_IS1200:
        return "IS-1200 Series";
    }
    return "Unknown";
}


void ic3_dumpTrackerStatus( t_ic3 *x )
{
    ISD_TRACKER_INFO_TYPE Tracker;
    ISD_STATION_INFO_TYPE Station;
    WORD i, numStations = 4;
    char buf[20];

    t_atom argv[1];

    if (x->x_handle < 1)
    {
        SETFLOAT(argv, 0);
        outlet_anything(x->x_outinfo, ic3_statesym, 1, argv);

        SETSYMBOL(argv, gensym("offline"));
        outlet_anything(x->x_outinfo, ic3_typesym, 1, argv);

        DBPOST("%s: ic3 not online",ic3_errstr1);
        return;
    }

    if(ISD_GetTrackerConfig( x->x_handle, &Tracker, TRUE ))
    {
        DBCONSOLE("\n\n********** InterSense Tracker Information ***********\n\n");

        DBCONSOLE("Type:     %s device on port %ld\n", systemType(Tracker.TrackerType), Tracker.Port);

        DBCONSOLE("Model:    %s\n", systemName(Tracker.TrackerModel));

        switch( Tracker.TrackerModel ) 
        {
        case ISD_IS300:
        case ISD_IS1200:
            numStations = 4;
            break;
        case ISD_IS600:
        case ISD_IS900:
            numStations = ISD_MAX_STATIONS;
            break;
        default:
            numStations = 1;
            break;
        }
        
        DBCONSOLE("\nStation\tTime\tState\tCube  Enhancement  Sensitivity  Compass  Prediction\n");
        
        for(i = 1; i <= numStations; i++)
        {
            DBCONSOLE("%d\t", i);
            
            if(ISD_GetStationConfig( x->x_handle, &Station, i, FALSE ))
            {
                sprintf(buf, "%ld", Station.InertiaCube);
                
                DBCONSOLE("%s\t%s\t%s\t   %lu\t\t%lu\t  %u\t  %lu\n", 
                    Station.TimeStamped ? "ON" : "OFF", 
                    Station.State ? "ON" : "OFF", 
                    Station.InertiaCube == -1 ? "None" : buf, 
                    Station.Enhancement, 
                    Station.Sensitivity, 
                    (int)Station.Compass,
                    Station.Prediction);
            }
            else
            {
                DBCONSOLE("ISD_GetStationConfig failed\n");
                break;
            }
        }
        DBCONSOLE("\n");

        ISD_GetCommInfo( x->x_handle, &Tracker );

        printf( "%5.2fKbps %ld Records/s \r",
                Tracker.KBitsPerSec,Tracker.RecordsPerSec );
        
        
        SETSYMBOL(argv, gensym((char *)systemType(Tracker.TrackerType)));
        outlet_anything(x->x_outinfo, ic3_typesym, 1, argv);

        SETSYMBOL(argv, gensym((char *)systemName(Tracker.TrackerModel)));
        outlet_anything(x->x_outinfo, ic3_modelsym, 1, argv);

        SETFLOAT(argv, ((float) Tracker.Port));
        outlet_anything(x->x_outinfo, ic3_portsym, 1, argv);

        SETFLOAT(argv, ((float)Station.TimeStamped));
        outlet_anything(x->x_outinfo, ic3_timesym, 1, argv);

        SETFLOAT(argv, ((float)Station.State));
        outlet_anything(x->x_outinfo, ic3_statesym, 1, argv);

        SETSYMBOL(argv, gensym(Station.InertiaCube == -1 ? "None" : buf));
        outlet_anything(x->x_outinfo, ic3_cubesym, 1, argv);

        SETFLOAT(argv, ((float)Station.Enhancement));
        outlet_anything(x->x_outinfo, ic3_enhancesym, 1, argv);

        SETFLOAT(argv, ((float)Station.Sensitivity));
        outlet_anything(x->x_outinfo, ic3_sensitvsym, 1, argv);

        SETFLOAT(argv, ((float)Station.Compass));
        outlet_anything(x->x_outinfo, ic3_compassym, 1, argv);

        SETFLOAT(argv, ((float)Station.Prediction));
        outlet_anything(x->x_outinfo, ic3_predictsym, 1, argv);
    }
    return;
}




#define CAMERA_TRACKER  0
#define GET_AUX_INPUTS  0

/*****************************************************************************
*
*   functionName:   main
*   Description:    a sample main
*   Created:        12/7/98
*   Author:         Yury Altshuler
*
*   Comments:       This simple main shows how to initialize and get data from
*                   two InterSense trackers. Most of the code is only relevant 
*                   when used with an IS-x series device and can be removed if
*                   using with InterTrax. 
*
******************************************************************************/


Bool init_ic3(t_ic3 *x)   // returns TRUE or FALSE (can't open)
{

    ISD_HARDWARE_INFO_TYPE   hwInfo;
    ISD_TRACKER_INFO_TYPE    Tracker;
    WORD station = 1;
    DWORD maxStations = 4;
    Bool vrbose = TRUE;

    // Detect first tracker. If you have more than one InterSense device and
    // would like to have a specific tracker, connected to a known port, 
    // initialized first, then enter the port number instead of 0. Otherwise, 
    // tracker connected to the rs232 port with lower number is found first 

    printf("ic3_init: attempting to initialize tracker...\n");
    
    x->x_handle = ISD_OpenTracker( (Hwnd) NULL, x->x_portID, FALSE, vrbose );
    
    // Check value of handle to see if tracker was located 
    if( x->x_handle < 1 )
    {
        printf("%s: Failed to detect InterSense tracking device\n",ic3_errstr1);  
        return(FALSE);
    }
    else
    {
        // Get tracker configuration info 
        ISD_GetTrackerConfig( x->x_handle, &Tracker, vrbose );
                
        memset((void *) &hwInfo, 0, sizeof(hwInfo));

        if( ISD_GetSystemHardwareInfo( x->x_handle, &hwInfo ) )
        {
            if( hwInfo.Valid )
            {
                maxStations = hwInfo.Capability.MaxStations;
            }
        }

        // Clear station configuration info to make sure GetAnalogData and other flags are FALSE 
        memset((void *) x->x_Station, 0, sizeof(x->x_Station));

        // General procedure for changing any setting is to first retrieve current 
        // configuration, make the change, and then apply it. Calling 
        // ISD_GetStationConfig is important because you only want to change 
        // some of the settings, leaving the rest unchanged. 
        
        if( Tracker.TrackerType == ISD_PRECISION_SERIES )
        {
            for( station = 1; station <= maxStations; station++ )
            {         
                // fill ISD_STATION_INFO_TYPE structure with current station configuration 
                if( !ISD_GetStationConfig( x->x_handle, 
                    &x->x_Station[station-1], station, vrbose )) break;
                
                if( GET_AUX_INPUTS && Tracker.TrackerModel == ISD_IS900 )
                {
                    x->x_Station[station-1].GetAuxInputs = TRUE;
                    
                    // apply new configuration 
                    if( !ISD_SetStationConfig( x->x_handle,
                        &x->x_Station[station-1], station, vrbose )) break;
                }

                if( CAMERA_TRACKER && Tracker.TrackerModel == ISD_IS900 )
                {
                    x->x_Station[station-1].GetCameraData = TRUE;
                    
                    // apply new configuration 
                    if( !ISD_SetStationConfig( x->x_handle,
                        &x->x_Station[station-1], station, vrbose )) break;
                }
            }
        }
    }
    ic3_port[x->x_portID] = TRUE;
    return(TRUE);
}


Bool  read_ic3(t_ic3 *x)	// returns TRUE if new data
{

    ISD_TRACKING_DATA_TYPE    data;
    Bool rval = FALSE;
   //  int station = 1;
   //  float lastTime = ISD_GetTime();


    // must be called at a reasonable rate
    if( x->x_handle > 0 )
    {
        ISD_GetTrackingData( x->x_handle, &data );    
        // ISD_GetCameraData( x->x_handle, &cameraData );

		
		if (x->x_yaw != data.Station[0].Euler[0])
		 x->x_yaw = data.Station[0].Euler[0], rval = TRUE;
		 
		 if (x->x_pitch != data.Station[0].Euler[1])
		 x->x_pitch = data.Station[0].Euler[1], rval = TRUE;
		 
		 if (x->x_roll != data.Station[0].Euler[2])
		 x->x_roll = data.Station[0].Euler[2], rval = TRUE;

		 /*        if (x->x_yaw != data.Station[0].Orientation[0])
            x->x_yaw = data.Station[0].Orientation[0], rval = TRUE;

        if (x->x_pitch != data.Station[0].Orientation[1])
            x->x_pitch = data.Station[0].Orientation[1], rval = TRUE;
            
        if (x->x_roll != data.Station[0].Orientation[2])
            x->x_roll = data.Station[0].Orientation[2], rval = TRUE;
*/
    }
    return rval;
}
    
Bool close_ic3(t_ic3 *x)
{
    Bool result = FALSE;

    if ( x->x_handle < 1)
    {
        DBCONSOLE("ic3 unit not online, can't close");
        return(result);
    }
    
    if (result=ISD_CloseTracker( x->x_handle ))
    {
        ic3_port[x->x_portID] = FALSE;
        DBPOST("ic3 closed");
        DBCONSOLE("ic3 closed\n");
    }

    return result; 
}



