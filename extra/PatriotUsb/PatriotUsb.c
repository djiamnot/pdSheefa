// *****************************************************************************
// PatriotUsb.c
//
// Pure Data external to get data from a Polhemus Patriot or Liberty (usb)
// written by Mike Wozniewski (mike@mikewoz.com)
// 
// (Based on LinuxTerm.c provided by Polhemus Inc.)
//
// *****************************************************************************

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <termios.h>

#include <pthread.h>

#include "m_pd.h"

#define POLHEMUS_MAX_DATA_SIZE 1000 // may need to be larger if getting alot of data

enum{PASS,FAIL};

static t_class *PatriotUsb_class;
 
static pthread_mutex_t pthreadLock = PTHREAD_MUTEX_INITIALIZER;

typedef struct _PatriotUsb {

	t_object  x_obj; //obj needed by pd   	

	t_symbol *deviceAddress;
	t_symbol *trackerType;
	
	int rdPort,wrPort;
	
	struct termios attributes;

	// pthread stuff
    pthread_t pthreadID; // id of child thread
    pthread_attr_t pthreadAttr; 
	
	t_outlet *xyz_1_outlet;
	t_outlet *pyr_1_outlet;
	t_outlet *xyz_2_outlet;
	t_outlet *pyr_2_outlet;
	
	int numAtoms;
	t_atom *atomList;

} t_PatriotUsb;


void PatriotUsb_bang(t_PatriotUsb *x)
{
	// return if not connected:
	if ((x->rdPort==-1) || (x->wrPort==-1))  return;
	
	// NOT THREADED:
	/*
	int br, count;
	char buf[POLHEMUS_MAX_DATA_SIZE];
	t_binbuf *b;

	memset(buf,0,POLHEMUS_MAX_DATA_SIZE);
	
	// request data:
	count=0;
	write(x->wrPort,"p",1);			
	do {
		br=read(x->rdPort,buf,POLHEMUS_MAX_DATA_SIZE);
		usleep(500);  // wait 0.5 ms
		if (count++ > 10) return; // don't wait longer than 5ms in total
	} while (br<=0);
	//} while ((br<=0) && (count++<100)); // try to read 10 times

	// terminate:
	buf[br]='\0';	
	
	
	// convert string into list of Pd atoms (using binbuf):
	b = binbuf_new();
	binbuf_text(b, buf, br);
	x->numAtoms = binbuf_getnatom(b);
	x->atomList = (t_atom *) copybytes(binbuf_getvec(b), sizeof(*binbuf_getvec(b)) * x->numAtoms);
	binbuf_free(b);

	*/
	// END NOT THREADED
	

	//post("got %d atoms:", x->numAtoms);
	//postatom(x->numAtoms, x->atomList);
	
	pthread_mutex_lock(&pthreadLock);

		// must deal with variable formats of output:

		if (x->numAtoms == 8) // sensor 1, with first atom being the id
		{
			outlet_list(x->pyr_1_outlet, gensym("list"), 3, x->atomList+4);
			outlet_list(x->xyz_1_outlet, gensym("list"), 3, x->atomList+1);
		}
		else if (x->numAtoms == 6) // sensor 2, without id atom
		{
			outlet_list(x->pyr_2_outlet, gensym("list"), 3, x->atomList+3);
			outlet_list(x->xyz_2_outlet, gensym("list"), 3, x->atomList+0);		
		}
		else if (x->numAtoms == 14) // both sensors
		{
			outlet_list(x->pyr_1_outlet, gensym("list"), 3, x->atomList+4);
			outlet_list(x->xyz_1_outlet, gensym("list"), 3, x->atomList+1);
			outlet_list(x->pyr_2_outlet, gensym("list"), 3, x->atomList+11);
			outlet_list(x->xyz_2_outlet, gensym("list"), 3, x->atomList+8);					
		}
	
	pthread_mutex_unlock(&pthreadLock);
	
	
}

void PatriotUsb_info(t_PatriotUsb *x)
{
	char cmd[10];
	char buf[400];
	int br;
	
	post("");
	post("***************************************");
	post("       [PatriotUsb] INFO:");
	post("");
	
	if ((x->rdPort==-1) || (x->wrPort==-1))
	{
		post(" NOT CONNECTED");
	
	} else {

		memset(buf,0,400);
		sprintf(cmd,"%c\r",22);
		write(x->wrPort,cmd,strlen(cmd));
		usleep(100000);	// wait 100 ms
		br=read(x->rdPort,buf,400);
		
		if (br>0) post(buf);
		else post("Error obtaining tracker information\n");
	}
	
	post("***************************************");
	
}

static void *PatriotUsb_poll(void *arg)
{
	int i, br, count;
	char buf[POLHEMUS_MAX_DATA_SIZE];
	t_binbuf *b;

	t_PatriotUsb *x = (t_PatriotUsb *)arg;
	
	while (1)
	{
		if ((x->rdPort==-1) || (x->wrPort==-1))
		{
			//tcsetattr(x->rdPort,TCSANOW,&(x->attributes)); // restore the original attributes
			close(x->rdPort);
			close(x->wrPort);
			pthread_exit(NULL);
		}
		
		// clear buffer:
		memset(buf,0,POLHEMUS_MAX_DATA_SIZE);
	
		// request data:
		count=0;
		write(x->wrPort,"p",1);			
		do {
			br=read(x->rdPort,buf,POLHEMUS_MAX_DATA_SIZE);
			usleep(1000);  // wait 1 ms
		} while ((br<=0) && (count++<100)); // try to read 10 times

		// terminate:
		buf[br]='\0';	
		
		// convert string into list of Pd atoms (using binbuf):
		b = binbuf_new();
		binbuf_text(b, buf, br);
		x->numAtoms = binbuf_getnatom(b);
		x->atomList = (t_atom *) copybytes(binbuf_getvec(b), sizeof(*binbuf_getvec(b)) * x->numAtoms);

		
		//post("got %d atoms:", x->numAtoms);
		//postatom(x->numAtoms, x->atomList);
		//post(buf);
		
		binbuf_free(b);
		
	}
	return 0;
}


void PatriotUsb_connect(t_PatriotUsb *x, t_symbol *addr)
{

	int exit=0;			
	char choice[10];
	char buf[POLHEMUS_MAX_DATA_SIZE];
	struct termios attr;
	
	// check if already connected:
	if ((x->rdPort!=-1) || (x->wrPort!=-1)) {
		post("Already connected");
		return;
	}
	
	
	// save address:
	x->deviceAddress = addr;

	// connect:
	x->rdPort=open(x->deviceAddress->s_name,O_RDONLY|O_NDELAY);
	x->wrPort=open(x->deviceAddress->s_name,O_WRONLY);
	
	if ((x->rdPort==-1) || (x->wrPort==-1)) {
		post("Error: could not connect to tracker at address: %s", x->deviceAddress->s_name);
		return;
	}

	// set up terminal for raw data
	tcgetattr(x->rdPort,&(x->attributes)); // save attributes for restore later
	attr=x->attributes;
	cfmakeraw(&attr);
	if (tcsetattr(x->rdPort,TCSANOW,&attr)){
		post("Error setting terminal attributes");
		close(x->rdPort);
		close(x->wrPort);
		return;
	}
	
	
	// create child thread which will poll for data:
	if(pthread_attr_init(&x->pthreadAttr) < 0)
		printf("PatriotUsb: warning: could not prepare child thread\n");
	if(pthread_attr_setdetachstate(&x->pthreadAttr, PTHREAD_CREATE_DETACHED) < 0)
		printf("PatriotUsb: warning: could not prepare child thread\n");
	if(pthread_create( &x->pthreadID, &x->pthreadAttr, PatriotUsb_poll, x) < 0)
		printf("PatriotUsb: could not create new thread\n");
	

	/*
	// determine tracker type
	GetVerInfo(buf);
	if (strstr(buf,"Patriot")) x->trackerType = gensym("Patriot");
	else if(strstr(buf,"Liberty")) x->trackerType = gensym("Liberty");
	else x->trackerType = gensym("Unknown Tracker");

	post("\nConnected to %s at address %s\n", x->trackerType, x->deviceAddress);
	*/
	post("\nConnected to tracker at address %s\n", x->deviceAddress->s_name);

	
}

void PatriotUsb_disconnect(t_PatriotUsb *x)
{
	//tcsetattr(x->rdPort,TCSANOW,&(x->attributes)); // restore the original attributes
	//close(x->rdPort);
	//close(x->wrPort);
	x->rdPort = -1;
	x->wrPort = -1;
}


void PatriotUsb_free(t_PatriotUsb *x)
{
	PatriotUsb_disconnect(x);
}

void *PatriotUsb_new(t_symbol *addr)
{
	t_PatriotUsb *x = (t_PatriotUsb *)pd_new(PatriotUsb_class);

	x->rdPort = -1;
	x->wrPort = -1;
	
	x->xyz_1_outlet = outlet_new(&x->x_obj, &s_list);
	x->pyr_1_outlet = outlet_new(&x->x_obj, &s_list);
	
	x->xyz_2_outlet = outlet_new(&x->x_obj, &s_list);
	x->pyr_2_outlet = outlet_new(&x->x_obj, &s_list);

	return (void *)x;
}

void PatriotUsb_setup(void) {
	
	PatriotUsb_class = class_new(gensym("PatriotUsb"), (t_newmethod)PatriotUsb_new, (t_method)PatriotUsb_free, sizeof(t_PatriotUsb), CLASS_DEFAULT, A_DEFSYMBOL, 0);

	class_addbang   (PatriotUsb_class, PatriotUsb_bang);
	class_addmethod (PatriotUsb_class, (t_method)PatriotUsb_info, gensym("info"), 0);
	class_addmethod (PatriotUsb_class, (t_method)PatriotUsb_connect, gensym("connect"), A_DEFSYMBOL, 0);
	class_addmethod (PatriotUsb_class, (t_method)PatriotUsb_disconnect, gensym("disconnect"), 0);

}
