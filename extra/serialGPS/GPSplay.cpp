

#include "GPSutil.h"

using namespace std;



template <class T> bool fromString(T& t, const std::string& s)
{
        std::istringstream iss(s);
        return !(iss >> t).fail();
}



static t_class *GPSplay_class;


typedef struct _GPSplay {

	t_object x_obj; //obj needed by pd   	

	t_symbol *filename;

	
	// GPX stuff:
	OGRDataSource *ogrDS;
	OGRLayer *ogrLayer;

	double lastTimeVal;

    
    OGRSpatialReference *spatialRef;    


	t_outlet *dataOutlet;
	t_outlet *posnOutlet;
	t_outlet *timeOutlet;
	t_outlet *statOutlet;

	t_atom outData[3];

} t_GPSplay;






// *****************************************************************************
void GPSplay_readLog(t_GPSplay *x, t_symbol *filesym)
{
	string filename;
	
	// replace tilde with $HOME variable
	if ((filesym->s_name)[0]=='~') filename = getenv("HOME") + string(filesym->s_name).substr(1);
	else filename = string(filesym->s_name);
	
	
	// info about the GPX driver: http://www.gdal.org/ogr/drv_gpx.html
	const char *pszDriverName = "GPX";
	

    x->ogrDS = OGRSFDriverRegistrar::Open( filename.c_str(), FALSE );
    if( x->ogrDS == NULL )
    {
        post( "[GPSplay] ERROR: Could not open file %s", filename.c_str() );
        return;
    }
    
    /*
    post("Opened file %s, and found %d layers:", filename.c_str(), x->ogrDS->GetLayerCount());
    for (int i=0; i<x->ogrDS->GetLayerCount(); i++)
    {
    	
    	OGRFeatureDefn *def = x->ogrDS->GetLayer(i)->GetLayerDefn();

    	const char *geomType;
    	if (def->GetGeomType()==wkbPoint) geomType = "wkbPoint";
       	else if (def->GetGeomType()==wkbLineString) geomType = "wkbLineString";
       	else if (def->GetGeomType()==wkbMultiPoint) geomType = "wkbMultiPoint";
       	else if (def->GetGeomType()==wkbMultiLineString) geomType = "wkbMultiLineString";
    	else geomType = "unknown";
 		
    
    	post("  Layer[%d]: %s, of type %s", i, x->ogrDS->GetLayer(i)->GetLayerDefn()->GetName(), geomType);
    }
*/
    
    // TODO: select layer more intelligently:

    //x->ogrLayer = x->ogrDS->GetLayerByName("SerialGPS_waypointLayer" );
    x->ogrLayer = x->ogrDS->GetLayer(0); // layer[0] should have waypoints
    if( x->ogrLayer == NULL )
    {
        post( "[GPSplay] ERROR: Could not find a valid layer in %s", filename.c_str() );
        OGRDataSource::DestroyDataSource( x->ogrDS );
        return;
    }
    
    post("Opened log file: %s", filename.c_str());
}

void GPSplay_outputFeature(t_GPSplay *x, OGRFeature *poFeature)
{
	OGRGeometry *poGeometry;
	OGRPoint *poPoint;
	
	t_atom posnAtoms[3];
	double elev;
	
	t_symbol *dataSelector;
	int dataAtomCount = 0;
	t_atom *dataAtoms;
	
	
	OGRFeatureDefn *def = poFeature->GetDefnRef();

	
	if (def->GetGeomType()==wkbPoint)
	{
		
		// only output if there is a valid lat/long:
		poGeometry = poFeature->GetGeometryRef();
		if( (poGeometry!=NULL) && (wkbFlatten(poGeometry->getGeometryType()) == wkbPoint) )
		{
			
			// get the point:
			poPoint = (OGRPoint *) poGeometry;

			// convert to UTM:			
			double UTM_X, UTM_Y, UTM_Z;
			UTM_X = nmea_degree2radian(poPoint->getX());
			UTM_Y = nmea_degree2radian(poPoint->getY());
			UTM_Z = elev;
			
			if (ConvertToUTM( &UTM_X, &UTM_Y, &UTM_Z ))
			{
				SETFLOAT(posnAtoms+0, UTM_X);
				SETFLOAT(posnAtoms+1, UTM_Y);
				SETFLOAT(posnAtoms+2, UTM_Z);
			} else {
				post("[GPSplay] Error: Could not convert to UTM.");
				return;
			}
			
			// get the elevation: field[0]
			elev = poFeature->GetFieldAsDouble(0);
			
			
			// get the type of data: field[4] 'name'
			dataSelector = gensym(poFeature->GetFieldAsString(4));
			
			// get this time value:field[5] 'cmt'
			/*
			double timeVal;
			if (!fromString<double>(timeVal, string(poFeature->GetFieldAsString(5))))
			{
				timeVal = 0;
			}
			*/
			double timeVal = poFeature->GetFieldAsDouble(5);
			
			
			// get the associated data: field[6] 'desc'
	        string dataList = poFeature->GetFieldAsString(6);
			
			// convert data to atoms:
	        if (dataList.size())
	        {
	        	t_binbuf *b = binbuf_new();
	        	binbuf_text(b, (char*) dataList.c_str(), dataList.size());
	        	dataAtomCount = binbuf_getnatom(b);
	        	dataAtoms = (t_atom *) copybytes(binbuf_getvec(b), sizeof(*binbuf_getvec(b)) * dataAtomCount);
	        	binbuf_free(b);
	        }

	        outlet_float(x->timeOutlet, timeVal);
	        outlet_anything(x->posnOutlet, gensym("UTM"), 3, posnAtoms);
	        outlet_anything(x->dataOutlet, dataSelector, dataAtomCount, dataAtoms);
		}

	}
}


void GPSplay_dump(t_GPSplay *x)
{
	OGRLayer *poLayer;
	OGRFeature *poFeature;

    for (int i=0; i<x->ogrDS->GetLayerCount(); i++)
    //for (int i=0; i<1; i++)
    {
    	poLayer = x->ogrDS->GetLayer(i);
    	poLayer->ResetReading();

    	if (poLayer->GetLayerDefn()->GetGeomType()==wkbPoint)
    	{
			while( (poFeature = poLayer->GetNextFeature()) != NULL )
			{
				GPSplay_outputFeature(x, poFeature);
			}
    	}
    	
    	else if (poLayer->GetLayerDefn()->GetGeomType()==wkbLineString)
    	{
    		post("Warning: no method implemented for playing routes (layer[%d]: wkbLineString)", i);
    	}
    	else if (poLayer->GetLayerDefn()->GetGeomType()==wkbMultiLineString)
    	{
    		post("Warning: no method implemented for playing tracks (layer[%d]: wkbMultiLineString)", i);
    	}   	
    	else
    	{
    		post("ERROR: Found layer %s in the file, of unkown type.", poLayer->GetLayerDefn()->GetName());
    	}
    }
    
    outlet_bang(x->statOutlet);
}


void GPSplay_rewind(t_GPSplay *x)
{
	if (x->ogrLayer != NULL) x->ogrLayer->ResetReading();
}


void GPSplay_bang(t_GPSplay *x)
{
	OGRFeature *poFeature;

	if (x->ogrLayer == NULL)
	{
		post("[GPSplay] Error: No log file loaded.");
		return;
	}
	
	if ((poFeature = x->ogrLayer->GetNextFeature()) == NULL )
	{
		outlet_bang(x->statOutlet);
		return;
	}
	
	GPSplay_outputFeature(x, poFeature);
}






// *****************************************************************************


void *GPSplay_new(t_symbol *f)
{
	t_GPSplay *x = (t_GPSplay *)pd_new(GPSplay_class);

	//post("creating GPSplay object with arg %s", f->s_name);
	
	x->filename = gensym("null");
	
	x->dataOutlet = outlet_new(&x->x_obj, NULL);
	x->posnOutlet = outlet_new(&x->x_obj, NULL);
	x->timeOutlet = outlet_new(&x->x_obj, NULL);
	x->statOutlet = outlet_new(&x->x_obj, NULL);
	
	
	return (void *)x;
			
}

void GPSplay_free(t_GPSplay *x)
{
	if (x->ogrDS != NULL)
	{
		OGRDataSource::DestroyDataSource( x->ogrDS );
	}
	
	x->ogrDS = NULL;

}


// *****************************************************************************
extern "C" void GPSplay_setup(void)
{

	OGRRegisterAll(); // we should load only GPX, but load all drivers for now
	
	/*
	for (int i=0; i<OGRSFDriverRegistrar::GetRegistrar()->GetDriverCount(); i++)
	{
		post("Registered OGR driver for %s", OGRSFDriverRegistrar::GetRegistrar()->GetDriver(i)->GetName());
	}
	*/
	
	
	GPSplay_class = class_new(gensym("GPSplay"), (t_newmethod)GPSplay_new,(t_method)GPSplay_free, sizeof(t_GPSplay),CLASS_DEFAULT, A_DEFSYMBOL, 0);

	class_addbang (GPSplay_class, GPSplay_bang);
	class_addmethod (GPSplay_class, (t_method)GPSplay_readLog, gensym("readLog"), A_DEFSYMBOL, 0);
	class_addmethod (GPSplay_class, (t_method)GPSplay_dump, gensym("dump"), A_DEFFLOAT, 0);
	class_addmethod (GPSplay_class, (t_method)GPSplay_rewind, gensym("rewind"), A_DEFFLOAT, 0);

}
