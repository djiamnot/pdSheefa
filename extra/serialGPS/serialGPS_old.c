#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h> 
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <termios.h> 
#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h> 
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>


#include "ogrsf_frmts.h" // GDAL/OGR


#include <pthread.h>

#include "m_pd.h"

#define MAX_BUFFER_SIZE 1024


static t_class *serialGPS_class;
 
static pthread_mutex_t pthreadLock = PTHREAD_MUTEX_INITIALIZER;

typedef struct _serialGPS {
  
  t_object  x_obj; //obj needed by pd   	
  
  t_symbol *addr;
  int baudrate;
  int sock;
  FILE * pFile;
  FILE * pFilewrite;
  
  double longitude;
  double latitude;
  char longit[128];
  char latit[128];	
  
  
  int debugFlag;
  
  
  char msg[MAX_BUFFER_SIZE];
  
  // pthread stuff
  pthread_t pthreadID; // id of child thread
  pthread_attr_t pthreadAttr; 
  
  t_outlet *theOutlet1;
  t_outlet *theOutlet2;
  t_outlet *theOutlet3;

  //t_symbol outData;
  t_atom outData[2];
  
} t_serialGPS;


/*
--------------------------------------------------------------------------------------
  N M E A   t e l e g r a m   f o r m a t   d e s c r i p t i o n s
--------------------------------------------------------------------------------------

 $GPRMC,<1>,<2>,<3>,<4>,<5>,<6>,<7>,<8>,<9>,<10>,<11>,<12>*hh<CR><LF>
    <1> UTC time of position fix, hhmmss format
    <2> Status, A = Valid position, V = NAV receiver warning
    <3> Latitude, ddmm.mmmm format (leading zeros will be transmitted)
    <4> Latitude hemisphere, N or S
    <5> Longitude, dddmm.mmmm format (leading zeros will be transmitted)
    <6> Longitude hemisphere, E or W
    <7> Speed over ground, 0000.0 to 1851.8 knots (leading zeros will be transmitted)
    <8> Course over ground, 000.0 to 359.9 degrees, true (leading zeros will be transmitted)
    <9> UTC date of position fix, ddmmyy format
    <10> Magnetic variation, 000.0 to 180.0 degrees (leading zeros will be transmitted)
    <11> Magnetic variation direction, E or W (westerly variation adds to true course)
    <12> Mode indicator (only output if NMEA 2.30 active), A = Autonomous, D = Differential, E =
         Estimated, N = Data not valid


 $GPGGA,<1>,<2>,<3>,<4>,<5>,<6>,<7>,<8>,<9>,M,<10>,M,<11>,<12>*hh<CR><LF>
    <1> UTC time of position fix, hhmmss format
    <2> Latitude, ddmm.mmmm format (leading zeros will be transmitted)
    <3> Latitude hemisphere, N or S
    <4> Longitude, dddmm.mmmm format (leading zeros will be transmitted)
    <5> Longitude hemisphere, E or W
    <6> GPS quality indication,
        0 = fix not available,
        1 = Non-differential GPS fix available,
        2 = Differential GPS (DGPS) fix available
        6 = Estimated
    <7> Number of satellites in use, 00 to 12 (leading zeros will be transmitted)
    <8> Horizontal dilution of precision, 0.5 to 99.9
    <9> Antenna height above/below mean sea level, -9999.9 to 99999.9 meters
    <10> Geoidal height, -999.9 to 9999.9 meters
    <11> Differential GPS (RTCM SC-104) data age, number of seconds since last valid RTCM
         transmission (null if non-DGPS)
    <12> Differential Reference Station ID, 0000 to 1023 (leading zeros transmitted, null if non-DGPS)
*/


void serialGPS_bang(t_serialGPS *x)
{
	double longitude, latitude;
	int gpstype;
	int iLat, iLng;
	
	// return if not connected:
	if ((x->sock==-1) || (!strlen(x->msg)))  return;
	
	char delims[] = ",";
	char *result = NULL;
	
	char *lngHemisphere;
	char *latHemisphere;
	
	
	pthread_mutex_lock(&pthreadLock);
	

		if (x->debugFlag)
		{			
			if (strncmp (x->msg,"$",1) != 0)
			  post("UBX:\t%s",x->msg);
			else
			  post("NMEA:\t%s",x->msg);
		}
		if (x->pFile!=NULL)
		  {
		    fwrite (x->msg , 1 , sizeof(x->msg) , x->pFile );
		    fwrite ("\n" , 1 , 1 , x->pFile );
		  }


	if (x->pFilewrite!=NULL)
	  {	    
	    fwrite (x->msg , 1 , sizeof(x->msg) , x->pFilewrite );
	    fwrite ("\n" , 1 , 1 , x->pFilewrite );
	  }
		result = strtok(x->msg, delims);
	
		if (strcmp(result,"$GPGGA")==0)
		{

			if ((result = strtok(NULL, delims)) == NULL ) return; // skip
				
			if ((result = strtok(NULL, delims)) == NULL ) return; // longitude
			longitude = atof(result);
			if ((lngHemisphere = strtok(NULL, delims)) == NULL ) return;
				
			if ((result = strtok(NULL, delims)) == NULL ) return; // latitude
			latitude = atof(result);
			if ((latHemisphere = strtok(NULL, delims)) == NULL ) return;
			

			if ((result = strtok(NULL, delims)) == NULL ) return;
			gpstype = atof(result);
			
			// convert from NMEA to decimal:
			iLng = (int) (longitude/100);
			iLat = (int) (latitude/100);
			longitude = iLng + ((longitude - (iLng*100))/60);
			latitude = iLat + ((latitude - (iLat*100))/60);
			
			// southern and western hemispheres are negative
			if (strcmp(lngHemisphere,"S")==0) longitude *= -1;
			if (strcmp(latHemisphere,"W")==0) latitude *= -1;
			

			x->longitude=longitude;
			x->latitude=latitude;
		
			//SETFLOAT(x->outData+0, longitude);
			//SETFLOAT(x->outData+1, latitude);
			sprintf(x->longit,"%f",longitude);
			sprintf(x->latit,"%f",latitude);

/* 			if (x->debugFlag) post("real LAT/LONG: %.8f %.8f", longitude, latitude); */
/* 			if (x->debugFlag) post("atom LAT/LONG: %.8f %.8f", atom_getfloat(x->outData), atom_getfloat(x->outData+1)); */
		

			outlet_symbol(x->theOutlet1,gensym(x->longit));
			outlet_symbol(x->theOutlet2,gensym(x->latit));
			outlet_symbol(x->theOutlet3,gensym(gpstype));
		}
	
	pthread_mutex_unlock(&pthreadLock);
	
}


void *serialGPS_poll(void *arg)
{
	fd_set rset;
	struct timeval timout;
	int ret, csize, i;
	char c;
	char buf[MAX_BUFFER_SIZE];
	int currentIndex = 0;

	t_serialGPS *x = (t_serialGPS *)arg;
	
	FD_ZERO(&rset);
	
 	while (1)
	{
		if (x->sock < 0)
		{
			close(x->sock);
			pthread_exit(NULL);
		}
		
		FD_ZERO(&rset);
		FD_SET(x->sock,&rset);

		//ret = select(x->sock+1, &rset, NULL, NULL, &timeout);
		ret = select(x->sock+1, &rset, NULL, NULL, NULL);
		if (ret < 0)
		{
			post("ERROR: serialGPS_poll on select()");
			break;
		}
		
		if (FD_ISSET(x->sock,&rset))
	    {
			// There's stuff to read:
			
			if ((csize = read(x->sock, &c, 1)) >= 1)
			{
				
				if ((c == '$') || (currentIndex >= MAX_BUFFER_SIZE-1))
				{
					buf[currentIndex] = '\0';
					
					// at this point, our message is complete.
					
					pthread_mutex_lock(&pthreadLock);
						strcpy(x->msg, buf);
						//strncpy(x->msg, buf, currentIndex);
					pthread_mutex_unlock(&pthreadLock);
					
					currentIndex = 0;
				}
				
				if (c != '\n')
				{
					memcpy(buf + currentIndex, &c, 1);
					currentIndex++;
				}
				
			}
			else { post("oops"); break; }           /* Failed */
			pthread_mutex_unlock(&pthreadLock);
	    }
	}
	
	return 0;
}



/* baudrate settings are defined in <asm/termbits.h>, which is included by <termios.h> */ 
int makeSerialPortSocket(char port[],int baud)
{
        int fd; 
        struct termios newtio; 

        fd = open(port,O_RDWR); // open up the port on read / write mode
        if (fd == -1)
                return(-1); // Opps. We just has an error

        /* Save the current serial port settings */
        tcgetattr(fd, &newtio);
        
        /* Set the input/output baud rates for this device */
        cfsetispeed(&newtio, baud); //115200

        /* CLOCAL:      Local connection (no modem control) */
        /* CREAD:       Enable the receiver */
        newtio.c_cflag |= (CLOCAL | CREAD);

        /* PARENB:      Use NO parity */
        /* CSTOPB:      Use 1 stop bit */
        /* CSIZE:       Next two constants: */
        /* CS8:         Use 8 data bits */
        newtio.c_cflag &= ~PARENB;
        newtio.c_cflag &= ~CSTOPB;
        newtio.c_cflag &= ~CSIZE;
        newtio.c_cflag |= CS8;

        /* Disable hardware flow control */
        // BAD:  newtio.c_cflag &= ~(CRTSCTS);

        /* ICANON:      Disable Canonical mode */
        /* ECHO:        Disable echoing of input characters */
        /* ECHOE:       Echo erase characters as BS-SP-BS */
        /* ISIG:        Disable status signals */
        // BAD: newtio.c_lflag = (ECHOK);

        /* IGNPAR:      Ignore bytes with parity errors */
        /* ICRNL:       Map CR to NL (otherwise a CR input on the other computer will not terminate input) */
        // BAD:  newtio.c_iflag |= (IGNPAR | ICRNL);
        newtio.c_iflag |= (IGNPAR | IGNBRK); 
        
        /* NO FLAGS AT ALL FOR OUTPUT CONTROL  -- Sean */
        newtio.c_oflag = 0;

        /* IXON:        Disable software flow control (incoming) */
        /* IXOFF:       Disable software flow control (outgoing) */
        /* IXANY:       Disable software flow control (any character can start flow control */
        newtio.c_iflag &= ~(IXON | IXOFF | IXANY);

        /* NO FLAGS AT ALL FOR LFLAGS  -- Sean*/
        newtio.c_lflag = 0;

        /*** The following settings are deprecated and we are no longer using them (~Peter) ****/
        // cam_data.newtio.c_lflag &= ~(ICANON && ECHO && ECHOE && ISIG); 
        // cam_data.newtio.c_lflag = (ECHO);
        // cam_data.newtio.c_iflag = (IXON | IXOFF);
        /* Raw output */
        // cam_data.newtio.c_oflag &= ~OPOST;

        /* Clean the modem line and activate new port settings */
        tcflush(fd, TCIOFLUSH);
        tcsetattr(fd, TCSANOW, &newtio);

        return(fd);
}



void serialGPS_setDGPS(t_serialGPS *x, t_floatarg flag)
{
	char cmd[512];

	if (x->sock==-1)
	{
		return;
	}
	
	else {

//DGPS enabling ubx message:
// - subsystem enabled
// - Apply SBAS correction data
// - nb of search channel = 3
// - PRN codes = WAAS
//B5 62 06 16 08 00 01 02 03 00 04 C0 04 00 F2 0A

	  unsigned char  buffertowrite[] = {
	    0xB5, 0x62, 0x06, 0x16, 0x08, 0x00,
	    0x01, 0x02, 0x03, 0x00, 0x04, 0xC0,
	    0x04, 0x00, 0xF2, 0x0A};

	  int result = write(x->sock,(char *) buffertowrite,16);  
	  write(x->sock, "\r", 1);  
	  if(result == 16) post("serialGPS: enabled DGPS mode");
	  else post("serialGPS: DGPS mode sending ubx-cfg-sbas message failed");
	}
		
	
	
}

void serialGPS_store(t_serialGPS *x, t_symbol *positionname)
{
  if (x->pFile!=NULL && (x->sock!=-1) )
    {	    
      fprintf(x->pFile,"%f",x->longitude);
      //      fwrite (&x->longitude) , 1 , 10 , x->pFile );
      fwrite ("," , 1 , 1 , x->pFile );
      //fwrite (ftoa(&x->latitude) , 1 , 10 , x->pFile );
      fprintf(x->pFile,"%f",x->latitude);
      fwrite ("," , 1 , 1 , x->pFile );
      fwrite (positionname->s_name, 1 , strlen(positionname->s_name) , x->pFile );
      fwrite ("\n" , 1 , 1 , x->pFile );
    }


}


void serialGPS_setDebug(t_serialGPS *x, t_floatarg f)
{
	x->debugFlag = (int)f;
}



void serialGPS_connect(t_serialGPS *x, t_symbol *targetAddress, t_floatarg targetBaud)
{
	x->addr = targetAddress;
	x->baudrate = targetBaud;

	switch ((int)targetBaud) {
		case 115200:
			x->baudrate = B115200;
			break;
		case 38400:
			x->baudrate = B38400;
			break;
		case 19200:
			x->baudrate = B19200;
			break;
		case 9600:
			x->baudrate = B9600;
			break;
		default:
			post("ERROR!: Unknown baud rate: %d\n", (int) targetBaud);
			return;
			break;
	}

	//nic
		
	  x->pFile = fopen ("serialGPS_datastored.txt","w");
 	  x->pFilewrite = fopen ("serialGPS_NMEA.txt","w");
	//endnic


 	x->sock = makeSerialPortSocket(x->addr->s_name, x->baudrate);
	
	if (x->sock <= 0) {
		post("ERROR: couldn't open serial port!\n");
		close(x->sock);
		return;
	} else {
		post("Listening to GPS data (NMEA format) on: %s", x->addr->s_name);
	}

	
	// create child thread which will poll for data:
	if (pthread_attr_init(&x->pthreadAttr) < 0)
		post("serialGPS: warning: could not prepare child thread\n");
	if (pthread_attr_setdetachstate(&x->pthreadAttr, PTHREAD_CREATE_DETACHED) < 0)
		post("serialGPS: warning: could not prepare child thread\n");
	if (pthread_create( &x->pthreadID, &x->pthreadAttr, serialGPS_poll, x) < 0)
		post("serialGPS: could not create new thread\n");
	
}

void serialGPS_disconnect(t_serialGPS *x)
{
	x->sock = -1;
}

void serialGPS_free(t_serialGPS *x)
{
 
  //nic
fclose (x->pFile);
fclose (x->pFilewrite);
//nic

	serialGPS_disconnect(x);
}

void *serialGPS_new(t_symbol *addr)
{
	t_serialGPS *x = (t_serialGPS *)pd_new(serialGPS_class);

	x->sock = -1;
	x->msg[0] = '\0';
	x->debugFlag = 0;	

	x->theOutlet1 = outlet_new(&x->x_obj, &s_symbol);
	x->theOutlet2 = outlet_new(&x->x_obj, &s_symbol);
	x->theOutlet3 = outlet_new(&x->x_obj, &s_symbol);
	return (void *)x;
}


int write_serial(t_serialGPS *x, unsigned char  serial_byte)
{
    int result = write(x->sock,(char *) &serial_byte,1);
    if (result != 1)
        post ("[serialGPS] write returned %d, errno is %d", result, errno);
    return result;
}



void serialGPS_float(t_serialGPS *x, t_float f)
{
    unsigned char serial_byte = ((int) f) & 0xFF; /* brutal conv */

    if (write_serial(x,serial_byte) != 1)
    {
        post("Write error, maybe TX-OVERRUNS on serial line");
    }
}



void serialGPS_setup(void) {
	
	serialGPS_class = class_new(gensym("serialGPS"), (t_newmethod)serialGPS_new, (t_method)serialGPS_free, sizeof(t_serialGPS), CLASS_DEFAULT, A_DEFSYMBOL, 0);
	//nic
	class_addfloat(serialGPS_class, (t_method)serialGPS_float);
	//endnic
	class_addbang   (serialGPS_class, serialGPS_bang);
	class_addmethod (serialGPS_class, (t_method)serialGPS_connect, gensym("connect"), A_DEFSYMBOL, A_DEFFLOAT, 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_setDGPS, gensym("setDGPS"), A_DEFFLOAT, 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_setDebug, gensym("setDebug"), A_DEFFLOAT, 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_disconnect, gensym("disconnect"), 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_store, gensym("store"), A_DEFSYMBOL, 0);

}
