#ifndef __GPDUTIL_H
#define __GPSUTIL_H

// NMEA lib (http://nmea.sourceforge.net)
#include <nmea/nmea.h>

// GDAL/OGR
#include "ogrsf_frmts.h" 
#include "ogr_api.h"

// Pd include:
#include "m_pd.h"

// PROJ.4
#include <proj_api.h>


/*

HERE IS SOME INFO ABOUT THE STRUCTS WE USE: 
 
NMEAinfo:
    int     smask;       Mask specifying types of packages from which data have been obtained
    nmeaTIME utc;        UTC of position
    int     sig;         GPS quality indicator (0 = Invalid; 1 = Fix; 2 = Differential, 3 = Sensitive)
    int     fix;         Operating mode, used for navigation (1 = Fix not available; 2 = 2D; 3 = 3D)
    double  PDOP;        Position Dilution Of Precision
    double  HDOP;        Horizontal Dilution Of Precision
    double  VDOP;        Vertical Dilution Of Precision
    double  lat;         Latitude in NDEG - +/-[degree][min].[sec/60]
    double  lon;         Longitude in NDEG - +/-[degree][min].[sec/60]
    double  elv;         Antenna altitude above/below mean sea level (geoid) in meters
    double  speed;       Speed over the ground in kilometers/hour
    double  direction;   Track angle in degrees True
    double  declination; Magnetic variation degrees (Easterly var. subtracts from true course)
    nmeaSATINFO satinfo; Satellites information
	 
OGR layer types:	 
    0  wkbUnknown 	unknown type, non-standard
    1  wkbPoint 	0-dimensional geometric object, standard WKB
    2  wkbLineString 	1-dimensional geometric object with linear interpolation between Points, standard WKB
    3  wkbPolygon 	planar 2-dimensional geometric object defined by 1 exterior boundary and 0 or more interior boundaries, standard WKB
    4  wkbMultiPoint 	GeometryCollection of Points, standard WKB
    5  wkbMultiLineString 	GeometryCollection of LineStrings, standard WKB
    6  wkbMultiPolygon 	GeometryCollection of Polygons, standard WKB
    7  wkbGeometryCollection 	geometric object that is a collection of 1 or more geometric objects, standard WKB
    8  wkbNone 	non-standard, for pure attribute records
    9  wkbLinearRing 	non-standard, just for createGeometry()
    10 wkbPoint25D 	2.5D extension as per 99-402
    11 wkbLineString25D 	2.5D extension as per 99-402
    12 wkbPolygon25D 	2.5D extension as per 99-402
    13 wkbMultiPoint25D 	2.5D extension as per 99-402
    14 wkbMultiLineString25D 	2.5D extension as per 99-402
    15 wkbMultiPolygon25D 	2.5D extension as per 99-402
    16 wkbGeometryCollection25D 	2.5D extension as per 99-402 	 
	 
Fields for layer of wkbPoint (waypoints):
  field[0]: 'ele', type=2, width=0, precision=0
  field[1]: 'time', type=11, width=0, precision=0
  field[2]: 'magvar', type=2, width=0, precision=0
  field[3]: 'geoidheight', type=2, width=0, precision=0
  field[4]: 'name', type=4, width=0, precision=0
  field[5]: 'cmt', type=4, width=0, precision=0
  field[6]: 'desc', type=4, width=0, precision=0
  field[7]: 'src', type=4, width=0, precision=0
  field[8]: 'link1_href', type=4, width=0, precision=0
  field[9]: 'link1_text', type=4, width=0, precision=0
  field[10]: 'link1_type', type=4, width=0, precision=0
  field[11]: 'link2_href', type=4, width=0, precision=0
  field[12]: 'link2_text', type=4, width=0, precision=0
  field[13]: 'link2_type', type=4, width=0, precision=0
  field[14]: 'sym', type=4, width=0, precision=0
  field[15]: 'type', type=4, width=0, precision=0
  field[16]: 'fix', type=4, width=0, precision=0
  field[17]: 'sat', type=0, width=0, precision=0
  field[18]: 'hdop', type=2, width=0, precision=0
  field[19]: 'vdop', type=2, width=0, precision=0
  field[20]: 'pdop', type=2, width=0, precision=0
  field[21]: 'ageofdgpsdata', type=2, width=0, precision=0
  field[22]: 'dgpsid', type=0, width=0, precision=0
  
Fields for layer of wkbLineString (routes):
  field[0]: 'name', type=4, width=0, precision=0
  field[1]: 'cmt', type=4, width=0, precision=0
  field[2]: 'desc', type=4, width=0, precision=0
  field[3]: 'src', type=4, width=0, precision=0
  field[4]: 'link1_href', type=4, width=0, precision=0
  field[5]: 'link1_text', type=4, width=0, precision=0
  field[6]: 'link1_type', type=4, width=0, precision=0
  field[7]: 'link2_href', type=4, width=0, precision=0
  field[8]: 'link2_text', type=4, width=0, precision=0
  field[9]: 'link2_type', type=4, width=0, precision=0
  field[10]: 'number', type=0, width=0, precision=0
  field[11]: 'type', type=4, width=0, precision=0


where types:
	[0]		OFTInteger 			Simple 32bit integer
	[1]		OFTIntegerList 		List of 32bit integers
	[2]		OFTReal 			Double Precision floating point
	[3]		OFTRealList 		List of doubles
	[4]		OFTString 			String of ASCII chars
	[5]		OFTStringList 		Array of strings
	[6]		OFTWideString 		deprecated
	[7]		OFTWideStringList 	deprecated
	[8]		OFTBinary 			Raw Binary data
	[9] 	OFTDate 			Date
	[10]	OFTTime 			Time
	[11]	OFTDateTime 		Date and Time 

*/
	




char* NMEATimeAsString(nmeaTIME *utc);
int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y);
int ConvertToUTM (double *lat, double *lon, double *elv);


#endif