
#include "GPSutil.h"


using namespace std;



/* Create a UTC-style date/time string from the nmeaTIME struct:
 */
char* NMEATimeAsString(nmeaTIME *utc)
{
	char *buffer = new char[22];
	sprintf (buffer, "%04d-%02d-%02d %02d:%02d:%02d.%02d", 1900+utc->year, utc->mon, utc->day, utc->hour, utc->min, utc->sec, utc->hsec);

	return buffer;
}


/* Subtract the `struct timeval' values X and Y, storing the result in RESULT.
 * Return 1 if the difference is negative, otherwise 0.
 */
int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y)
{
  // Perform the carry for the later subtraction by updating y:
  if (x->tv_usec < y->tv_usec) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
    y->tv_usec -= 1000000 * nsec;
    y->tv_sec += nsec;
  }
  if (x->tv_usec - y->tv_usec > 1000000) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000;
    y->tv_usec += 1000000 * nsec;
    y->tv_sec -= nsec;
  }

  // Compute the time remaining to wait. tv_usec is certainly positive:
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_usec = x->tv_usec - y->tv_usec;

  // Return 1 if result is negative:
  return x->tv_sec < y->tv_sec;
}

int ConvertToUTM (double *lat, double *lon, double *elv)
{
	projPJ pj_utm, pj_latlong;
	pj_utm = pj_init_plus("+proj=utm  +ellps=intl +zone=11");
	pj_latlong = pj_init_plus("+proj=latlong +datum=WGS84");
	
	if (pj_utm && pj_latlong)
	{
		if (pj_transform(pj_latlong, pj_utm, 1, 1, lat, lon, elv ) == 0) return 1; // success
	}
	
	return 0;
}
