pdsheefa library sources
Current version  0.4  (April 2011)



The pdsheefa library provides a mix of abstractions and externs to provide for pd-based SPIN (http://spinframework.org/) client development.  

Included are objects for scene management ("show control") and audio rendering, among other things.

This library consolidates patches and externals taken from other (now frozen) repositories,
such as "audioscsape", spinwidgets, and pdsheefa-0.3.6.


DEPENDENCIES: 

The pdsheefa library automatically includes an external repository called "xjimmies" (https://code.sat.qc.ca/nslam/trunk/xjimmies), upon which, certain abstractions in pdsheefa depend for audio processing.




All externs and abstractions will be installed to:

for OSX: 
	~/Library/Pd/pdsheefa
	~/Library/Pd/xjimmies
	
for Linux:
	/usr/local/lib/pd-externals/pdsheefa
	/usr/local/lib/pd-externals/xjimmies

note: you will need to add the above two directories to your pd search path, according to your OS

--z.settel 2011



